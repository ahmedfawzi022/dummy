import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbMenuModule } from '@nebular/theme';
import { ThemeModule } from '../../@theme/theme.module';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NotificationsComponent } from './notifications.component';
import { NgSelectModule } from '@ng-select/ng-select';

import {
  NbAccordionModule,
  NbButtonModule,
  NbSpinnerModule,
  NbListModule,
  NbRouteTabsetModule,
  NbStepperModule,
  NbTabsetModule, NbUserModule,
  NbCardModule, NbIconModule, NbInputModule, NbTreeGridModule,
  NbActionsModule,
  NbCheckboxModule,
  NbDatepickerModule,
  NbRadioModule,
  NbSelectModule,
} from '@nebular/theme';

import { NotificationsRoutingModule } from './notifications-routing.module';
import { NotificationComponent } from './notification/notification.component';
import { AddNotificationComponent } from './add-notification/add-notification.component';
import { NotificationsActionButtonComponent } from './notification/notifications-action-button/notifications-action-button.component';
import { NotifiactionsModelSendSpesificUsersComponent } from './notification/notifiactions-model-send-spesific-users/notifiactions-model-send-spesific-users.component';


@NgModule({
  declarations: [NotificationComponent,NotificationsComponent, AddNotificationComponent, NotificationsActionButtonComponent, NotifiactionsModelSendSpesificUsersComponent],
  imports: [
    CommonModule,
    NotificationsRoutingModule,
    NbMenuModule,
    NbCardModule,
    NbTreeGridModule,
    NbIconModule,
    NbInputModule,
    NbSpinnerModule,
    ThemeModule,
    Ng2SmartTableModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule,
    NbTabsetModule,
    NbRouteTabsetModule,
    NbStepperModule,
    NbCardModule,
    NbButtonModule,
    NbListModule,
    NbAccordionModule,
    NbUserModule,
    NbActionsModule,
    NbCheckboxModule,
    NbDatepickerModule,
    NbRadioModule,
    NbSelectModule
  ],
  entryComponents: [NotificationsActionButtonComponent,NotifiactionsModelSendSpesificUsersComponent],

})
export class NotificationsModule { }

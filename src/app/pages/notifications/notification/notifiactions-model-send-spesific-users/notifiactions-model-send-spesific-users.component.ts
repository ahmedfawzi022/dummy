import { Component, OnInit ,Input} from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { NotificationService } from '../../../../services/notifications/notification.service';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { UsersService } from '../../../../services/users/users.service';
import {
  NbComponentStatus,
  NbGlobalLogicalPosition,
  NbGlobalPhysicalPosition,
  NbGlobalPosition,
  NbToastrConfig,
  NbToastrService
} from '@nebular/theme';
@Component({
  selector: 'ngx-notifiactions-model-send-spesific-users',
  templateUrl:'./notifiactions-model-send-spesific-users.component.html',
  styleUrls: ['./notifiactions-model-send-spesific-users.component.scss']
})
export class NotifiactionsModelSendSpesificUsersComponent implements OnInit {
  loading:any;
  destroyByClick = true;
  duration = 2000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;
  status: NbComponentStatus = 'primary';
  title = 'HI there!';
  content = `I'm cool toaster!`;
cellData:any;
  constructor(protected ref: NbDialogRef<NotifiactionsModelSendSpesificUsersComponent>,public toastrService: NbToastrService,public usersService:UsersService,public notificationService:NotificationService) { }
    //Define Form Group with its controls
    addForm: FormGroup;
    idControl: FormControl;
    usersControl: FormControl;
    customers=[];
  @Input() titleCard: string;

  ngOnInit(): void {
            // create form group and its form controls and
            this.createFormControls();
            this.createForm();
            this.getCustomers();
  }
    // create form controls that we need to validate with their needed validators
    createFormControls() {
      this.usersControl = new FormControl(null, [
 //       Validators.required,
      ]),
 
  
      this.idControl = new FormControl(null)
    }
  
  
    // add form controls to form group
    createForm() {
      this.addForm = new FormGroup({
        users: this.usersControl,
        id: this.idControl
      });
    }
  sendToSpesficUsers(){
    this.loading = true;

    this.idControl.setValue(this.cellData._id)
    this.notificationService.notifyUsers(this.addForm.value)

        .subscribe(
          data => {
            // this.del.nativeElement.click();
            // this.makeToast(this.configurationDataObject.feesDeleteSuccessfullMessage);
            this.status = 'success'
            this.title = 'Successfully Saved .. ';
            this.content = 'Please check !!'
            this.showToast(this.status, this.title, this.content);
            this.usersControl.setValue([])

            this.loading = false;

          },
          error => {
            this.loading = false;
            try {
              this.status = 'danger'
              this.title = 'Please check !!';
              this.content = error.error.message
              this.showToast(this.status, this.title, this.content);
            } catch (err) {
              this.status = 'danger'
              this.title = 'Please check !!';
              this.content = err.message;
              this.showToast(this.status, this.title, this.content);
              // this.openModal(this.configurationDataObject.error, this.configurationDataObject.errorMessage);
            }
          },
      );
    // this.status='danger'
    // this.title='Successfully Saved .. ';
    // this.content='Please check !!'
    // this.showToast(this.status, this.title, this.content);
  }

  getCustomers(){
    this.usersService.getCustomers()

        .subscribe(
          data => {
            this.customers=data.data.list
            // this.del.nativeElement.click();
            // this.makeToast(this.configurationDataObject.feesDeleteSuccessfullMessage);

          },
          error => {
//            this.del.nativeElement.click();
            try {
              // if (error.error.errorCode)
              // this.openModal(error.error.errorCode, error.error.errorMessage);
              // this.openModal(this.configurationDataObject.error, this.configurationDataObject.errorMessage);
            } catch (err) {
              // this.openModal(this.configurationDataObject.error, this.configurationDataObject.errorMessage);
            }
          },
      );

  }

  public showToast(type: NbComponentStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title ? `. ${title}` : '';

    this.toastrService.show(body, title, config);
  }
  dismiss() {
this.sendToSpesficUsers()
    this.ref.close();
  }

 
}

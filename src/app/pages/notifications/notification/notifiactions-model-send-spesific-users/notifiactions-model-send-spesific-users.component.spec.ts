import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotifiactionsModelSendSpesificUsersComponent } from './notifiactions-model-send-spesific-users.component';

describe('NotifiactionsModelSendSpesificUsersComponent', () => {
  let component: NotifiactionsModelSendSpesificUsersComponent;
  let fixture: ComponentFixture<NotifiactionsModelSendSpesificUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotifiactionsModelSendSpesificUsersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotifiactionsModelSendSpesificUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

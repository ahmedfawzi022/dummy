import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificationsActionButtonComponent } from './notifications-action-button.component';

describe('NotificationsActionButtonComponent', () => {
  let component: NotificationsActionButtonComponent;
  let fixture: ComponentFixture<NotificationsActionButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotificationsActionButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationsActionButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, Input } from '@angular/core';
import { NbDialogService } from '@nebular/theme';
import { NotifiactionsModelSendSpesificUsersComponent } from './../../notification/notifiactions-model-send-spesific-users/notifiactions-model-send-spesific-users.component';
import { BaseComponent } from '.././../../../pages/general/base/base.component';
import { NotificationService } from '../../../../services/notifications/notification.service';
import { DefaultEditor, ViewCell } from 'ng2-smart-table';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import {
  NbComponentStatus,
  NbGlobalLogicalPosition,
  NbGlobalPhysicalPosition,
  NbGlobalPosition,
  NbToastrConfig,
  NbToastrService
} from '@nebular/theme';
@Component({
  selector: 'ngx-notifications-action-button',
  templateUrl: './notifications-action-button.component.html',
  styleUrls: ['./notifications-action-button.component.scss']
})
export class NotificationsActionButtonComponent extends DefaultEditor implements OnInit, ViewCell {
  @Input() value: string | number;
  @Input() rowData: any;
  destroyByClick = true;
  duration = 2000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;
  status: NbComponentStatus = 'primary';
  title = 'HI there!';
  content = `I'm cool toaster!`;
  //Define Form Group with its controls
  addForm: FormGroup;
  idControl: FormControl;
  usersControl: FormControl;
  loading: any;
  constructor(private dialogService: NbDialogService, public notificationService: NotificationService, public toastrService: NbToastrService) {
    super();

  }

  ngOnInit(): void {
    // create form group and its form controls and
    this.createFormControls();
    this.createForm();

  }

  // create form controls that we need to validate with their needed validators
  createFormControls() {
    this.usersControl = new FormControl(null, [
      //       Validators.required,
    ]),


      this.idControl = new FormControl(null)
  }


  // add form controls to form group
  createForm() {
    this.addForm = new FormGroup({
      users: this.usersControl,
      id: this.idControl
    });
  }
  open() {
    this.dialogService.open(NotifiactionsModelSendSpesificUsersComponent, {
      context: {
        titleCard: 'This is a title passed to the dialog component',
        cellData: this.rowData
      },
    });
  }

  sendToAll() {
    // this.loading = true;

    this.idControl.setValue(this.rowData._id)
    this.usersControl.setValue([])
    this.usersControl.disable();
    this.notificationService.notifyUsers(this.addForm.value)
      .subscribe(
        data => {
          // this.del.nativeElement.click();
          // this.loading = false;
          this.status = 'success'
          this.title = 'Successfully Saved .. ';
          this.content = 'Please check !!'
          this.showToast(this.status, this.title, this.content);
          this.loading = false;
        },
        error => {
          this.loading = false;
          try {
            this.loading = false;
            this.status = 'danger'
            this.title = 'Please check !!';
            this.content = error.error.message
            this.showToast(this.status, this.title, this.content);
          } catch (err) {
            this.loading = false;
            this.status = 'danger'
            this.title = 'Please check !!';
            this.content = err.message;
            this.showToast(this.status, this.title, this.content);
          }
        },
      );
      this.loading = false;
  }
  public showToast(type: NbComponentStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title ? `. ${title}` : '';

    this.toastrService.show(body, title, config);
  }

}

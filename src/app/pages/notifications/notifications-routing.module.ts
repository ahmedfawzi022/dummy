import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotificationComponent } from './notification/notification.component';
import { AddNotificationComponent } from './add-notification/add-notification.component';
import { NotificationsComponent } from './notifications.component';


const routes: Routes = [{
  path: '',
  component: NotificationsComponent,
  children: [

    {
      path: 'notification',
      component: NotificationComponent,
      // canActivate: [RoleGuard],
      // data: {
      //   expectedRole: 'Read Configuration Template'
      // },
    },
    {
      path: 'add-notification',
      component: AddNotificationComponent,
      // canActivate: [RoleGuard],
      // data: {
      //   expectedRole: 'Read Configuration Template'
      // },
    },
    {
      path: 'add-notification/:id',
      component: AddNotificationComponent,
      // canActivate: [RoleGuard],
      // data: {
      //   expectedRole: 'Read Configuration Template'
      // },
    }

  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NotificationsRoutingModule { }

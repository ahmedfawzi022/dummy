import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RejectOrdersComponent } from './reject-orders.component';

describe('RejectOrdersComponent', () => {
  let component: RejectOrdersComponent;
  let fixture: ComponentFixture<RejectOrdersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RejectOrdersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RejectOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

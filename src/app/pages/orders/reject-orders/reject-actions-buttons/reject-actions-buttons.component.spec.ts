import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RejectActionsButtonsComponent } from './reject-actions-buttons.component';

describe('RejectActionsButtonsComponent', () => {
  let component: RejectActionsButtonsComponent;
  let fixture: ComponentFixture<RejectActionsButtonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RejectActionsButtonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RejectActionsButtonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

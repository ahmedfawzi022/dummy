import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RejectModelDetailsComponent } from './reject-model-details.component';

describe('RejectModelDetailsComponent', () => {
  let component: RejectModelDetailsComponent;
  let fixture: ComponentFixture<RejectModelDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RejectModelDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RejectModelDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit ,Input} from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { NbSortDirection, NbSortRequest, NbTreeGridDataSource, NbTreeGridDataSourceBuilder } from '@nebular/theme';

@Component({
  selector: 'ngx-reject-model-details',
  templateUrl: './reject-model-details.component.html',
  styleUrls: ['./reject-model-details.component.scss']
})
export class RejectModelDetailsComponent implements OnInit {

  celldata: any;
  isVendor:boolean=false;

  constructor(protected ref: NbDialogRef<RejectModelDetailsComponent>) { 

    }
    @Input() title: string;

  ngOnInit(): void {
    var currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if(currentUser.roles[0]=="Vendor"){
      this.isVendor=true;
    }
    else{
      this.isVendor=false;
    }
  }
  dismiss() {
    this.ref.close();
  }

}



import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FirstNameCustomerRejectOrdersComponent } from './first-name-customer-reject-orders.component';

describe('FirstNameCustomerRejectOrdersComponent', () => {
  let component: FirstNameCustomerRejectOrdersComponent;
  let fixture: ComponentFixture<FirstNameCustomerRejectOrdersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FirstNameCustomerRejectOrdersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FirstNameCustomerRejectOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

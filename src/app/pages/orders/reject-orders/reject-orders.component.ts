import { Component, OnInit, Input } from '@angular/core';
import { LocalDataSource, ServerDataSource } from 'ng2-smart-table';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { DataService } from '../../../services/data/data.service';

import { SmartTableData } from '../../../@core/data/smart-table';
import { DomSanitizer } from '@angular/platform-browser';
import { PendingActionsButtonComponent } from '../pending-orders/pending-actions-button/pending-actions-button.component';
import { FirstNameCustomerPendingOrderComponent } from '../pending-orders/first-name-customer-pending-order/first-name-customer-pending-order.component';
import { LastNameCustomerPendingOrderComponent } from '../pending-orders/last-name-customer-pending-order/last-name-customer-pending-order.component';
import { PhoneNumberCustomerPendingOrderComponent } from '../pending-orders/phone-number-customer-pending-order/phone-number-customer-pending-order.component';
import { NbDialogService } from '@nebular/theme';
import { environment } from '../../../../environments/environment';
@Component({
  selector: 'ngx-reject-orders',
  templateUrl: './reject-orders.component.html',
  styleUrls: ['./reject-orders.component.scss']
})
export class RejectOrdersComponent implements OnInit {
  @Input() inputCustomerId;
  @Input() inputVendorId;
  @Input() inputDashboard;


  loading = false;
  fubUrl: any;
  settings = {
    mode: 'external',
    actions:{
      add:false,
      edit:false,
      delete:false
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      // _id: {
      //   title: 'Order Id',
      //   type: 'string',
      //   filter: false,
      //   valuePrepareFunction: (imageUrl) => {
      //     let i = 0;
      //     return i + 1;
      //   },


      // },
      trackingNumber: {
        title: 'Order Id',
        type: 'string',
        filter:true,
      },
      
      formattedDate: {
        title: 'Date Created',
        type: 'string',
        filter: true,

      },
      firstNamecustomer: {
        title: 'First Name',
        type: 'custom',
        renderComponent: FirstNameCustomerPendingOrderComponent,
        editor: {
          type: 'custom',
          component: FirstNameCustomerPendingOrderComponent
        },
        sort: false,
        filter: false,
      },
      lastNamecustomer: {
        title: 'Last Name',
        type: 'custom',
        renderComponent: LastNameCustomerPendingOrderComponent,
        editor: {
          type: 'custom',
          component: LastNameCustomerPendingOrderComponent
        },
        sort: false,
        filter: false,
      },
      phoneNumbercustomer: {
        title: 'Phone Number',
        type: 'custom',
        renderComponent: PhoneNumberCustomerPendingOrderComponent,
        editor: {
          type: 'custom',
          component: PhoneNumberCustomerPendingOrderComponent
        },
        sort: false,
        filter: false,
      },
      relayAction: {
        title: 'Call To Actions',
        type: 'custom',
        renderComponent: PendingActionsButtonComponent,
        editor: {
          type: 'custom',
          component: PendingActionsButtonComponent
        },
        
        sort: false,
        filter: false,
        width: '340px',

      },
    //   imageUrl: {
    //     title: 'Details',
    //     filter: false,
    //     type: 'html',
    //     valuePrepareFunction: (imageUrl) => {
    //         return this._domSanitizer.bypassSecurityTrustHtml(`<button type='submit' nbButton  status="danger">View Details</button>`);
    //     },
    // },
    // age: {
    //   title: 'Status',
    //   filter: false,
    //   type: 'html',
    //   valuePrepareFunction: (imageUrl) => {
    //       return this._domSanitizer.bypassSecurityTrustHtml(`<button type='button' class='btn btn-primary'>Change Status</button>`);
    //   },
    // },
  
  }
  };

  source:ServerDataSource

  constructor(public data:DataService,private service: SmartTableData, public httpClient:HttpClient,private dialogService: NbDialogService,private router: Router, private _domSanitizer: DomSanitizer) {
    this.fubUrl = environment.FUBS_API_URL

  }
  ngOnInit(): void {
    var currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if(currentUser.roles[0]=="Vendor"){
      this.data.currentMessage.subscribe(
        data => {
          this.getserviceForVendor()
        });
      this.getserviceForVendor()
    }else{
      if(this.inputCustomerId){
        this.data.currentMessage.subscribe(
          data => {
            this.getserviceByCustomerId(this.inputCustomerId)
          });
        this.getserviceByCustomerId(this.inputCustomerId)
      }
      else if(this.inputVendorId){
        this.data.currentMessage.subscribe(
          data => {
            this.getserviceByVendorId(this.inputVendorId)
          });
        this.getserviceByVendorId(this.inputVendorId)

      }
      else{
        this.data.currentMessage.subscribe(
          data => {
            this.getservice()
          });
        this.getservice();
      }
    }
  }

  getservice() {
    this.loading = true;
    this.source = new ServerDataSource(this.httpClient, {
      endPoint: `${this.fubUrl}/orders/admin?orderStatus=2`,
      pagerPageKey: 'page',
      pagerLimitKey: 'limit',
      filterFieldKey: '#field#_like',
      totalKey: 'data.count',
      dataKey: 'data.list',
    }
    );
    this.loading = false;

  }
  getserviceByCustomerId(id) {
    this.loading = true;
    this.source = new ServerDataSource(this.httpClient, {
      endPoint: `${this.fubUrl}/shipments/admin?orderStatus=2&&customerId=${id}`,
      pagerPageKey: 'page',
      pagerLimitKey: 'limit',
      filterFieldKey: '#field#_like',
      totalKey: 'data.count',
      dataKey: 'data.list',
    }
    );
    this.loading = false;

  }
  getserviceByVendorId(id) {
    this.loading = true;
    this.source = new ServerDataSource(this.httpClient, {
      endPoint: `${this.fubUrl}/shipments/admin?orderStatus=2&&vendorId=${id}`,
      pagerPageKey: 'page',
      pagerLimitKey: 'limit',
      filterFieldKey: '#field#_like',
      totalKey: 'data.count',
      dataKey: 'data.list',
    }
    );
    this.loading = false;

  }
  getserviceForVendor() {
    this.loading = true;
    this.source = new ServerDataSource(this.httpClient, {
      endPoint: `${this.fubUrl}/shipments/vendor?shipmentStatus=2`,
      pagerPageKey: 'page',
      pagerLimitKey: 'limit',
      filterFieldKey: '#field#_like',
      totalKey: 'data.count',
      dataKey: 'data.list',
    }
    );
    this.loading = false;

  }

}





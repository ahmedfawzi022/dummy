import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RejectModelStatusComponent } from './reject-model-status.component';

describe('RejectModelStatusComponent', () => {
  let component: RejectModelStatusComponent;
  let fixture: ComponentFixture<RejectModelStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RejectModelStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RejectModelStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

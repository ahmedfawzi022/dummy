import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhoneNumberCustomerRejectOrdersComponent } from './phone-number-customer-reject-orders.component';

describe('PhoneNumberCustomerRejectOrdersComponent', () => {
  let component: PhoneNumberCustomerRejectOrdersComponent;
  let fixture: ComponentFixture<PhoneNumberCustomerRejectOrdersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhoneNumberCustomerRejectOrdersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhoneNumberCustomerRejectOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

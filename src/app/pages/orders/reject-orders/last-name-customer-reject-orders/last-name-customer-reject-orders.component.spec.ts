import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LastNameCustomerRejectOrdersComponent } from './last-name-customer-reject-orders.component';

describe('LastNameCustomerRejectOrdersComponent', () => {
  let component: LastNameCustomerRejectOrdersComponent;
  let fixture: ComponentFixture<LastNameCustomerRejectOrdersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LastNameCustomerRejectOrdersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LastNameCustomerRejectOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

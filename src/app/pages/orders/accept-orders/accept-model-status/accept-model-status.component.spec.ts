import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcceptModelStatusComponent } from './accept-model-status.component';

describe('AcceptModelStatusComponent', () => {
  let component: AcceptModelStatusComponent;
  let fixture: ComponentFixture<AcceptModelStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcceptModelStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcceptModelStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

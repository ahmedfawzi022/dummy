import { Component, OnInit ,Input} from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { OrdersService } from '../../../../services/orders/orders.service';
import { DataService } from '../../../../services/data/data.service';

import {
  NbComponentStatus,
  NbGlobalLogicalPosition,
  NbGlobalPhysicalPosition,
  NbGlobalPosition,
  NbToastrConfig,
  NbToastrService
} from '@nebular/theme';

@Component({
  selector: 'ngx-accept-model-status',
  templateUrl: './accept-model-status.component.html',
  styleUrls: ['./accept-model-status.component.scss']
})
export class AcceptModelStatusComponent implements OnInit {
  statusId:any;
  constructor(public toastrService: NbToastrService,public data:DataService,public ordersService:OrdersService,protected ref: NbDialogRef<AcceptModelStatusComponent>) { }
  loading:false;
  destroyByClick = true;
  duration = 2000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;
  status: NbComponentStatus = 'primary';
  titleMsg = 'HI there!';
  content = `I'm cool toaster!`;
  @Input() title: string;
@Input() celldata:any;
  ngOnInit(): void {
  }
  cancel() {
    this.ref.close();
  }
  onSelectStatus(event){
    console.log(event)
    this.statusId=event
 
  }

  submit() {
    this.ordersService.changeStatus(this.statusId,this.celldata._id).subscribe(
      data => {
        this.data.changeMessage(true);
        this.status = 'success'
        this.titleMsg = 'Successfully Saved .. ';
        this.content = 'Please check !!'
        this.showToast(this.status, this.titleMsg, this.content);
        this.loading=false;


      },
      error => {
        this.loading = false;
        try {
          this.status = 'danger'
          this.titleMsg = 'Please check !!';
          this.content = error.error.message
          this.showToast(this.status, this.titleMsg, this.content);
        } catch (err) {
          this.status = 'danger'
          this.titleMsg = 'Please check !!';
          this.content = err.message;
          this.showToast(this.status, this.titleMsg, this.content);
        }
      },
    );
    this.ref.close();
  }
  public showToast(type: NbComponentStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title ? `. ${title}` : '';

    this.toastrService.show(body, title, config);
  }
}

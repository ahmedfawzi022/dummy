import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LastnameCustomerAcceptOrderComponent } from './lastname-customer-accept-order.component';

describe('LastnameCustomerAcceptOrderComponent', () => {
  let component: LastnameCustomerAcceptOrderComponent;
  let fixture: ComponentFixture<LastnameCustomerAcceptOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LastnameCustomerAcceptOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LastnameCustomerAcceptOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FirstnameCustomerAcceptOrderComponent } from './firstname-customer-accept-order.component';

describe('FirstnameCustomerAcceptOrderComponent', () => {
  let component: FirstnameCustomerAcceptOrderComponent;
  let fixture: ComponentFixture<FirstnameCustomerAcceptOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FirstnameCustomerAcceptOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FirstnameCustomerAcceptOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

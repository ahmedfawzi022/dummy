import { Component, OnInit, Input } from '@angular/core';
import { LocalDataSource, ServerDataSource } from 'ng2-smart-table';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { DataService } from '../../../services/data/data.service';

import { SmartTableData } from '../../../@core/data/smart-table';
import { DomSanitizer } from '@angular/platform-browser';
import { PhonenumberCustomerAcceptOrderComponent } from '../accept-orders/phonenumber-customer-accept-order/phonenumber-customer-accept-order.component';
import { LastnameCustomerAcceptOrderComponent } from '../accept-orders/lastname-customer-accept-order/lastname-customer-accept-order.component';
import { FirstnameCustomerAcceptOrderComponent } from '../accept-orders/firstname-customer-accept-order/firstname-customer-accept-order.component';
import { AcceptActionsButtonComponent } from '../accept-orders/accept-actions-button/accept-actions-button.component';
import { NbDialogService } from '@nebular/theme';
import { environment } from '../../../../environments/environment';
@Component({
  selector: 'ngx-accept-orders',
  templateUrl: './accept-orders.component.html',
  styleUrls: ['./accept-orders.component.scss']
})
export class AcceptOrdersComponent implements OnInit {
  @Input() inputCustomerId;
  @Input() inputVendorId;
  @Input() inputDashboard;


  loading = false;
  fubUrl: any;
  settings = {
    mode: 'external',
    actions:{
      add:false,
      edit:false,
      delete:false
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      // _id: {
      //   title: 'Order Id',
      //   type: 'string',
      //   filter:false

      // },
      // createdAt: {
      //   title: 'Date Created',
      //   type: 'string',
      //   filter:false
      // },
      trackingNumber: {
        title: 'Order Id',
        type: 'string',
        filter:true,
      },
      
      formattedDate: {
        title: 'Date Created',
        type: 'string',
        filter: true,

      },
      firstNamecustomer: {
        title: 'First Name',
        type: 'custom',
        renderComponent: FirstnameCustomerAcceptOrderComponent,
        editor: {
          type: 'custom',
          component: FirstnameCustomerAcceptOrderComponent
        },
        sort: false,
        filter: false,
      },
      lastNamecustomer: {
        title: 'Last Name',
        type: 'custom',
        renderComponent: LastnameCustomerAcceptOrderComponent,
        editor: {
          type: 'custom',
          component: LastnameCustomerAcceptOrderComponent
        },
        sort: false,
        filter: false,
      },
      phoneNumbercustomer: {
        title: 'Phone Number',
        type: 'custom',
        renderComponent: PhonenumberCustomerAcceptOrderComponent,
        editor: {
          type: 'custom',
          component: PhonenumberCustomerAcceptOrderComponent
        },
        sort: false,
        filter: false,
      },
      relayAction: {
        title: 'Call To Actions',
        type: 'custom',
        renderComponent: AcceptActionsButtonComponent,
        editor: {
          type: 'custom',
          component: AcceptActionsButtonComponent
        },
        
        sort: false,
        filter: false,
        width: '340px',

      },
    //   imageUrl: {
    //     title: 'Details',
    //     filter: false,
    //     type: 'html',
    //     valuePrepareFunction: (imageUrl) => {
    //         return this._domSanitizer.bypassSecurityTrustHtml(`<button type='submit' nbButton  status="danger">View Details</button>`);
    //     },
    // },
    // age: {
    //   title: 'Status',
    //   filter: false,
    //   type: 'html',
    //   valuePrepareFunction: (imageUrl) => {
    //       return this._domSanitizer.bypassSecurityTrustHtml(`<button type='button' class='btn btn-primary'>Change Status</button>`);
    //   },
    // },
  
  }
  };

  source:ServerDataSource

  constructor(private service: SmartTableData,public data:DataService, public httpClient:HttpClient,private dialogService: NbDialogService,private router: Router, private _domSanitizer: DomSanitizer) {
    this.fubUrl = environment.FUBS_API_URL

  }
  ngOnInit(): void {
    var currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if(currentUser.roles[0]=="Vendor"){
      this.data.currentMessage.subscribe(
        data => {
          this.getserviceForVendor()
        });
      this.getserviceForVendor()
    }else{
      if(this.inputCustomerId){
        this.data.currentMessage.subscribe(
          data => {
            this.getserviceByCustomerId(this.inputCustomerId)
          });
        this.getserviceByCustomerId(this.inputCustomerId)
      }
      else if(this.inputVendorId){
        this.data.currentMessage.subscribe(
          data => {
            this.getserviceByVendorId(this.inputVendorId)
          });
        this.getserviceByVendorId(this.inputVendorId)

      }
      else{
        this.data.currentMessage.subscribe(
          data => {
            this.getservice()
          });
        this.getservice();
      }
    }
  }

  getservice() {
    this.loading = true;
    this.source = new ServerDataSource(this.httpClient, {
      endPoint: `${this.fubUrl}/orders/admin?orderStatus=3`,
      pagerPageKey: 'page',
      pagerLimitKey: 'limit',
      filterFieldKey: '#field#_like',
      totalKey: 'data.count',
      dataKey: 'data.list',
    }
    );
    this.loading = false;

  }
  getserviceByCustomerId(id) {
    this.loading = true;
    this.source = new ServerDataSource(this.httpClient, {
      endPoint: `${this.fubUrl}/shipments/admin?orderStatus=3&&customerId=${id}`,
      pagerPageKey: 'page',
      pagerLimitKey: 'limit',
      filterFieldKey: '#field#_like',
      totalKey: 'data.count',
      dataKey: 'data.list',
    }
    );
    this.loading = false;

  }
  getserviceByVendorId(id) {
    this.loading = true;
    this.source = new ServerDataSource(this.httpClient, {
      endPoint: `${this.fubUrl}/shipments/admin?orderStatus=3&&vendorId=${id}`,
      pagerPageKey: 'page',
      pagerLimitKey: 'limit',
      filterFieldKey: '#field#_like',
      totalKey: 'data.count',
      dataKey: 'data.list',
    }
    );
    this.loading = false;

  }
  getserviceForVendor() {
    this.loading = true;
    this.source = new ServerDataSource(this.httpClient, {
      endPoint: `${this.fubUrl}/shipments/vendor?shipmentStatus=3`,
      pagerPageKey: 'page',
      pagerLimitKey: 'limit',
      filterFieldKey: '#field#_like',
      totalKey: 'data.count',
      dataKey: 'data.list',
    }
    );
    this.loading = false;

  }

}





import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhonenumberCustomerAcceptOrderComponent } from './phonenumber-customer-accept-order.component';

describe('PhonenumberCustomerAcceptOrderComponent', () => {
  let component: PhonenumberCustomerAcceptOrderComponent;
  let fixture: ComponentFixture<PhonenumberCustomerAcceptOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhonenumberCustomerAcceptOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhonenumberCustomerAcceptOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

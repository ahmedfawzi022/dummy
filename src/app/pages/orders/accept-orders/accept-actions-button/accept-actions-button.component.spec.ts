import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcceptActionsButtonComponent } from './accept-actions-button.component';

describe('AcceptActionsButtonComponent', () => {
  let component: AcceptActionsButtonComponent;
  let fixture: ComponentFixture<AcceptActionsButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcceptActionsButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcceptActionsButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

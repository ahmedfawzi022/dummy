import { Component, OnInit,Input } from '@angular/core';
import { NbDialogService } from '@nebular/theme';
import { AcceptModelDetailsComponent } from '../accept-model-details/accept-model-details.component';
import { AcceptModelStatusComponent } from '../accept-model-status/accept-model-status.component';
import { DefaultEditor, ViewCell } from 'ng2-smart-table';
import { UsersService } from '../../../../services/users/users.service';
import { Router } from '@angular/router';
import { DataService } from '../../../../services/data/data.service';
@Component({
  selector: 'ngx-accept-actions-button',
  templateUrl: './accept-actions-button.component.html',
  styleUrls: ['./accept-actions-button.component.scss']
})
export class AcceptActionsButtonComponent extends DefaultEditor implements OnInit, ViewCell {
  constructor(private dialogService: NbDialogService,public  data:DataService,public usersService:UsersService, private router: Router) { super()}
  @Input() value: string | number;
  @Input() rowData: any;
  isVendor:boolean=false;
  loading:any;
  ngOnInit(): void {
    console.log(this.rowData)
    console.log(this.value)
    var currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if(currentUser.roles[0]=="Vendor"){
      this.isVendor=true;
    }else{
      this.isVendor=false;
    }
  }
  open() {
    this.dialogService.open(AcceptModelDetailsComponent, {
      context: {
        title: 'This is a title passed to the dialog component',
        celldata:this.rowData
      },
    });
  }
  changeStatus() {
    this.dialogService.open(AcceptModelStatusComponent, {
      context: {
        title: 'This is a title passed to the dialog component',
        celldata:this.rowData

      },
    });
  }

}

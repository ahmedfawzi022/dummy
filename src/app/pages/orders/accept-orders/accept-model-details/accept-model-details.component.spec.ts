import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcceptModelDetailsComponent } from './accept-model-details.component';

describe('AcceptModelDetailsComponent', () => {
  let component: AcceptModelDetailsComponent;
  let fixture: ComponentFixture<AcceptModelDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcceptModelDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcceptModelDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingModelStatusComponent } from './pending-model-status.component';

describe('PendingModelStatusComponent', () => {
  let component: PendingModelStatusComponent;
  let fixture: ComponentFixture<PendingModelStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingModelStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingModelStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

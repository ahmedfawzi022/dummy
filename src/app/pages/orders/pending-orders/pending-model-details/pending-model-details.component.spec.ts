import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingModelDetailsComponent } from './pending-model-details.component';

describe('PendingModelDetailsComponent', () => {
  let component: PendingModelDetailsComponent;
  let fixture: ComponentFixture<PendingModelDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingModelDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingModelDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

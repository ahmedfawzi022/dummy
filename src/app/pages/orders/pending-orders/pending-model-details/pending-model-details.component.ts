import { Component, OnInit ,Input} from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { NbSortDirection, NbSortRequest, NbTreeGridDataSource, NbTreeGridDataSourceBuilder } from '@nebular/theme';


@Component({
  selector: 'ngx-pending-model-details',
  templateUrl: './pending-model-details.component.html',
  styleUrls: ['./pending-model-details.component.scss']
})
export class PendingModelDetailsComponent implements OnInit {

  celldata: any;
  isVendor:boolean=false;

  constructor(protected ref: NbDialogRef<PendingModelDetailsComponent>) { 

    }
    @Input() title: string;

  ngOnInit(): void {
    var currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if(currentUser.roles[0]=="Vendor"){
      this.isVendor=true;
    }
    else{
      this.isVendor=false;
    }
  }
  dismiss() {
    this.ref.close();
  }

}



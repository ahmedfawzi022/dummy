import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingActionsButtonComponent } from './pending-actions-button.component';

describe('PendingActionsButtonComponent', () => {
  let component: PendingActionsButtonComponent;
  let fixture: ComponentFixture<PendingActionsButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingActionsButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingActionsButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

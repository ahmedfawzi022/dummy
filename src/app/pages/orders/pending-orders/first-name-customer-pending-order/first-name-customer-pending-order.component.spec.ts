import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FirstNameCustomerPendingOrderComponent } from './first-name-customer-pending-order.component';

describe('FirstNameCustomerPendingOrderComponent', () => {
  let component: FirstNameCustomerPendingOrderComponent;
  let fixture: ComponentFixture<FirstNameCustomerPendingOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FirstNameCustomerPendingOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FirstNameCustomerPendingOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

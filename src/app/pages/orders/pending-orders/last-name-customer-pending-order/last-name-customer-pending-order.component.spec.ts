import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LastNameCustomerPendingOrderComponent } from './last-name-customer-pending-order.component';

describe('LastNameCustomerPendingOrderComponent', () => {
  let component: LastNameCustomerPendingOrderComponent;
  let fixture: ComponentFixture<LastNameCustomerPendingOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LastNameCustomerPendingOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LastNameCustomerPendingOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhoneNumberCustomerPendingOrderComponent } from './phone-number-customer-pending-order.component';

describe('PhoneNumberCustomerPendingOrderComponent', () => {
  let component: PhoneNumberCustomerPendingOrderComponent;
  let fixture: ComponentFixture<PhoneNumberCustomerPendingOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhoneNumberCustomerPendingOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhoneNumberCustomerPendingOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { CommonModule,DatePipe } from '@angular/common';
import { NbMenuModule } from '@nebular/theme';
import { ThemeModule } from '../../@theme/theme.module';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {OrdersComponent  } from './orders.component';

import {
  NbAccordionModule,
  NbButtonModule,
  NbSpinnerModule,
  NbListModule,
  NbRouteTabsetModule,
  NbStepperModule,
  NbTabsetModule, NbUserModule,
  NbCardModule, NbIconModule, NbInputModule, NbTreeGridModule,
  NbActionsModule,
  NbCheckboxModule,
  NbDatepickerModule,
  NbRadioModule,
  NbSelectModule,
  
} from '@nebular/theme';

import { OrdersRoutingModule } from './orders-routing.module';
import { PendingOrdersComponent } from './pending-orders/pending-orders.component';
import { RunningOrdersComponent } from './running-orders/running-orders.component';
import { CompletedOrdersComponent } from './completed-orders/completed-orders.component';
import { PendingActionsButtonComponent } from './pending-orders/pending-actions-button/pending-actions-button.component';
import { CompletedActionsButtonComponent } from './completed-orders/completed-actions-button/completed-actions-button.component';
import { RunningActionButtonComponent } from './running-orders/running-action-button/running-action-button.component';
import { PendingModelDetailsComponent } from './pending-orders/pending-model-details/pending-model-details.component';
import { PendingModelStatusComponent } from './pending-orders/pending-model-status/pending-model-status.component';
import { RunningModelDetailsComponent } from './running-orders/running-model-details/running-model-details.component';
import { CompletedModelDetailsComponent } from './completed-orders/completed-model-details/completed-model-details.component';
import { FirstNameCustomerPendingOrderComponent } from './pending-orders/first-name-customer-pending-order/first-name-customer-pending-order.component';
import { LastNameCustomerPendingOrderComponent } from './pending-orders/last-name-customer-pending-order/last-name-customer-pending-order.component';
import { PhoneNumberCustomerPendingOrderComponent } from './pending-orders/phone-number-customer-pending-order/phone-number-customer-pending-order.component';
import { FirstNameCustomerRunningOrdersComponent } from './running-orders/first-name-customer-running-orders/first-name-customer-running-orders.component';
import { LastNameCustomerRunningOrdersComponent } from './running-orders/last-name-customer-running-orders/last-name-customer-running-orders.component';
import { PhoneNumberCustomerRunningOrdersComponent } from './running-orders/phone-number-customer-running-orders/phone-number-customer-running-orders.component';
import { ModelStatusRunningOrdersComponent } from './running-orders/model-status-running-orders/model-status-running-orders.component';
import { FirstNameCustomerCompletedOrdersComponent } from './completed-orders/first-name-customer-completed-orders/first-name-customer-completed-orders.component';
import { LastNameCustomerCompletedOrdersComponent } from './completed-orders/last-name-customer-completed-orders/last-name-customer-completed-orders.component';
import { PhoneNumberCustomerCompletedOrdersComponent } from './completed-orders/phone-number-customer-completed-orders/phone-number-customer-completed-orders.component';
import { AllOrdersComponent } from './all-orders/all-orders.component';
import { FirstNameCustomerOrderComponent } from './all-orders/first-name-customer-order/first-name-customer-order.component';
import { LastNameCustomerOrderComponent } from './all-orders/last-name-customer-order/last-name-customer-order.component';
import { PhoneNumberCustomerOrderComponent } from './all-orders/phone-number-customer-order/phone-number-customer-order.component';
import { DetailsCustomerOrderComponent } from './all-orders/details-customer-order/details-customer-order.component';
import { ActionsAllOrdersComponent } from './all-orders/actions-all-orders/actions-all-orders.component';
import { AcceptOrdersComponent } from './accept-orders/accept-orders.component';
import { RejectOrdersComponent } from './reject-orders/reject-orders.component';
import { FirstnameCustomerAcceptOrderComponent } from './accept-orders/firstname-customer-accept-order/firstname-customer-accept-order.component';
import { LastnameCustomerAcceptOrderComponent } from './accept-orders/lastname-customer-accept-order/lastname-customer-accept-order.component';
import { AcceptActionsButtonComponent } from './accept-orders/accept-actions-button/accept-actions-button.component';
import { AcceptModelDetailsComponent } from './accept-orders/accept-model-details/accept-model-details.component';
import { AcceptModelStatusComponent } from './accept-orders/accept-model-status/accept-model-status.component';
import { PhonenumberCustomerAcceptOrderComponent } from './accept-orders/phonenumber-customer-accept-order/phonenumber-customer-accept-order.component';
import { RejectActionsButtonsComponent } from './reject-orders/reject-actions-buttons/reject-actions-buttons.component';
import { RejectModelStatusComponent } from './reject-orders/reject-model-status/reject-model-status.component';
import { RejectModelDetailsComponent } from './reject-orders/reject-model-details/reject-model-details.component';
import { FirstNameCustomerRejectOrdersComponent } from './reject-orders/first-name-customer-reject-orders/first-name-customer-reject-orders.component';
import { LastNameCustomerRejectOrdersComponent } from './reject-orders/last-name-customer-reject-orders/last-name-customer-reject-orders.component';
import { PhoneNumberCustomerRejectOrdersComponent } from './reject-orders/phone-number-customer-reject-orders/phone-number-customer-reject-orders.component';


@NgModule({
  declarations: [PendingOrdersComponent, RunningOrdersComponent, CompletedOrdersComponent,OrdersComponent, PendingActionsButtonComponent, CompletedActionsButtonComponent, RunningActionButtonComponent, PendingModelDetailsComponent, PendingModelStatusComponent, RunningModelDetailsComponent, CompletedModelDetailsComponent, FirstNameCustomerPendingOrderComponent, LastNameCustomerPendingOrderComponent, PhoneNumberCustomerPendingOrderComponent, FirstNameCustomerRunningOrdersComponent, LastNameCustomerRunningOrdersComponent, PhoneNumberCustomerRunningOrdersComponent, ModelStatusRunningOrdersComponent, FirstNameCustomerCompletedOrdersComponent, LastNameCustomerCompletedOrdersComponent, PhoneNumberCustomerCompletedOrdersComponent, AllOrdersComponent, FirstNameCustomerOrderComponent, LastNameCustomerOrderComponent, PhoneNumberCustomerOrderComponent, DetailsCustomerOrderComponent, ActionsAllOrdersComponent, AcceptOrdersComponent, RejectOrdersComponent, FirstnameCustomerAcceptOrderComponent, LastnameCustomerAcceptOrderComponent, AcceptActionsButtonComponent, AcceptModelDetailsComponent, AcceptModelStatusComponent, PhonenumberCustomerAcceptOrderComponent, RejectActionsButtonsComponent, RejectModelStatusComponent, RejectModelDetailsComponent, FirstNameCustomerRejectOrdersComponent, LastNameCustomerRejectOrdersComponent, PhoneNumberCustomerRejectOrdersComponent],
  imports: [
    CommonModule,
    OrdersRoutingModule,
    CommonModule,
    NbMenuModule,
    NbCardModule,
    NbTreeGridModule,
    NbIconModule,
    NbInputModule,
    NbSpinnerModule,
    ThemeModule,
    Ng2SmartTableModule,
    FormsModule,
    ReactiveFormsModule,
    NbTabsetModule,
    NbRouteTabsetModule,
    NbStepperModule,
    NbCardModule,
    NbButtonModule,
    NbListModule,
    NbAccordionModule,
    NbUserModule,
    NbActionsModule,
    NbCheckboxModule,
    NbDatepickerModule,
    NbRadioModule,
    NbSelectModule
  ],
  providers: [

    DatePipe
    ],
    exports: [PendingOrdersComponent,RunningOrdersComponent,CompletedOrdersComponent],
  entryComponents: [PendingActionsButtonComponent,CompletedActionsButtonComponent,RunningActionButtonComponent],

})
export class OrdersModule { }

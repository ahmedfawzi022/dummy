import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PendingOrdersComponent } from './pending-orders/pending-orders.component';
import { RunningOrdersComponent } from './running-orders/running-orders.component';
import { CompletedOrdersComponent } from './completed-orders/completed-orders.component';
import {OrdersComponent  } from './orders.component';
import { AllOrdersComponent } from './all-orders/all-orders.component';
import { AcceptOrdersComponent } from './accept-orders/accept-orders.component';
import { RejectOrdersComponent } from './reject-orders/reject-orders.component';

const routes: Routes = [{
  path: '',
  component: OrdersComponent,
  children: [

    {
      path: 'pending-orders',
      component: PendingOrdersComponent,
      // canActivate: [RoleGuard],
      // data: {
      //   expectedRole: 'Read Configuration Template'
      // },
    },
    {
      path: 'running-orders',
      component: RunningOrdersComponent,
      // canActivate: [RoleGuard],
      // data: {
      //   expectedRole: 'Read Configuration Template'
      // },
    },
    {
      path: 'complete-orders',
      component: CompletedOrdersComponent,
      // canActivate: [RoleGuard],
      // data: {
      //   expectedRole: 'Read Configuration Template'
      // },
    },
    {
      path: 'accept-orders',
      component: AcceptOrdersComponent,
      // canActivate: [RoleGuard],
      // data: {
      //   expectedRole: 'Read Configuration Template'
      // },
    },
    {
      path: 'reject-orders',
      component: RejectOrdersComponent,
      // canActivate: [RoleGuard],
      // data: {
      //   expectedRole: 'Read Configuration Template'
      // },
    },
    {
      path: 'all-orders',
      component: AllOrdersComponent,
      // canActivate: [RoleGuard],
      // data: {
      //   expectedRole: 'Read Configuration Template'
      // },
    }
  ],
}];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrdersRoutingModule { }

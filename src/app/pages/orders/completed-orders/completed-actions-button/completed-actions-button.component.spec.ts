import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompletedActionsButtonComponent } from './completed-actions-button.component';

describe('CompletedActionsButtonComponent', () => {
  let component: CompletedActionsButtonComponent;
  let fixture: ComponentFixture<CompletedActionsButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompletedActionsButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompletedActionsButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

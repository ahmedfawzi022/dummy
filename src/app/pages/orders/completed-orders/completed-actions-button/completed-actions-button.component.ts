import { Component, OnInit,Input } from '@angular/core';
import { NbDialogService } from '@nebular/theme';
import { CompletedModelDetailsComponent } from '../completed-model-details/completed-model-details.component';
import { DefaultEditor, ViewCell } from 'ng2-smart-table';
import { UsersService } from '../../../../services/users/users.service';
import { Router } from '@angular/router';
import { DataService } from '../../../../services/data/data.service';
@Component({
  selector: 'ngx-completed-actions-button',
  templateUrl: './completed-actions-button.component.html',
  styleUrls: ['./completed-actions-button.component.scss']
})
export class CompletedActionsButtonComponent extends DefaultEditor implements OnInit, ViewCell {
  constructor(private dialogService: NbDialogService,public  data:DataService,public usersService:UsersService, private router: Router) { super()}
  @Input() value: string | number;
  @Input() rowData: any;
  loading:any;

  ngOnInit(): void {
  }
  open() {
    this.dialogService.open(CompletedModelDetailsComponent, {
      context: {
        title: 'This is a title passed to the dialog component',
        celldata:this.rowData

      },
    });
  }

  }



import { Component, OnInit, Input } from '@angular/core';
import { ServerDataSource } from 'ng2-smart-table';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

import { SmartTableData } from '../../../@core/data/smart-table';
import { DomSanitizer } from '@angular/platform-browser';
import { CompletedActionsButtonComponent } from '../completed-orders/completed-actions-button/completed-actions-button.component';
import { FirstNameCustomerCompletedOrdersComponent } from '../completed-orders/first-name-customer-completed-orders/first-name-customer-completed-orders.component';
import { LastNameCustomerCompletedOrdersComponent } from '../completed-orders/last-name-customer-completed-orders/last-name-customer-completed-orders.component';
import { PhoneNumberCustomerCompletedOrdersComponent } from '../completed-orders/phone-number-customer-completed-orders/phone-number-customer-completed-orders.component';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'ngx-completed-orders',
  templateUrl: './completed-orders.component.html',
  styleUrls: ['./completed-orders.component.scss']
})
export class CompletedOrdersComponent implements OnInit {
  @Input() inputCustomerId;
  @Input() inputVendorId;
  @Input() inputDashboard;


  loading: any;
  fubUrl: any
  settings = {
    mode: 'external',
    actions: {
      add: false,
      edit: false,
      delete: false
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      // _id: {
      //   title: 'Order Id',
      //   type: 'string',
      //   filter: false,
      //   valuePrepareFunction: (imageUrl) => {
      //     let i = 0;
      //     return i + 1;
      //   },


      // },
      trackingNumber: {
        title: 'Order Id',
        type: 'string',
        filter:true,
      },
      
      formattedDate: {
        title: 'Date Created',
        type: 'string',
        filter: true,

      },
      // createdAt: {
      //   title: 'Date Created',
      //   type: 'string',
      //   filter: false,

      // },
      firstNamecustomer: {
        title: 'First Name',
        type: 'custom',
        renderComponent: FirstNameCustomerCompletedOrdersComponent,
        editor: {
          type: 'custom',
          component: FirstNameCustomerCompletedOrdersComponent
        },
        sort: false,
        filter: false,
      },
      lastNamecustomer: {
        title: 'Last Name',
        type: 'custom',
        renderComponent: LastNameCustomerCompletedOrdersComponent,
        editor: {
          type: 'custom',
          component: LastNameCustomerCompletedOrdersComponent
        },
        sort: false,
        filter: false,
      },
      phoneNumbercustomer: {
        title: 'Phone Number',
        type: 'custom',
        renderComponent: PhoneNumberCustomerCompletedOrdersComponent,
        editor: {
          type: 'custom',
          component: PhoneNumberCustomerCompletedOrdersComponent
        },
        sort: false,
        filter: false,
      },
      relayAction: {
        title: 'Call To Actions',
        type: 'custom',
        renderComponent: CompletedActionsButtonComponent,
        editor: {
          type: 'custom',
          component: CompletedActionsButtonComponent
        },

        sort: false,
        filter: false,
        width: '340px',

      },
      //   imageUrl: {
      //     title: 'Details',
      //     filter: false,
      //     type: 'html',
      //     valuePrepareFunction: (imageUrl) => {
      //         return this._domSanitizer.bypassSecurityTrustHtml(`<button type='submit' nbButton  status="danger">View Details</button>`);
      //     },
      // },
      // age: {
      //   title: 'Status',
      //   filter: false,
      //   type: 'html',
      //   valuePrepareFunction: (imageUrl) => {
      //       return this._domSanitizer.bypassSecurityTrustHtml(`<button type='button' class='btn btn-primary'>Change Status</button>`);
      //   },
      // },

    }
  };

  source: ServerDataSource;

  constructor(private service: SmartTableData, public httpClient: HttpClient, private router: Router, private _domSanitizer: DomSanitizer) {
    this.fubUrl = environment.FUBS_API_URL

  }
  ngOnInit(): void {
    var currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (currentUser.roles[0] == "Vendor") {
      this.getserviceForVendor()
    } else {
      if (this.inputCustomerId) {
        this.getserviceByCustomerId(this.inputCustomerId)
      }
      else if (this.inputVendorId) {
        this.getserviceByVendorId(this.inputVendorId)


      } else {
        this.getservice();
      }
    }
  }

  getservice() {
    this.loading = true;
    this.source = new ServerDataSource(this.httpClient, {
      endPoint: `${this.fubUrl}/orders/admin?orderStatus=6`,
      pagerPageKey: 'page',
      pagerLimitKey: 'limit',
      filterFieldKey: '#field#_like',
      totalKey: 'data.count',
      dataKey: 'data.list',
    }
    );
    this.loading = false;

  }
  getserviceByCustomerId(id) {
    this.loading = true;
    this.source = new ServerDataSource(this.httpClient, {
      endPoint: `${this.fubUrl}/shipments/admin?orderStatus=6&&customerId=${id}`,
      pagerPageKey: 'page',
      pagerLimitKey: 'limit',
      filterFieldKey: '#field#_like',
      totalKey: 'data.count',
      dataKey: 'data.list',
    }
    );
    this.loading = false;

  }
  getserviceByVendorId(id) {
    this.loading = true;
    this.source = new ServerDataSource(this.httpClient, {
      endPoint: `${this.fubUrl}/shipments/admin?orderStatus=6&&vendorId=${id}`,
      pagerPageKey: 'page',
      pagerLimitKey: 'limit',
      filterFieldKey: '#field#_like',
      totalKey: 'data.count',
      dataKey: 'data.list',
    }
    );
    this.loading = false;

  }
  getserviceForVendor() {
    this.loading = true;
    this.source = new ServerDataSource(this.httpClient, {
      endPoint: `${this.fubUrl}/shipments/vendor?shipmentStatus=6`,
      pagerPageKey: 'page',
      pagerLimitKey: 'limit',
      filterFieldKey: '#field#_like',
      totalKey: 'data.count',
      dataKey: 'data.list',
    }
    );
    this.loading = false;
  }
}





import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompletedModelDetailsComponent } from './completed-model-details.component';

describe('CompletedModelDetailsComponent', () => {
  let component: CompletedModelDetailsComponent;
  let fixture: ComponentFixture<CompletedModelDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompletedModelDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompletedModelDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

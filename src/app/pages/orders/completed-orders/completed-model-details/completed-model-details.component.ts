import { Component, OnInit ,Input} from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
@Component({
  selector: 'ngx-completed-model-details',
  templateUrl: './completed-model-details.component.html',
  styleUrls: ['./completed-model-details.component.scss']
})
export class CompletedModelDetailsComponent implements OnInit {
  celldata: any;
  isVendor:boolean=false;


  constructor(protected ref: NbDialogRef<CompletedModelDetailsComponent>) { 

    }
    @Input() title: string;

  ngOnInit(): void {
    var currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if(currentUser.roles[0]=="Vendor"){
      this.isVendor=true;
    }
    else{
      this.isVendor=false;
    }
  }
  dismiss() {
    this.ref.close();
  }

}

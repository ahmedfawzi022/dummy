import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FirstNameCustomerCompletedOrdersComponent } from './first-name-customer-completed-orders.component';

describe('FirstNameCustomerCompletedOrdersComponent', () => {
  let component: FirstNameCustomerCompletedOrdersComponent;
  let fixture: ComponentFixture<FirstNameCustomerCompletedOrdersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FirstNameCustomerCompletedOrdersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FirstNameCustomerCompletedOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

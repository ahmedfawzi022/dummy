import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhoneNumberCustomerCompletedOrdersComponent } from './phone-number-customer-completed-orders.component';

describe('PhoneNumberCustomerCompletedOrdersComponent', () => {
  let component: PhoneNumberCustomerCompletedOrdersComponent;
  let fixture: ComponentFixture<PhoneNumberCustomerCompletedOrdersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhoneNumberCustomerCompletedOrdersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhoneNumberCustomerCompletedOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LastNameCustomerCompletedOrdersComponent } from './last-name-customer-completed-orders.component';

describe('LastNameCustomerCompletedOrdersComponent', () => {
  let component: LastNameCustomerCompletedOrdersComponent;
  let fixture: ComponentFixture<LastNameCustomerCompletedOrdersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LastNameCustomerCompletedOrdersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LastNameCustomerCompletedOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionsAllOrdersComponent } from './actions-all-orders.component';

describe('ActionsAllOrdersComponent', () => {
  let component: ActionsAllOrdersComponent;
  let fixture: ComponentFixture<ActionsAllOrdersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActionsAllOrdersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionsAllOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit,Input } from '@angular/core';
import { DetailsCustomerOrderComponent } from '../details-customer-order/details-customer-order.component';
import { NbDialogService } from '@nebular/theme';
import { DefaultEditor, ViewCell } from 'ng2-smart-table';
import { UsersService } from '../../../../services/users/users.service';
import { Router } from '@angular/router';
import { DataService } from '../../../../services/data/data.service';
@Component({
  selector: 'ngx-actions-all-orders',
  templateUrl: './actions-all-orders.component.html',
  styleUrls: ['./actions-all-orders.component.scss']
})
export class ActionsAllOrdersComponent extends DefaultEditor implements OnInit, ViewCell {
  constructor(private dialogService: NbDialogService,public  data:DataService,public usersService:UsersService, private router: Router) { super()}
  @Input() value: string | number;
  @Input() rowData: any;
  loading:any;
  ngOnInit(): void {
    console.log(this.rowData)
    console.log(this.value)
  }
  open() {
    this.dialogService.open(DetailsCustomerOrderComponent, {
      context: {
        title: 'This is a title passed to the dialog component',
        celldata:this.rowData
      },
    });
  }

}

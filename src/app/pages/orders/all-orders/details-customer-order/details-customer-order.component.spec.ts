import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsCustomerOrderComponent } from './details-customer-order.component';

describe('DetailsCustomerOrderComponent', () => {
  let component: DetailsCustomerOrderComponent;
  let fixture: ComponentFixture<DetailsCustomerOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailsCustomerOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsCustomerOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

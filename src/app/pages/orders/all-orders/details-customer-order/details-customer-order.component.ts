import { Component, OnInit ,Input} from '@angular/core';
import { NbDialogRef } from '@nebular/theme';

@Component({
  selector: 'ngx-details-customer-order',
  templateUrl: './details-customer-order.component.html',
  styleUrls: ['./details-customer-order.component.scss']
})
export class DetailsCustomerOrderComponent implements OnInit {

  celldata: any;
  isVendor:boolean=false;

  constructor(protected ref: NbDialogRef<DetailsCustomerOrderComponent>) { 

    }
    @Input() title: string;

  ngOnInit(): void {
    var currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if(currentUser.roles[0]=="Vendor"){
      this.isVendor=true;
    }
    else{
      this.isVendor=false;
    }
  }
  dismiss() {
    this.ref.close();
  }

}



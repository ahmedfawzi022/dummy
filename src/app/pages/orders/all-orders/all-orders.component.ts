import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ServerDataSource } from 'ng2-smart-table';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { SmartTableData } from '../../../@core/data/smart-table';
import { DomSanitizer } from '@angular/platform-browser';
import { FirstNameCustomerOrderComponent } from '../all-orders/first-name-customer-order/first-name-customer-order.component';
import { LastNameCustomerOrderComponent } from '../all-orders/last-name-customer-order/last-name-customer-order.component';
import { PhoneNumberCustomerOrderComponent } from '../all-orders/phone-number-customer-order/phone-number-customer-order.component';
import { DetailsCustomerOrderComponent } from '../all-orders/details-customer-order/details-customer-order.component';
import { ActionsAllOrdersComponent } from '../all-orders/actions-all-orders/actions-all-orders.component';
import { NbDateService } from '@nebular/theme';

@Component({
  selector: 'ngx-all-orders',
  templateUrl: './all-orders.component.html',
  styleUrls: ['./all-orders.component.scss']
})
export class AllOrdersComponent implements OnInit {
  from:any;
  to:any;
  loading:any;
  fubUrl:any;
  currentUser:any;
  settings = {
    mode: 'external',
    actions:{
      add:false,
      edit:false,
      delete:false
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      // _id: {
      //   title: 'Order Id',
      //   type: 'string',
      //   filter:false

      // },
      // createdAt: {
      //   title: 'Date Created',
      //   type: 'string',
      //   filter:false
      // },
      trackingNumber: {
        title: 'Order Id',
        type: 'string',
        filter:true,
      },
      
      formattedDate: {
        title: 'Date Created',
        type: 'string',
        filter: true,

      },
      firstNamecustomer: {
        title: 'First Name',
        type: 'custom',
        renderComponent: FirstNameCustomerOrderComponent,
        editor: {
          type: 'custom',
          component: FirstNameCustomerOrderComponent
        },
        sort: false,
        filter: false,
      },
      lastNamecustomer: {
        title: 'Last Name',
        type: 'custom',
        renderComponent: LastNameCustomerOrderComponent,
        editor: {
          type: 'custom',
          component: LastNameCustomerOrderComponent
        },
        sort: false,
        filter: false,
      },
      phoneNumbercustomer: {
        title: 'Phone Number',
        type: 'custom',
        renderComponent: PhoneNumberCustomerOrderComponent,
        editor: {
          type: 'custom',
          component: PhoneNumberCustomerOrderComponent
        },
        sort: false,
        filter: false,
      },
      
      orderStatus: {
      title: 'Status',
      filter: false,
      type: 'html',
      valuePrepareFunction: (orderStatus) => {
        if(orderStatus == 1){
          return 'Pending'
         }
         else if(orderStatus == 2){
          return 'Rejected'
         }
         else if(orderStatus == 3){
          return 'Accepted'
         }
         else if(orderStatus == 4){
          return 'In-Progress'
         }
         else if(orderStatus == 5){
          return 'Delivered'
         }
      },
    },
      relayAction: {
        title: 'Call To Actions',
        type: 'custom',
        renderComponent: ActionsAllOrdersComponent,
        editor: {
          type: 'custom',
          component: ActionsAllOrdersComponent
        },
        
        sort: false,
        filter: false,
        width: '140px',

      },
    //   imageUrl: {
    //     title: 'Details',
    //     filter: false,
    //     type: 'html',
    //     valuePrepareFunction: (imageUrl) => {
    //         return this._domSanitizer.bypassSecurityTrustHtml(`<button type='submit' nbButton  status="danger">View Details</button>`);
    //     },
    // },
    // age: {
    //   title: 'Status',
    //   filter: false,
    //   type: 'html',
    //   valuePrepareFunction: (imageUrl) => {
    //       return this._domSanitizer.bypassSecurityTrustHtml(`<button type='button' class='btn btn-primary'>Change Status</button>`);
    //   },
    // },
  
  }
  };

  source: ServerDataSource

  constructor(public datepipe: DatePipe,protected dateService: NbDateService<Date>,private service: SmartTableData,public httpClient:HttpClient, private router: Router, private _domSanitizer: DomSanitizer) {
    this.fubUrl = environment.FUBS_API_URL
  }
  ngOnInit(): void {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if(this.currentUser.roles[0]=="Vendor"){
      this.getserviceForVendor()
    }else{
      this.getservice();
    }  
  
  }
  onSearch(){
    this.loading = true;
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if(this.currentUser.roles[0]=="Vendor"){
    this.source = new ServerDataSource(this.httpClient, {
      endPoint: `${this.fubUrl}/orders/admin?from=${this.datepipe.transform(this.from, 'yyyy-MM-dd')}&to=${this.datepipe.transform(this.to, 'yyyy-MM-dd')}`,
      pagerPageKey: 'page',
      pagerLimitKey: 'limit',
      filterFieldKey: '#field#_like',
      totalKey: 'data.count',
      dataKey: 'data.list',
    }
    );
  }else{
    this.source = new ServerDataSource(this.httpClient, {
      endPoint: `${this.fubUrl}/shipments/vendor?from=${this.datepipe.transform(this.from, 'yyyy-MM-dd')}&to=${this.datepipe.transform(this.to, 'yyyy-MM-dd')}`,
      pagerPageKey: 'page',
      pagerLimitKey: 'limit',
      filterFieldKey: '#field#_like',
      totalKey: 'data.count',
      dataKey: 'data.list',
    }
    );
  }
    this.loading = false;
  }
  getservice() {
    this.loading = true;
    this.source = new ServerDataSource(this.httpClient, {
      endPoint: `${this.fubUrl}/orders/admin`,
      pagerPageKey: 'page',
      pagerLimitKey: 'limit',
      filterFieldKey: '#field#_like',
      totalKey: 'data.count',
      dataKey: 'data.list',
    }
    );
    this.loading = false;

  }

  getserviceForVendor() {
    this.loading = true;
    this.source = new ServerDataSource(this.httpClient, {
      endPoint: `${this.fubUrl}/shipments/vendor`,
      pagerPageKey: 'page',
      pagerLimitKey: 'limit',
      filterFieldKey: '#field#_like',
      totalKey: 'data.count',
      dataKey: 'data.list',
    }
    );
    this.loading = false;

  }


}





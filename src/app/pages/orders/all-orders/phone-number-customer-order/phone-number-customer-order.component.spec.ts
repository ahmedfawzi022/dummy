import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhoneNumberCustomerOrderComponent } from './phone-number-customer-order.component';

describe('PhoneNumberCustomerOrderComponent', () => {
  let component: PhoneNumberCustomerOrderComponent;
  let fixture: ComponentFixture<PhoneNumberCustomerOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhoneNumberCustomerOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhoneNumberCustomerOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FirstNameCustomerOrderComponent } from './first-name-customer-order.component';

describe('FirstNameCustomerOrderComponent', () => {
  let component: FirstNameCustomerOrderComponent;
  let fixture: ComponentFixture<FirstNameCustomerOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FirstNameCustomerOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FirstNameCustomerOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

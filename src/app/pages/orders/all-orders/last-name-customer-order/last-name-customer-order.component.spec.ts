import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LastNameCustomerOrderComponent } from './last-name-customer-order.component';

describe('LastNameCustomerOrderComponent', () => {
  let component: LastNameCustomerOrderComponent;
  let fixture: ComponentFixture<LastNameCustomerOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LastNameCustomerOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LastNameCustomerOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

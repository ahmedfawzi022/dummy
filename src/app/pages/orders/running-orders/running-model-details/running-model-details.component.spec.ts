import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RunningModelDetailsComponent } from './running-model-details.component';

describe('RunningModelDetailsComponent', () => {
  let component: RunningModelDetailsComponent;
  let fixture: ComponentFixture<RunningModelDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RunningModelDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RunningModelDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

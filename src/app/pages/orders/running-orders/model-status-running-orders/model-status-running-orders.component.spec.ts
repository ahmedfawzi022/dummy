import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModelStatusRunningOrdersComponent } from './model-status-running-orders.component';

describe('ModelStatusRunningOrdersComponent', () => {
  let component: ModelStatusRunningOrdersComponent;
  let fixture: ComponentFixture<ModelStatusRunningOrdersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModelStatusRunningOrdersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelStatusRunningOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

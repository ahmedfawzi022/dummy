import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LastNameCustomerRunningOrdersComponent } from './last-name-customer-running-orders.component';

describe('LastNameCustomerRunningOrdersComponent', () => {
  let component: LastNameCustomerRunningOrdersComponent;
  let fixture: ComponentFixture<LastNameCustomerRunningOrdersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LastNameCustomerRunningOrdersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LastNameCustomerRunningOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit,Input } from '@angular/core';
import { ServerDataSource } from 'ng2-smart-table';
import { Router } from '@angular/router';
import { FirstNameCustomerRunningOrdersComponent } from '../running-orders/first-name-customer-running-orders/first-name-customer-running-orders.component';
import { LastNameCustomerRunningOrdersComponent } from '../running-orders/last-name-customer-running-orders/last-name-customer-running-orders.component';
import { PhoneNumberCustomerRunningOrdersComponent } from '../running-orders/phone-number-customer-running-orders/phone-number-customer-running-orders.component';
import { ModelStatusRunningOrdersComponent } from '../running-orders/model-status-running-orders/model-status-running-orders.component';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';

import { SmartTableData } from '../../../@core/data/smart-table';
import { DomSanitizer } from '@angular/platform-browser';
import { RunningActionButtonComponent } from '../running-orders/running-action-button/running-action-button.component';
@Component({
  selector: 'ngx-running-orders',
  templateUrl: './running-orders.component.html',
  styleUrls: ['./running-orders.component.scss']
})
export class RunningOrdersComponent implements OnInit {
  @Input() inputCustomerId;
  @Input() inputVendorId;
  @Input() inputDashboard;


  loading:any;
  fubUrl:any;
  settings = {
    mode: 'external',
    actions:{
      add:false,
      edit:false,
      delete:false
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      // _id: {
      //   title: 'Order Id',
      //   type: 'string',
      //   filter:false

      // },
      // createdAt: {
      //   title: 'Date Created',
      //   type: 'string',
      //   filter:false
      // },
      trackingNumber: {
        title: 'Order Id',
        type: 'string',
        filter:true,
      },
      
      formattedDate: {
        title: 'Date Created',
        type: 'string',
        filter: true,

      },
      firstNamecustomer: {
        title: 'First Name',
        type: 'custom',
        renderComponent: FirstNameCustomerRunningOrdersComponent,
        editor: {
          type: 'custom',
          component: FirstNameCustomerRunningOrdersComponent
        },
        sort: false,
        filter: false,
      },
      lastNamecustomer: {
        title: 'Last Name',
        type: 'custom',
        renderComponent: LastNameCustomerRunningOrdersComponent,
        editor: {
          type: 'custom',
          component: LastNameCustomerRunningOrdersComponent
        },
        sort: false,
        filter: false,
      },
      phoneNumbercustomer: {
        title: 'Phone Number',
        type: 'custom',
        renderComponent: PhoneNumberCustomerRunningOrdersComponent,
        editor: {
          type: 'custom',
          component: PhoneNumberCustomerRunningOrdersComponent
        },
        sort: false,
        filter: false,
      },
      relayAction: {
        title: 'Call To Actions',
        type: 'custom',
        renderComponent: RunningActionButtonComponent,
        editor: {
          type: 'custom',
          component: RunningActionButtonComponent
        },
        
        sort: false,
        filter: false,
        width: '340px',

      },
    //   imageUrl: {
    //     title: 'Details',
    //     filter: false,
    //     type: 'html',
    //     valuePrepareFunction: (imageUrl) => {
    //         return this._domSanitizer.bypassSecurityTrustHtml(`<button type='submit' nbButton  status="danger">View Details</button>`);
    //     },
    // },
    // age: {
    //   title: 'Status',
    //   filter: false,
    //   type: 'html',
    //   valuePrepareFunction: (imageUrl) => {
    //       return this._domSanitizer.bypassSecurityTrustHtml(`<button type='button' class='btn btn-primary'>Change Status</button>`);
    //   },
    // },
  
  }
  };

  source: ServerDataSource

  constructor(private service: SmartTableData,public httpClient:HttpClient, private router: Router, private _domSanitizer: DomSanitizer) {
    this.fubUrl = environment.FUBS_API_URL
  }
  ngOnInit(): void {
    var currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if(currentUser.roles[0]=="Vendor"){
      this.getserviceForVendor()
    }else{
      if(this.inputCustomerId){
        this.getserviceByCustomerId(this.inputCustomerId)
      }
      else if(this.inputVendorId){
        this.getserviceByCustomerId(this.inputVendorId)

      }
      else{
        this.getservice();
      }    
    } 
   }

  getservice() {
    this.loading = true;
    this.source = new ServerDataSource(this.httpClient, {
      endPoint: `${this.fubUrl}/orders/admin?orderStatus=5`,
      pagerPageKey: 'page',
      pagerLimitKey: 'limit',
      filterFieldKey: '#field#_like',
      totalKey: 'data.count',
      dataKey: 'data.list',
    }
    );
    this.loading = false;

  }
  getserviceByCustomerId(id) {
    this.loading = true;
    this.source = new ServerDataSource(this.httpClient, {
      endPoint: `${this.fubUrl}/shipments/admin?orderStatus=5&&customerId=${id}`,
      pagerPageKey: 'page',
      pagerLimitKey: 'limit',
      filterFieldKey: '#field#_like',
      totalKey: 'data.count',
      dataKey: 'data.list',
    }
    );
    this.loading = false;

  }
  getserviceByVendorId(id) {
    this.loading = true;
    this.source = new ServerDataSource(this.httpClient, {
      endPoint: `${this.fubUrl}/shipments/admin?orderStatus=5&&vendorId=${id}`,
      pagerPageKey: 'page',
      pagerLimitKey: 'limit',
      filterFieldKey: '#field#_like',
      totalKey: 'data.count',
      dataKey: 'data.list',
    }
    );
    this.loading = false;

  }
  getserviceForVendor(){
    this.loading = true;
    this.source = new ServerDataSource(this.httpClient, {
      endPoint: `${this.fubUrl}/shipments/vendor?shipmentStatus=5`,
      pagerPageKey: 'page',
      pagerLimitKey: 'limit',
      filterFieldKey: '#field#_like',
      totalKey: 'data.count',
      dataKey: 'data.list',
    }
    );
    this.loading = false;
  }


}





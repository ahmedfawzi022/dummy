import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhoneNumberCustomerRunningOrdersComponent } from './phone-number-customer-running-orders.component';

describe('PhoneNumberCustomerRunningOrdersComponent', () => {
  let component: PhoneNumberCustomerRunningOrdersComponent;
  let fixture: ComponentFixture<PhoneNumberCustomerRunningOrdersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhoneNumberCustomerRunningOrdersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhoneNumberCustomerRunningOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

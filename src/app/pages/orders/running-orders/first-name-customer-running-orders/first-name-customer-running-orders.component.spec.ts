import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FirstNameCustomerRunningOrdersComponent } from './first-name-customer-running-orders.component';

describe('FirstNameCustomerRunningOrdersComponent', () => {
  let component: FirstNameCustomerRunningOrdersComponent;
  let fixture: ComponentFixture<FirstNameCustomerRunningOrdersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FirstNameCustomerRunningOrdersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FirstNameCustomerRunningOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

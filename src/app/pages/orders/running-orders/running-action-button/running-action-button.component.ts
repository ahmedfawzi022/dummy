import { Component, OnInit,Input } from '@angular/core';
import { NbDialogService } from '@nebular/theme';
import { RunningModelDetailsComponent } from '../running-model-details/running-model-details.component';
import { ModelStatusRunningOrdersComponent } from '../model-status-running-orders/model-status-running-orders.component';
import { DefaultEditor, ViewCell } from 'ng2-smart-table';
import { UsersService } from '../../../../services/users/users.service';
import { Router } from '@angular/router';
import { DataService } from '../../../../services/data/data.service';
@Component({
  selector: 'ngx-running-action-button',
  templateUrl: './running-action-button.component.html',
  styleUrls: ['./running-action-button.component.scss']
})
export class RunningActionButtonComponent extends DefaultEditor implements OnInit, ViewCell {
  constructor(private dialogService: NbDialogService,public  data:DataService,public usersService:UsersService, private router: Router) { super()}
  @Input() value: string | number;
  @Input() rowData: any;
  isVendor:boolean=false;
  loading:any;
  ngOnInit(): void {
    console.log(this.rowData)
    console.log(this.value)
    var currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if(currentUser.roles[0]=="Vendor"){
      this.isVendor=true;
    }else{
      this.isVendor=false;
    }
  }
  open() {
    console.log(this.rowData)
    this.dialogService.open(RunningModelDetailsComponent, {
      context: {
        title: 'This is a title passed to the dialog component',
        celldata:this.rowData
      },
    });
  }
  changeStatus() {
    this.dialogService.open(ModelStatusRunningOrdersComponent, {
      context: {
        title: 'This is a title passed to the dialog component',
        celldata:this.rowData

      },
    });
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RunningActionButtonComponent } from './running-action-button.component';

describe('RunningActionButtonComponent', () => {
  let component: RunningActionButtonComponent;
  let fixture: ComponentFixture<RunningActionButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RunningActionButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RunningActionButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

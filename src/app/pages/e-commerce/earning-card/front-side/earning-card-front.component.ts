import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { interval, Subscription } from 'rxjs';
import { switchMap, takeWhile } from 'rxjs/operators';
import { LiveUpdateChart, EarningData } from '../../../../@core/data/earning';
import { UsersService } from '../../../../services/users/users.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'ngx-earning-card-front',
  styleUrls: ['./earning-card-front.component.scss'],
  templateUrl: './earning-card-front.component.html',
})
export class EarningCardFrontComponent implements OnDestroy, OnInit {
  private alive = true;
  isVendor:any;
  loading: any;
  totalCustomer: any;
  totalOrders:any;
  @Input() inputTypeChild;
  @Input() selectedType: string = 'Bitcoin';
  @Input() selectedType2: string = 'Bitcoin';
  fromDate: any;
  toDate: any;
  intervalSubscription: Subscription;
  currencies: string[] = ['Today', 'Week', 'Month'];
  currentTheme: string;
  earningLiveUpdateCardData: LiveUpdateChart;
  liveUpdateChartData: { value: [string, number] }[];

  constructor(private themeService: NbThemeService, public usersService: UsersService, public datepipe: DatePipe,
    private earningService: EarningData) {
    this.themeService.getJsTheme()
      .pipe(takeWhile(() => this.alive))
      .subscribe(theme => {
        this.currentTheme = theme.name;
      });
  }

  ngOnInit() {
    var currentUser = JSON.parse(localStorage.getItem('currentUser'));
    console.log(this.inputTypeChild)
    var today = new Date();
    this.fromDate = this.datepipe.transform(today, 'yyyy-MM-dd');
    this.toDate = this.datepipe.transform(today, 'yyyy-MM-dd');
    if(currentUser.roles[0]=="Vendor"){
      this.isVendor=true
      this.getTotalOrdersServiceVendor();
    }else{
      this.isVendor=false
      this.getTotalCustomersService();
      this.getTotalOrdersService();

    }


     this.getEarningCardData(this.selectedType);
  }

  changeCurrency(type, typeMenu) {
    if (this.selectedType !== type) {
      this.selectedType = type;
      console.log(this.selectedType);
      console.log(typeMenu)
      // [(ngmodel)]
      this.getTotalCustomers(this.selectedType, typeMenu);
    }
  }

  private getEarningCardData(currency) {
    this.earningService.getEarningCardData(currency)
      .pipe(takeWhile(() => this.alive))
      .subscribe((earningLiveUpdateCardData: LiveUpdateChart) => {
        // this.earningLiveUpdateCardData = earningLiveUpdateCardData;
        // this.liveUpdateChartData = earningLiveUpdateCardData.liveChart;

        this.startReceivingLiveData(currency);
      });
  }

  startReceivingLiveData(currency) {
    if (this.intervalSubscription) {
      this.intervalSubscription.unsubscribe();
    }

    this.intervalSubscription = interval(200)
      .pipe(
        takeWhile(() => this.alive),
        switchMap(() => this.earningService.getEarningLiveUpdateCardData(currency)),
      )
      .subscribe((liveUpdateChartData: any[]) => {
        this.liveUpdateChartData = [...liveUpdateChartData];
      });
  }

  getTotalCustomers(selected, menu) {
    if (menu == 'customers') {


      if (selected == 'Week') {
        var current = new Date();     // get current date    
        var weekstart = current.getDate() - current.getDay() + 1;
        var weekend = weekstart + 6;       // end day is the first day + 6 
        var firstday = new Date(current.setDate(weekstart));
        var lastday = new Date(current.setDate(weekend));
        this.fromDate = this.datepipe.transform(firstday, 'yyyy-MM-dd');
        this.toDate = this.datepipe.transform(lastday, 'yyyy-MM-dd');
      } else if (selected == 'Month') {
        var date = new Date();
        var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
        var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
        this.fromDate = this.datepipe.transform(firstDay, 'yyyy-MM-dd');
        this.toDate = this.datepipe.transform(lastDay, 'yyyy-MM-dd');

      } else if (selected == 'Today') {
        var today = new Date();
        this.fromDate = this.datepipe.transform(today, 'yyyy-MM-dd');
        this.toDate = this.datepipe.transform(today, 'yyyy-MM-dd');

      }
      this.getTotalCustomersService();

    } else {
  
      
      if (selected == 'Week') {
        var current = new Date();     // get current date    
        var weekstart = current.getDate() - current.getDay() + 1;
        var weekend = weekstart + 6;       // end day is the first day + 6 
        var firstday = new Date(current.setDate(weekstart));
        var lastday = new Date(current.setDate(weekend));
        this.fromDate = this.datepipe.transform(firstday, 'yyyy-MM-dd');
        this.toDate = this.datepipe.transform(lastday, 'yyyy-MM-dd');
      } else if (selected == 'Month') {
        var date = new Date();
        var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
        var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
        this.fromDate = this.datepipe.transform(firstDay, 'yyyy-MM-dd');
        this.toDate = this.datepipe.transform(lastDay, 'yyyy-MM-dd');

      } else if (selected == 'Today') {
        var today = new Date();
        this.fromDate = this.datepipe.transform(today, 'yyyy-MM-dd');
        this.toDate = this.datepipe.transform(today, 'yyyy-MM-dd');

      }
      this.getTotalOrdersService();


    }
     this.loading=true;
    

  }

  getTotalCustomersService(){
    this.usersService.getTotalCustomers(this.fromDate,this.toDate).subscribe(
      data => {
        console.log(data.data)
        this.totalCustomer=data.data.totalCustomers

      },
      error => {
        this.loading=false;

        try {
        } catch (err) {
        }
      },
    );
  }
  getTotalOrdersService(){
    this.usersService.getTotalOrders(this.fromDate,this.toDate).subscribe(
      data => {
        console.log(data.data)
        this.totalOrders=data.data.totalOrders

      },
      error => {
        this.loading=false;

        try {
        } catch (err) {
        }
      },
    );
  }
  getTotalOrdersServiceVendor(){
    this.usersService.getTotalOrdersVendor(this.fromDate,this.toDate).subscribe(
      data => {
        console.log(data.data)
        this.totalOrders=data.data.totalShipments

      },
      error => {
        this.loading=false;

        try {
        } catch (err) {
        }
      },
    );
  }

  ngOnDestroy() {
    this.alive = false;
  }
}

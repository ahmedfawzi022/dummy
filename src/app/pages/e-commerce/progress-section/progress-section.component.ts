import { Component, OnDestroy,OnInit } from '@angular/core';
import { ProgressInfo, StatsProgressBarData } from '../../../@core/data/stats-progress-bar';
import { takeWhile } from 'rxjs/operators';
import { OrdersService } from '../../../services/orders/orders.service';


@Component({
  selector: 'ngx-progress-section',
  styleUrls: ['./progress-section.component.scss'],
  templateUrl: './progress-section.component.html',
})
export class ECommerceProgressSectionComponent implements OnInit, OnDestroy {
  loading:any;
  private alive = true;
  totalRevenue:any;
  totalCollected:any;
  isVendor:any;

  progressInfoData: ProgressInfo[];

  constructor(public ordersService:OrdersService,private statsProgressBarService: StatsProgressBarData) {

    this.progressInfoData=[
      {
        title: 'Total Revenue This Month',
        value: 572900,
        activeProgress: 30,
        description: 'This month',
      },
      {
        title: 'Total Collected Money',
        value: 6378,
        activeProgress: 60,
        description: 'money',
      }
      // {
      //   title: 'New Comments',
      //   value: 200,
      //   activeProgress: 55,
      //   description: 'Better than last week (55%)',
      // },
    ];
    // this.statsProgressBarService.getProgressInfoData()
    //   .pipe(takeWhile(() => this.alive))
    //   .subscribe((data) => {
    //     this.progressInfoData = data;
    //   });
  }
  ngOnInit(): void {
    var currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if(currentUser.roles[0]=="Vendor"){
      this.isVendor=true

      this.getTotalRevenueVendor();
      this.getTotalCollectedVendor();
    }else{
      this.isVendor=false

      this.getTotalRevenue();
      this.getTotalCollected();
    }


  }
  getTotalRevenue(){
    this.loading=true;

    this.ordersService.getTotalRevenue().subscribe(
      data => {
        console.log(data.data)
        this.totalRevenue=data.data.totalRevenue


  //      this.isActiveControl.setValue(data.data.isActive)
        // this.categories.isActive=this.isActiveControl.value

        // this.router.navigateByUrl(`/pages/categories/categorie`);


        console.log(data)
        // console.log(token)

      },
      error => {
        this.loading=false;

        // this.spinnerService.hide();
        try {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.loginerrorMessage);
        } catch (err) {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.errorMessage);
        }
      },
    );
  }
  getTotalCollected(){
    this.loading=true;

    this.ordersService.getTotalCollected().subscribe(
      data => {
        console.log(data.data)
        this.totalCollected=data.data.total


  //      this.isActiveControl.setValue(data.data.isActive)
        // this.categories.isActive=this.isActiveControl.value

        // this.router.navigateByUrl(`/pages/categories/categorie`);


        console.log(data)
        // console.log(token)

      },
      error => {
        this.loading=false;

        // this.spinnerService.hide();
        try {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.loginerrorMessage);
        } catch (err) {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.errorMessage);
        }
      },
    );
  }
  getTotalRevenueVendor(){
    this.loading=true;

    this.ordersService.getTotalRevenueVendor().subscribe(
      data => {
        console.log(data.data)
        this.totalRevenue=data.data.totalShipments


  //      this.isActiveControl.setValue(data.data.isActive)
        // this.categories.isActive=this.isActiveControl.value

        // this.router.navigateByUrl(`/pages/categories/categorie`);


        console.log(data)
        // console.log(token)

      },
      error => {
        this.loading=false;

        // this.spinnerService.hide();
        try {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.loginerrorMessage);
        } catch (err) {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.errorMessage);
        }
      },
    );
  }
  getTotalCollectedVendor(){
    this.loading=true;

    this.ordersService.getTotalCollectedVendor().subscribe(
      data => {
        console.log(data.data)
        this.totalCollected=data.data.total


  //      this.isActiveControl.setValue(data.data.isActive)
        // this.categories.isActive=this.isActiveControl.value

        // this.router.navigateByUrl(`/pages/categories/categorie`);


        console.log(data)
        // console.log(token)

      },
      error => {
        this.loading=false;

        // this.spinnerService.hide();
        try {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.loginerrorMessage);
        } catch (err) {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.errorMessage);
        }
      },
    );
  }
  ngOnDestroy() {
    this.alive = true;
  }
}



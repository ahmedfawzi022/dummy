import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RunningOrdersDashboardComponent } from './running-orders-dashboard.component';

describe('RunningOrdersDashboardComponent', () => {
  let component: RunningOrdersDashboardComponent;
  let fixture: ComponentFixture<RunningOrdersDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RunningOrdersDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RunningOrdersDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

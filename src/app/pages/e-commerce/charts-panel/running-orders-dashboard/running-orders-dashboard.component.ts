import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { Router } from '@angular/router';

import { SmartTableData } from '../../../../@core/data/smart-table';
import { DomSanitizer } from '@angular/platform-browser';
@Component({
  selector: 'ngx-running-orders-dashboard',
  templateUrl: './running-orders-dashboard.component.html',
  styleUrls: ['./running-orders-dashboard.component.scss']
})
export class RunningOrdersDashboardComponent implements OnInit {

  settings = {
    mode: 'external',
    actions:{
      add:false,
      edit:false,
      delete:false
    },
    pager:{
      display:true,
      perPage: 6
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'Order Id',
        type: 'string',
      },
      date: {
        title: 'Date Created',
        type: 'string',
      },
      name: {
        title: 'Customer Name',
        type: 'string',
      },
      phone: {
        title: 'Phone Number',
        type: 'string',
      },
      firstname: {
        title: 'Vendor Name',
        type: 'string',
      },
      vendorPhone: {
        title: 'Vendor Number',
        type: 'string',
      },
      // relayAction: {
      //   title: 'Actions',
      //   type: 'custom',
      //   renderComponent: PendingActionsButtonComponent,
      //   editor: {
      //     type: 'custom',
      //     component: PendingActionsButtonComponent
      //   },
        
      //   sort: false,
      //   filter: false,
      //   width: '340px'
      // },
    //   imageUrl: {
    //     title: 'Details',
    //     filter: false,
    //     type: 'html',
    //     valuePrepareFunction: (imageUrl) => {
    //         return this._domSanitizer.bypassSecurityTrustHtml(`<button type='submit' nbButton  status="danger">View Details</button>`);
    //     },
    // },
    // age: {
    //   title: 'Status',
    //   filter: false,
    //   type: 'html',
    //   valuePrepareFunction: (imageUrl) => {
    //       return this._domSanitizer.bypassSecurityTrustHtml(`<button type='button' class='btn btn-primary'>Change Status</button>`);
    //   },
    // },
  
  }
  };

  source: LocalDataSource = new LocalDataSource();

  constructor(private service: SmartTableData, private router: Router, private _domSanitizer: DomSanitizer) {
    const data = this.service.getData();
    this.source.load(data);
  }
  ngOnInit(): void {
  }
  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }

  onAdd(){
    this.router.navigateByUrl('/pages/notifications/add-notification');

  }
}






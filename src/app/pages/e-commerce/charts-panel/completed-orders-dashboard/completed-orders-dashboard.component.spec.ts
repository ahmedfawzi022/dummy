import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompletedOrdersDashboardComponent } from './completed-orders-dashboard.component';

describe('CompletedOrdersDashboardComponent', () => {
  let component: CompletedOrdersDashboardComponent;
  let fixture: ComponentFixture<CompletedOrdersDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompletedOrdersDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompletedOrdersDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

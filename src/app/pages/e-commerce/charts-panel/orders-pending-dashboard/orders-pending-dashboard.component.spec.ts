import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdersPendingDashboardComponent } from './orders-pending-dashboard.component';

describe('OrdersPendingDashboardComponent', () => {
  let component: OrdersPendingDashboardComponent;
  let fixture: ComponentFixture<OrdersPendingDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrdersPendingDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrdersPendingDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

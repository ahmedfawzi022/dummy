import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';

import { SmartTableData } from '../../../@core/data/smart-table';
import { Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
@Component({
  selector: 'ngx-subscribers',
  templateUrl: './subscribers.component.html',
  styleUrls: ['./subscribers.component.scss']
})
export class SubscribersComponent implements OnInit {

  settings = {
    mode: 'external',

    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    actions:{
      add:false,
      edit:false,
      delete:false
    },
    pager:{
      display:false,
      perPage: 10
    },
    columns: {
      imageUrl: {
        title: 'Profile Picture',
        filter: false,
        type: 'html',
        valuePrepareFunction: (imageUrl) => {
            return this._domSanitizer.bypassSecurityTrustHtml(`<img src="../../../../assets/images/user.png" alt="Smiley face" height="50" width="50">`);
        },
    },
      firstName: {
        title: 'Full Name',
        type: 'string',
      },
      lastName: {
        title: 'User Name',
        type: 'string',
      },
      phone: {
        title: 'Phone Number',
        type: 'string',
      },
      email: {
        title: 'E-mail',
        type: 'string',
      }
    },
  };

  source: LocalDataSource = new LocalDataSource();

  constructor(private service: SmartTableData, private router: Router, private _domSanitizer: DomSanitizer) {
    const data = this.service.getData();
    this.source.load(data);
  }
  ngOnInit(): void {
  }
  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }

  onAdd(){
    this.router.navigateByUrl('/pages/users/add-operator');

  }
}





import { Component,OnInit } from '@angular/core';
import { DataService } from '../../services/data/data.service';
import { BaseComponent } from '../../../app/pages/general/base/base.component';
import { NbToastrService } from '@nebular/theme';

@Component({
  selector: 'ngx-ecommerce',
  templateUrl: './e-commerce.component.html',
})
export class ECommerceComponent extends BaseComponent implements OnInit {
  messageLogin:any;
  isVendor:any;
  constructor(public data: DataService,public toastrService: NbToastrService){
    super(toastrService);

  }
  ngOnInit() {
    var currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if(currentUser.roles[0]=="Vendor"){
      this.isVendor=true;
    }
    else{
      this.isVendor=false;
    }
    this.data.currentLogin.subscribe(login => {
      this.messageLogin = login
      if(localStorage.getItem('signinValues')){
      this.displayMessageLogin()
      localStorage.removeItem('signinValues')
      }
    })

  }

  displayMessageLogin(){
    this.status='success'
    //  let token = JWT(user.data.token)
    let userData =JSON.parse(localStorage.getItem('signinValues'));
    console.log(userData);
    this.title='welcome '+userData.userName;
    this.content='TO FUBS ADMIN PANEL'

    this.showToast(this.status, this.title, this.content);
  }

}

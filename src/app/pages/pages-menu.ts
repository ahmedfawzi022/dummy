import { NbMenuItem } from '@nebular/theme';

export let MENU_ITEMS: NbMenuItem[]=[];
  MENU_ITEMS =[
    {
    title: 'Dashboard',
    icon: 'shopping-cart-outline',
    link: '/pages/dashboard',
    home: true,
  },
  {
    title: 'Users',
    icon: 'people-outline',
    children: [
      {
        title: 'Administrators',
        link: '/pages/users/administrator',
      },
      {
        title: 'Operators',
        link: '/pages/users/operator',
      },
      {
        title: 'Vendors',
        link: '/pages/operations/vendors',
      },
      {
        title: 'Customers',
        link: '/pages/operations/customers',
      },
    ],
  },
  {
    title: 'Categories',
    icon: 'copy-outline',
    children: [
      {
        title: 'Categories',
        link: '/pages/categories/categorie',
      },
      {
        title: 'Sub Categories',
        link: '/pages/categories/add-sub-categorie',
      },
    ],
  },
  {
    title: 'Locations',
    icon: 'map-outline',
    children: [
      {
        title: 'Countries',
        link: '/pages/locations/countries',
      },
      {
        title: 'Cities',
        link: '/pages/locations/cities',
      },
      {
        title: 'Districts',
        link: '/pages/locations/districts',
      },
    ],
  },
  {
    title: 'Configuration',
    icon: 'settings-outline',
    children: [
      {
        title: 'Currencies',
        link: '/pages/locations/currencies',
      },
    ],
  },
  {
    title: 'Operations',
    icon: 'options-2-outline',
    children: [
      {
        title: 'Products',
        link: '/pages/operations/products',
      },
      {
        title: 'Shipment Type',
        link: '/pages/operations/shipmenttype',
      },
      {
        title: 'Banners',
        link: '/pages/operations/banners',
      },

    ],
  },
  {
    title: 'Orders',
    icon: 'options-2-outline',
    children: [
      {
        title: 'Pending Orders',
        link: '/pages/orders/pending-orders',
      },
      {
        title: 'Running Orders',
        link: '/pages/orders/running-orders',
      },
      {
        title: 'Completed Orders',
        link: '/pages/orders/complete-orders',
      },
      {
        title: 'Accept Orders',
        link: '/pages/orders/accept-orders',
      },
      {
        title: 'Reject Orders',
        link: '/pages/orders/reject-orders',
      },
    ],
  },
  {
    title: 'Reports',
    icon: 'options-2-outline',
    children: [
      {
        title: 'All Orders',
        link: '/pages/orders/all-orders',
      },
    ],
  },
  {
    title: 'Notifications',
    icon: 'bell-outline',
    link: '/pages/notifications/notification',
    home: false,
  },
  {
    title: 'Settings',
    icon: 'settings-2-outline',
    link: '/pages/settings/app-settings',
    home: false,
  },
 
];

export let MENU_ITEMS_Vendor: NbMenuItem[]=[];

  MENU_ITEMS_Vendor =[
    {
      title: 'Dashboard',
      icon: 'shopping-cart-outline',
      link: '/pages/dashboard',
      home: true,
    },
 



 
  {
    title: 'Operations',
    icon: 'options-2-outline',
    children: [
      {
        title: 'Products',
        link: '/pages/operations/products',
      }

    ],
  },
  {
    title: 'Orders',
    icon: 'options-2-outline',
    children: [
      {
        title: 'Pending Orders',
        link: '/pages/orders/pending-orders',
      },
      {
        title: 'Running Orders',
        link: '/pages/orders/running-orders',
      },
      {
        title: 'Completed Orders',
        link: '/pages/orders/complete-orders',
      },
      {
        title: 'Accept Orders',
        link: '/pages/orders/accept-orders',
      },
      {
        title: 'Reject Orders',
        link: '/pages/orders/reject-orders',
      },
    ],
  },
  {
    title: 'Reports',
    icon: 'options-2-outline',
    children: [
      {
        title: 'All Orders',
        link: '/pages/orders/all-orders',
      },
    ],
  },

 
];

export let MENU_ITEMS_Admin: NbMenuItem[]=[];
MENU_ITEMS_Admin =[
  {
  title: 'Dashboard',
  icon: 'shopping-cart-outline',
  link: '/pages/dashboard',
  home: true,
},
{
  title: 'Users',
  icon: 'people-outline',
  children: [
    {
      title: 'Operators',
      link: '/pages/users/operator',
    },
    {
      title: 'Vendors',
      link: '/pages/operations/vendors',
    },
    {
      title: 'Customers',
      link: '/pages/operations/customers',
    },
  ],
},
{
  title: 'Categories',
  icon: 'copy-outline',
  children: [
    {
      title: 'Categories',
      link: '/pages/categories/categorie',
    },
    {
      title: 'Sub Categories',
      link: '/pages/categories/add-sub-categorie',
    },
  ],
},
{
  title: 'Locations',
  icon: 'map-outline',
  children: [
    {
      title: 'Countries',
      link: '/pages/locations/countries',
    },
    {
      title: 'Cities',
      link: '/pages/locations/cities',
    },
    {
      title: 'Districts',
      link: '/pages/locations/districts',
    },
  ],
},
{
  title: 'Configuration',
  icon: 'settings-outline',
  children: [
    {
      title: 'Currencies',
      link: '/pages/locations/currencies',
    },
  ],
},
{
  title: 'Operations',
  icon: 'options-2-outline',
  children: [
    {
      title: 'Products',
      link: '/pages/operations/products',
    },
    {
      title: 'Shipment Type',
      link: '/pages/operations/shipmenttype',
    },

  ],
},
{
  title: 'Orders',
  icon: 'options-2-outline',
  children: [
    {
      title: 'Pending Orders',
      link: '/pages/orders/pending-orders',
    },
    {
      title: 'Running Orders',
      link: '/pages/orders/running-orders',
    },
    {
      title: 'Completed Orders',
      link: '/pages/orders/complete-orders',
    },
    {
      title: 'Accept Orders',
      link: '/pages/orders/accept-orders',
    },
    {
      title: 'Reject Orders',
      link: '/pages/orders/reject-orders',
    },
  ],
},
{
  title: 'Reports',
  icon: 'options-2-outline',
  children: [
    {
      title: 'All Orders',
      link: '/pages/orders/all-orders',
    },
  ],
},
{
  title: 'Notifications',
  icon: 'bell-outline',
  link: '/pages/notifications/notification',
  home: false,
},
{
  title: 'Settings',
  icon: 'settings-2-outline',
  link: '/pages/settings/app-settings',
  home: false,
},

];

export let MENU_ITEMS_Operator: NbMenuItem[]=[];
MENU_ITEMS_Operator =[
  {
  title: 'Dashboard',
  icon: 'shopping-cart-outline',
  link: '/pages/dashboard',
  home: true,
},
{
  title: 'Users',
  icon: 'people-outline',
  children: [
    {
      title: 'Vendors',
      link: '/pages/operations/vendors',
    },
    {
      title: 'Customers',
      link: '/pages/operations/customers',
    },
  ],
},
{
  title: 'Categories',
  icon: 'copy-outline',
  children: [
    {
      title: 'Categories',
      link: '/pages/categories/categorie',
    },
    {
      title: 'Sub Categories',
      link: '/pages/categories/add-sub-categorie',
    },
  ],
},
{
  title: 'Locations',
  icon: 'map-outline',
  children: [
    {
      title: 'Countries',
      link: '/pages/locations/countries',
    },
    {
      title: 'Cities',
      link: '/pages/locations/cities',
    },
    {
      title: 'Districts',
      link: '/pages/locations/districts',
    },
  ],
},
{
  title: 'Configuration',
  icon: 'settings-outline',
  children: [
    {
      title: 'Currencies',
      link: '/pages/locations/currencies',
    },
  ],
},
{
  title: 'Operations',
  icon: 'options-2-outline',
  children: [
    {
      title: 'Products',
      link: '/pages/operations/products',
    },
    {
      title: 'Shipment Type',
      link: '/pages/operations/shipmenttype',
    },

  ],
},
{
  title: 'Orders',
  icon: 'options-2-outline',
  children: [
    {
      title: 'Pending Orders',
      link: '/pages/orders/pending-orders',
    },
    {
      title: 'Running Orders',
      link: '/pages/orders/running-orders',
    },
    {
      title: 'Completed Orders',
      link: '/pages/orders/complete-orders',
    },
    {
      title: 'Accept Orders',
      link: '/pages/orders/accept-orders',
    },
    {
      title: 'Reject Orders',
      link: '/pages/orders/reject-orders',
    },
  ],
},
{
  title: 'Reports',
  icon: 'options-2-outline',
  children: [
    {
      title: 'All Orders',
      link: '/pages/orders/all-orders',
    },
  ],
},


];
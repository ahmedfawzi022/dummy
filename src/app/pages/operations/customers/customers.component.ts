import { Component, OnInit,Input,TemplateRef } from '@angular/core';
import { LocalDataSource,ServerDataSource } from 'ng2-smart-table';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DataService } from '../../../services/data/data.service';
import { NbDialogService } from '@nebular/theme';

import { SmartTableData } from '../../../@core/data/smart-table';
import { DomSanitizer } from '@angular/platform-browser';
import { CustomersActionButtonComponent } from '../customers/customers-action-button/customers-action-button.component';
import { environment } from '../../../../environments/environment';
import { UsersService } from '../../../services/users/users.service';


@Component({
  selector: 'ngx-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.scss']
})
export class CustomersComponent implements OnInit {
  @Input() inputSubscripe;
  rowData:any
  loading = false;
  fubUrl: any;
  settings = {
    mode: 'external',
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
    },
    columns: {
      profilePicture: {
        title: 'Profile Picture',
        filter: false,
        type: 'html',
        valuePrepareFunction: (profilePicture) => {
            return this._domSanitizer.bypassSecurityTrustHtml(`<img class="img-box" style="bo" src="${profilePicture}" />`);
        },
    },
      firstName: {
        title: 'First Name',
        type: 'string',
      },
      lastName: {
        title: 'Last Name',
        type: 'string',
      },
      userName: {
        title: 'User Name',
        type: 'string',
      },
      phoneNumber: {
        title: 'Phone Number',
        type: 'string',
      },
      email: {
        title: 'E-mail',
        type: 'string',
      },

      
      relayAction: {
        title: 'Actions',
        type: 'custom',
        renderComponent: CustomersActionButtonComponent,
        editor: {
          type: 'custom',
          component: CustomersActionButtonComponent
        },
        
        sort: false,
        filter: false,
        width: '150px'
      },

      // picture: {
      //   title: 'Picture',
      //   type: 'html',
      //   valuePrepareFunction: (picture:string) => { return `<img width="50px" src="${picture}" />`; },
      //   },
    },
  };
  

    source: ServerDataSource


  constructor(private dialogService: NbDialogService,public data:DataService,private service: SmartTableData,public usersService:UsersService, private httpClient: HttpClient,private router: Router, private _domSanitizer: DomSanitizer) {
    this.fubUrl = environment.FUBS_API_URL

  }
  ngOnInit(): void {
        this.data.currentMessage.subscribe(
      data => {
        this. getservice();
      });
    this. getservice();
  }
  dialogDelete(dialog: TemplateRef<any>,event) {
    console.log(event)
    this.rowData=event;
    this.dialogService.open(
      dialog,
      {
        context: 'this is some additional data passed to dialog',
        closeOnBackdropClick: false,
      });

  }
  onClickYes(){
    this.onDelete(this.rowData)
  }
  getservice() {
    this.loading = true;
    this.source = new ServerDataSource(this.httpClient, {
      endPoint: `${this.fubUrl}/users/customer`,
      pagerPageKey: 'page',
      pagerLimitKey: 'limit',
      filterFieldKey: '#field#',
      totalKey: 'data.count',
      dataKey: 'data.list',
    }
    );
    this.loading = false;

  }
  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
      this.onDelete(event)

    } else {
      event.confirm.reject();
    }
  }

  onAdd() {
    this.router.navigateByUrl('/pages/operations/add-customer');

  }
  onEdit(event){
    console.log(event.data._id)
    this.router.navigateByUrl(`/pages/operations/add-customer/${event.data._id}`);
  }
  onDelete(event) {
    this.usersService.deleteUser(event.data._id)

        .subscribe(
          data => {
            // this.del.nativeElement.click();
            this.getservice();
            // this.makeToast(this.configurationDataObject.feesDeleteSuccessfullMessage);

          },
          error => {
//            this.del.nativeElement.click();
            try {
              // if (error.error.errorCode)
              // this.openModal(error.error.errorCode, error.error.errorMessage);
              // this.openModal(this.configurationDataObject.error, this.configurationDataObject.errorMessage);
            } catch (err) {
              // this.openModal(this.configurationDataObject.error, this.configurationDataObject.errorMessage);
            }
          },
      );

  }
}






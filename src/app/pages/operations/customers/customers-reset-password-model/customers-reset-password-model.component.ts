import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ngx-customers-reset-password-model',
  templateUrl: './customers-reset-password-model.component.html',
  styleUrls: ['./customers-reset-password-model.component.scss']
})
export class CustomersResetPasswordModelComponent implements OnInit {
 @Input() title
 @Input()celldata:any;
  constructor() { }

  ngOnInit(): void {
  }

}

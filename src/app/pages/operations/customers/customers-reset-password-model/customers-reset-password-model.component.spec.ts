import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomersResetPasswordModelComponent } from './customers-reset-password-model.component';

describe('CustomersResetPasswordModelComponent', () => {
  let component: CustomersResetPasswordModelComponent;
  let fixture: ComponentFixture<CustomersResetPasswordModelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomersResetPasswordModelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomersResetPasswordModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

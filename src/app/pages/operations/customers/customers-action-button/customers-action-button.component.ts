import { Component, OnInit,Input } from '@angular/core';
import { NbDialogService } from '@nebular/theme';
import { DefaultEditor, ViewCell } from 'ng2-smart-table';
import { UsersService } from '../../../../services/users/users.service';
import { Router } from '@angular/router';
import { DataService } from '../../../../services/data/data.service';
import {
  NbComponentStatus,
  NbGlobalLogicalPosition,
  NbGlobalPhysicalPosition,
  NbGlobalPosition,
  NbToastrConfig,
  NbToastrService
} from '@nebular/theme';
import { CustomerOrdersDetailsComponent } from '../customer-orders-details/customer-orders-details.component';

import { CustomersResetPasswordModelComponent } from '../customers-reset-password-model/customers-reset-password-model.component';
@Component({
  selector: 'ngx-customers-action-button',
  templateUrl: './customers-action-button.component.html',
  styleUrls: ['./customers-action-button.component.scss']
})
export class CustomersActionButtonComponent extends DefaultEditor implements OnInit, ViewCell {
  @Input() value: string | number;
  @Input() rowData: any;
  loading:any;
  destroyByClick = true;
  duration = 2000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;
  status: NbComponentStatus = 'primary';
  title = 'HI there!';
  content = `I'm cool toaster!`;
  constructor(public toastrService: NbToastrService,private dialogService: NbDialogService,public  data:DataService,public usersService:UsersService, private router: Router) { super()}

  ngOnInit(): void {
  }

  active(id){
    this.loading=true;

    this.usersService.activeUser(id).subscribe(
      data => {
        this.loading=false;
        this.data.changeMessage(true);
          // this.del.nativeElement.click();
          this.status = 'success'
          this.title = 'Successfully Saved .. ';
          this.content = 'Please check !!'
          this.showToast(this.status, this.title, this.content);
          this.loading=false;

      },
      error => {
        this.loading = false;
        try {
          this.status = 'danger'
          this.title = 'Please check !!';
          this.content = error.error.message
          this.showToast(this.status, this.title, this.content);
        } catch (err) {
          this.status = 'danger'
          this.title = 'Please check !!';
          this.content = err.message;
          this.showToast(this.status, this.title, this.content);
        }
      },
    );
  }

  resetPassword() {
    this.loading=true;
    localStorage.getItem('token')

    this.usersService.resetAdminPassword(this.rowData._id).subscribe(
      data => {
        this.data.changeMessage(true);
          // this.del.nativeElement.click();
          this.status = 'success'
          this.title = 'Successfully Saved .. ';
          this.content = 'Please check !!'
          this.showToast(this.status, this.title, this.content);
          this.loading=false;
      },
      error => {
        this.loading = false;
        try {
          this.status = 'danger'
          this.title = 'Please check !!';
          this.content = error.error.message
          this.showToast(this.status, this.title, this.content);
        } catch (err) {
          this.status = 'danger'
          this.title = 'Please check !!';
          this.content = err.message;
          this.showToast(this.status, this.title, this.content);
        }
      },
    );

    // this.dialogService.open(CustomersResetPasswordModelComponent, {
    //   context: {
    //     title: 'This is a title passed to the dialog component',
    //     celldata:this.rowData

    //   },
    // });
  }

  deactive(id){
    this.loading=true;

    this.usersService.deactiveUser(id).subscribe(
      data => {
        this.loading=false;
        this.data.changeMessage(true);
          // this.del.nativeElement.click();
          this.status = 'success'
          this.title = 'Successfully Saved .. ';
          this.content = 'Please check !!'
          this.showToast(this.status, this.title, this.content);
          this.loading=false;

      },
      error => {
        this.loading = false;
        try {
          this.status = 'danger'
          this.title = 'Please check !!';
          this.content = error.error.message
          this.showToast(this.status, this.title, this.content);
        } catch (err) {
          this.status = 'danger'
          this.title = 'Please check !!';
          this.content = err.message;
          this.showToast(this.status, this.title, this.content);
        }
      },
    );
  }

  viewDetails(){
      this.dialogService.open(CustomerOrdersDetailsComponent, {
      context: {
        title: 'This is a title passed to the dialog component',
        celldata:this.rowData

      },
    });

  }
  public showToast(type: NbComponentStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title ? `. ${title}` : '';

    this.toastrService.show(body, title, config);
  }
}

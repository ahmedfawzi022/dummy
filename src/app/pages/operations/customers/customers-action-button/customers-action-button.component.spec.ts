import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomersActionButtonComponent } from './customers-action-button.component';

describe('CustomersActionButtonComponent', () => {
  let component: CustomersActionButtonComponent;
  let fixture: ComponentFixture<CustomersActionButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomersActionButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomersActionButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

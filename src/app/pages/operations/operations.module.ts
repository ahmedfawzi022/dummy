import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbMenuModule } from '@nebular/theme';
import { ThemeModule } from '../../@theme/theme.module';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OperationsRoutingModule } from './operations-routing.module';
import { ProductsComponent } from './products/products.component';
import { AddProductComponent } from './add-product/add-product.component';
import { VendorsComponent } from './vendors/vendors.component';
import { AddVendorComponent } from './add-vendor/add-vendor.component';
import { CustomersComponent } from './customers/customers.component';
import { AddCustomerComponent } from './add-customer/add-customer.component';
import { OperationsComponent } from './operations.component';
import { NgSelectModule } from '@ng-select/ng-select';

import {
  NbAccordionModule,
  NbButtonModule,
  NbSpinnerModule,
  NbListModule,
  NbRouteTabsetModule,
  NbStepperModule,
  NbTabsetModule, NbUserModule,
  NbCardModule, NbIconModule, NbInputModule, NbTreeGridModule,
  NbActionsModule,
  NbCheckboxModule,
  NbDatepickerModule,
  NbRadioModule,
  NbTooltipModule,
  NbSelectModule,
} from '@nebular/theme';
import { CustomersActionButtonComponent } from './customers/customers-action-button/customers-action-button.component';
import { VendorsActionButtonComponent } from './vendors/vendors-action-button/vendors-action-button.component';
import { ProductsActionButtonComponent } from './products/products-action-button/products-action-button.component';
import { VendorsModelDetailsComponent } from './vendors/vendors-model-details/vendors-model-details.component';
import { VendorsResetPasswordModelComponent } from './vendors/vendors-reset-password-model/vendors-reset-password-model.component';
import { CustomersResetPasswordModelComponent } from './customers/customers-reset-password-model/customers-reset-password-model.component';
import { ShipmenttypeComponent } from './shipmenttype/shipmenttype.component';
import { AddShipmenttypeComponent } from './add-shipmenttype/add-shipmenttype.component';
import { CustomerOrdersDetailsComponent } from './customers/customer-orders-details/customer-orders-details.component';

import {OrdersModule  } from '../orders/orders.module';
import { VendorModelHistoryComponent } from './vendors/vendor-model-history/vendor-model-history.component';
import { BannersComponent } from './banners/banners.component';
import { AddBannerComponent } from './add-banner/add-banner.component';

@NgModule({
  declarations: [ProductsComponent, OperationsComponent,AddProductComponent, VendorsComponent, AddVendorComponent, CustomersComponent, AddCustomerComponent, CustomersActionButtonComponent, VendorsActionButtonComponent, ProductsActionButtonComponent, VendorsModelDetailsComponent, VendorsResetPasswordModelComponent, CustomersResetPasswordModelComponent, ShipmenttypeComponent, AddShipmenttypeComponent, CustomerOrdersDetailsComponent, VendorModelHistoryComponent, BannersComponent, AddBannerComponent],
  imports: [
    CommonModule,
    OperationsRoutingModule,
    NbMenuModule,
    NbCardModule,
    NbTreeGridModule,
    NgSelectModule,
    NbIconModule,
    NbInputModule,
    NbSpinnerModule,
    ThemeModule,
    Ng2SmartTableModule,
    FormsModule,
    ReactiveFormsModule,
    NbTabsetModule,
    NbRouteTabsetModule,
    OrdersModule,
    NbStepperModule,
    NbCardModule,
    NbButtonModule,
    NbListModule,
    NbAccordionModule,
    NbUserModule,
    NbActionsModule,
    NbCheckboxModule,
    NbTooltipModule,
    NbDatepickerModule,
    NbRadioModule,
    NbSelectModule
  ],
  exports: [CustomersComponent,VendorsComponent],
  entryComponents: [CustomersActionButtonComponent,VendorsActionButtonComponent],

})
export class OperationsModule { }

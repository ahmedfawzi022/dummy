import { Component, OnInit,TemplateRef } from '@angular/core';
import { ServerDataSource } from 'ng2-smart-table';
import { Router, ActivatedRoute } from '@angular/router';
import { UsersService } from '../../../services/users/users.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DataService } from '../../../services/data/data.service';
import { OperationsService } from '../../../services/operations/operations.service';
import { NbDialogService } from '@nebular/theme';

import { SmartTableData } from '../../../@core/data/smart-table';
import { DomSanitizer } from '@angular/platform-browser';
import { ProductsActionButtonComponent } from '../products/products-action-button/products-action-button.component';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'ngx-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  fubUrl:any;
  rowData:any;
  vendorId:any;
vendors=[];
loading=false;
name: string = '';
isVendor=false;

  settings = {
    mode: 'external',
    actions:{
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      image: {
        title: 'Image',
        filter: false,
        type: 'html',
        sort:false,
        valuePrepareFunction: (image) => {
            return this._domSanitizer.bypassSecurityTrustHtml(`<img class="img-box" style="bo" src="${image}" />`);
        },
    },
 
      name: {
        title: 'Name',
        type: 'string',
      },
      price: {
        title: 'Price',
        type: 'string',
        filter:false
      },
      stock: {
        title: 'Stock',
        type: 'string',
        filter:false
      },

      discount: {
        title: 'Discount',
        type: 'string',
        filter:false
      },
      isDiscountActive: {
        title: 'Discount Active',
        filter: false,
        type: 'html',
        sort:false,
        valuePrepareFunction: (value) => {
          return (value) ? `<i class="fa fa fa-check fa fa-check-green"></i>` : `<i class="fa fa-times fa-times-red"></i>`
        }
    },


    isTopSelling: {
      title: 'Top Selling',
      filter: false,
      type: 'html',
      sort:false,
      valuePrepareFunction: (value) => {
        return (value) ? `<i class="fa fa fa-check fa fa-check-green"></i>` : `<i class="fa fa-times fa-times-red"></i>`
      }
  },
 
      relayAction: {
        title: 'Actions',
        type: 'custom',
        renderComponent: ProductsActionButtonComponent,
        editor: {
          type: 'custom',
          component: ProductsActionButtonComponent
        },
        
        sort: false,
        filter: false,
        width: '350px'
      }
    },
  };

  source: ServerDataSource ;

  constructor(private dialogService: NbDialogService,public operationsService:OperationsService,private service: SmartTableData,public data:DataService,public httpClient:HttpClient,public route: ActivatedRoute,public usersService:UsersService, private router: Router, private _domSanitizer: DomSanitizer) {
    this.fubUrl = environment.FUBS_API_URL
  }
  ngOnInit(): void {
    this.route.params.subscribe(params => {
      if(params['vendorId']){
        this.name=params['vendorId']
      }
    });
    // this.data.currentMessage.subscribe(
    //   data => {
    //     this.getAllservice();
    //   });
    
    var currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if(currentUser.roles[0]=="Vendor"){
      this.isVendor=true
      this.getserviceForCurrentVendor()
  
    }
    else{
      this.getVendors();

    }

  }

  onChangeVendor(event){
    console.log(event)
    this.vendorId=event;
    this.getservice(event)
  }
  getAllservice() {
    this.loading = true;
    this.source = new ServerDataSource(this.httpClient, {
      endPoint: `${this.fubUrl}/products/admin`,
      pagerPageKey: 'page',
      pagerLimitKey: 'limit',
      filterFieldKey: '#field#_like',
      totalKey: 'data.count',
      dataKey: 'data.list',
    }
    );
    this.loading = false;

  }
  getservice(id) {
    this.loading = true;
    this.source = new ServerDataSource(this.httpClient, {
      endPoint: `${this.fubUrl}/products/admin?vendorId=${id}`,
      pagerPageKey: 'page',
      pagerLimitKey: 'limit',
      filterFieldKey: '#field#',
      totalKey: 'data.count',
      dataKey: 'data.list',
    }
    );
    this.loading = false;

  }
  getserviceForCurrentVendor() {
    this.loading = true;
    this.source = new ServerDataSource(this.httpClient, {
      endPoint: `${this.fubUrl}/products/vendor`,
      pagerPageKey: 'page',
      pagerLimitKey: 'limit',
      filterFieldKey: '#field#_like',
      totalKey: 'data.count',
      dataKey: 'data.list',
    }
    );
    this.loading = false;

  }
  getVendors(){
    this.loading=true;

    this.usersService.getVendors().subscribe(
      data => {
        console.log(data.data)
        this.vendors=data.data.list


  //      this.isActiveControl.setValue(data.data.isActive)
        // this.categories.isActive=this.isActiveControl.value

        // this.router.navigateByUrl(`/pages/categories/categorie`);


        console.log(data)
        // console.log(token)

      },
      error => {
        this.loading=false;

        // this.spinnerService.hide();
        try {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.loginerrorMessage);
        } catch (err) {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.errorMessage);
        }
      },
    );
  }
  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }
  dialogDelete(dialog: TemplateRef<any>,event) {
    console.log(event)
    this.rowData=event;
    this.dialogService.open(
      dialog,
      {
        context: 'this is some additional data passed to dialog',
        closeOnBackdropClick: false,
      });

  }
  onClickYes(){
    this.onDelete(this.rowData)
  }
  onAdd(){
    this.router.navigateByUrl(`/pages/operations/add-product/${this.name}`);

  }
  onEdit(event){
    this.router.navigateByUrl(`/pages/operations/add-product/${event.data.vendorId._id}`+`/`+`${event.data._id}`);
  }
  onDelete(event) {
    this.operationsService.deleteProduct(event.data._id)

        .subscribe(
          data => {
            // this.del.nativeElement.click();
            this.getservice(this.vendorId);
            // this.makeToast(this.configurationDataObject.feesDeleteSuccessfullMessage);

          },
          error => {
//            this.del.nativeElement.click();
            try {
              // if (error.error.errorCode)
              // this.openModal(error.error.errorCode, error.error.errorMessage);
              // this.openModal(this.configurationDataObject.error, this.configurationDataObject.errorMessage);
            } catch (err) {
              // this.openModal(this.configurationDataObject.error, this.configurationDataObject.errorMessage);
            }
          },
      );

  }
}





import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductsActionButtonComponent } from './products-action-button.component';

describe('ProductsActionButtonComponent', () => {
  let component: ProductsActionButtonComponent;
  let fixture: ComponentFixture<ProductsActionButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductsActionButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsActionButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit,Input } from '@angular/core';
import { OperationsService } from '../../../../services/operations/operations.service';
import { DataService } from '../../../../services/data/data.service';

import {
  NbComponentStatus,
  NbGlobalLogicalPosition,
  NbGlobalPhysicalPosition,
  NbGlobalPosition,
  NbToastrConfig,
  NbToastrService
} from '@nebular/theme';
import { DefaultEditor, ViewCell } from 'ng2-smart-table';

@Component({
  selector: 'ngx-products-action-button',
  templateUrl: './products-action-button.component.html',
  styleUrls: ['./products-action-button.component.scss']
})
export class ProductsActionButtonComponent extends DefaultEditor implements OnInit, ViewCell {
  @Input() value: string | number;
  @Input() rowData: any;
  loading:any;
  destroyByClick = true;
  duration = 2000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;
  status: NbComponentStatus = 'primary';
  title = 'HI there!';
  content = `I'm cool toaster!`;
  constructor(public toastrService: NbToastrService,public operationsService:OperationsService,public data:DataService) { 
    super()
  }

  ngOnInit(): void {
  }
  activeProduct(id){
    this.loading=true;

    this.operationsService.activeProduct(id).subscribe(
      data => {
        this.data.changeMessage(true);
          // this.del.nativeElement.click();
          this.status = 'success'
          this.title = 'Successfully Saved .. ';
          this.content = 'Please check !!'
          this.showToast(this.status, this.title, this.content);
          this.loading=false;

      },
      error => {
        this.loading = false;
        try {
          this.status = 'danger'
          this.title = 'Please check !!';
          this.content = error.error.message
          this.showToast(this.status, this.title, this.content);
        } catch (err) {
          this.status = 'danger'
          this.title = 'Please check !!';
          this.content = err.message;
          this.showToast(this.status, this.title, this.content);
        }
      },
    );
  }



  deactiveProduct(id){
    this.loading=true;

    this.operationsService.deactiveProduct(id).subscribe(
      data => {
        this.data.changeMessage(true);
        this.status = 'success'
        this.title = 'Successfully Saved .. ';
        this.content = 'Please check !!'
        this.showToast(this.status, this.title, this.content);
        this.loading=false;

      },
      error => {
        this.loading = false;
        try {
          this.status = 'danger'
          this.title = 'Please check !!';
          this.content = error.error.message
          this.showToast(this.status, this.title, this.content);
        } catch (err) {
          this.status = 'danger'
          this.title = 'Please check !!';
          this.content = err.message;
          this.showToast(this.status, this.title, this.content);
        }
      },
    );
  }
  public showToast(type: NbComponentStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title ? `. ${title}` : '';

    this.toastrService.show(body, title, config);
  }

}

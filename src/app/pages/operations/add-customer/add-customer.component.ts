import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../../services/users/users.service';
import { environment } from '../../../../environments/environment';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Customer } from '../../../models/users/customer'
import { Address } from '../../../models/users/address'

import { Router, ActivatedRoute } from '@angular/router';
import { DataService } from '../../../services/data/data.service';
import { BaseComponent } from '../../general/base/base.component';
import {
  NbComponentStatus,
  NbGlobalLogicalPosition,
  NbGlobalPhysicalPosition,
  NbGlobalPosition,
  NbToastrService,
  NbToastrConfig,
} from '@nebular/theme';
@Component({
  selector: 'ngx-add-customer',
  templateUrl: './add-customer.component.html',
  styleUrls: ['./add-customer.component.scss']
})
export class AddCustomerComponent extends BaseComponent implements OnInit {
  customer: Customer
  address:Address
  countries=[];
  //Define Form Group with its controls
  addForm: FormGroup;
  idControl: FormControl;
  userNameControl: FormControl;
  firstNameControl: FormControl;
  lastNameControl:FormControl;
  phoneNumberControl: FormControl;
  emailControl: FormControl;
  passwordControl: FormControl;
  profilePictureControl: FormControl;
  addresseNameControl:FormControl;
  contactNumberControl:FormControl;
  countryIdControl:FormControl;
  countryControl:FormControl;
  districtControl:FormControl;
  buildingNumberControl:FormControl;
  apartmentNumberControl:FormControl;
  streetNameControl:FormControl;
  cityControl:FormControl;
  public imageProfileSrc: string = '';
  loading = false;

  constructor(public usersService: UsersService, public route: ActivatedRoute, public data: DataService, public router: Router, public toastrService: NbToastrService) {
    super(toastrService)
    this.customer = new Customer()
    // this.address=new Address()
  }

  ngOnInit(): void {
    // create form group and its form controls and
    this.createFormControls();
    this.createForm();
    this.route.params.subscribe(params => {
      if(params['id']){
      this.idControl.setValue(params['id'])
      this.getUserById(this.idControl.value)
      }
    });
    this.getCountry();
  }

  // create form controls that we need to validate with their needed validators
  createFormControls() {
    this.firstNameControl = new FormControl(null, [
      Validators.required,
    ]),
    this.lastNameControl = new FormControl(null, [
      Validators.required,
    ]),
      this.userNameControl = new FormControl(null, [
        Validators.required,
      ]),
      this.passwordControl = new FormControl(null, [
        Validators.required,
        Validators.pattern("[a-zA-Z0-9]{6,255}$")
      ]);
    this.emailControl = new FormControl(null, [
      Validators.required,
      Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")
    ]),
    // this.apartmentNumberControl= new FormControl(null, [
    //   //Validators.required,
    // ]),
    // this.buildingNumberControl= new FormControl(null, [
    //   //Validators.required,
    // ]),
    // this.streetNameControl= new FormControl(null, [
    //   //Validators.required,
    // ]),
    // this.districtControl= new FormControl(null, [
    //   //Validators.required,
    // ]),
    // this.cityControl= new FormControl(null, [
    //   //Validators.required,
    // ]),
    // this.countryControl= new FormControl(null, [
    //   //Validators.required,
    // ]),
    this.countryIdControl=new FormControl(null, [
      Validators.required,
    ]),
    // this.contactNumberControl= new FormControl(null, [
    //   //Validators.required,
    // ]),
    // this.addresseNameControl= new FormControl(null, [
    // //  Validators.required,
    // ]),
    
      this.phoneNumberControl = new FormControl(null, [
        Validators.required,
        Validators.pattern("(01)[0-9]{9}")
      ]);
    this.profilePictureControl = new FormControl('', [
     Validators.required,
    ]);
    this.idControl = new FormControl(null)
  }


  // add form controls to form group
  createForm() {
    this.addForm = new FormGroup({
      firstName: this.firstNameControl,
      lastName:this.lastNameControl,
      userName: this.userNameControl,
      email: this.emailControl,
      phoneNumber: this.phoneNumberControl,
      profilePicture: this.profilePictureControl,
      countryId:this.countryIdControl,
      password: this.passwordControl,
      // addresseName:this.addresseNameControl,
      // contactNumber:this.contactNumberControl,
      // country:this.countryControl,
      // city:this.cityControl,
      // district:this.districtControl,
      // streetName:this.streetNameControl,
      // buildingNumber:this.buildingNumberControl,
      // apartmentNumber:this.apartmentNumberControl,
      id: this.idControl
    });
  }

  getCountry(){
    this.loading=true;

    this.usersService.getCountry().subscribe(
      data => {
        console.log(data.data)
        this.countries=data.data.list


  //      this.isActiveControl.setValue(data.data.isActive)
        // this.categories.isActive=this.isActiveControl.value

        // this.router.navigateByUrl(`/pages/categories/categorie`);


        console.log(data)
        // console.log(token)

      },
      error => {
        this.loading=false;

        // this.spinnerService.hide();
        try {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.loginerrorMessage);
        } catch (err) {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.errorMessage);
        }
      },
    );
  }

  getUserById(id){
    this.loading=true;

    this.usersService.getUserById(id).subscribe(
      data => {
        console.log(data.data)
        this.passwordControl.setValidators([])
        this.passwordControl.updateValueAndValidity();
        this.firstNameControl.setValue(data.data.firstName)
        this.customer.firstName = this.firstNameControl.value
        this.lastNameControl.setValue(data.data.lastName)
        this.customer.lastName = this.lastNameControl.value;
        this.imageProfileSrc = data?.data?.profilePicture;
        this.profilePictureControl.setValue(data?.data?.profilePicture)
        this.customer.password = this.passwordControl.value
        this.userNameControl.setValue(data.data.userName)
        this.customer.userName = this.userNameControl.value
        this.emailControl.setValue(data.data.email)
        this.customer.email = this.emailControl.value
        this.phoneNumberControl.setValue(data.data.phoneNumber)
        this.customer.phoneNumber = this.phoneNumberControl.value
        this.countryIdControl.setValue(data.data.countryId._id)
        this.customer.countryId = this.countryIdControl.value

        // this.addresseNameControl.setValue(data.data.addresses.name)
        // this.address.name = this.addresseNameControl.value
        // this.contactNumberControl.setValue(data.data.addresses.contactNumber)
        // this.address.contactNumber = this.contactNumberControl.value
        // this.countryControl.setValue(data.data.addresses.country)
        // this.address.country = this.countryControl.value
        // this.cityControl.setValue(data.data.addresses.city)
        // this.address.city = this.cityControl.value
        // this.districtControl.setValue(data.data.addresses.district)
        // this.address.district = this.districtControl.value
        // this.streetNameControl.setValue(data.data.addresses.streetName)
        // this.address.streetName = this.streetNameControl.value
        // this.buildingNumberControl.setValue(data.data.addresses.buildingNumber)
        // this.address.buildingNumber = this.buildingNumberControl.value
        // this.apartmentNumberControl.setValue(data.data.addresses.apartmentNumber)
        // this.address.apartmentNumber = this.apartmentNumberControl.value
        this.loading=false;

  //      this.isActiveControl.setValue(data.data.isActive)
        // this.categories.isActive=this.isActiveControl.value

        // this.router.navigateByUrl(`/pages/categories/categorie`);


        console.log(data)
        // console.log(token)

      },
      error => {
        this.loading=false;

        // this.spinnerService.hide();
        try {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.loginerrorMessage);
        } catch (err) {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.errorMessage);
        }
      },
    );
  }
  handleInputProfileChange(e) {
    var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
    var pattern = /image-*/;
    var reader = new FileReader();
    if (!file.type.match(pattern)) {
      alert('invalid format');
      return;
    }
    reader.onload = this._handleProfileReaderLoaded.bind(this);
    reader.readAsDataURL(file);
  }
  _handleProfileReaderLoaded(e) {
    let reader = e.target;
    this.imageProfileSrc = reader.result;
    console.log(this.imageProfileSrc)
    this.profilePictureControl.setValue(this.imageProfileSrc)
  }

  save() {

    this.loading = true;
    this.customer.firstName = this.firstNameControl.value
    this.customer.lastName = this.lastNameControl.value
    this.customer.userName = this.userNameControl.value;
    this.customer.email = this.emailControl.value
    this.customer.password = this.passwordControl.value
    this.customer.phoneNumber = this.phoneNumberControl.value
    this.customer.profilePicture = this.profilePictureControl.value
    this.customer.countryId=this.countryIdControl.value;
    // this.address.name = this.addresseNameControl.value
    // this.address.contactNumber = this.contactNumberControl.value
    // this.address.country = this.countryControl.value
    // this.address.city = this.cityControl.value
    // this.address.district = this.districtControl.value
    // this.address.streetName = this.streetNameControl.value
    // this.address.buildingNumber = this.buildingNumberControl.value
    // this.address.apartmentNumber = this.apartmentNumberControl.value
    // this.customer.addresses.push(this.address)
    if (this.idControl.value) {
      this.idControl.disable();
      this.passwordControl.disable();
      // this.categories.isActive=this.isActiveControl.value
      this.usersService.saveCustomer(this.addForm.value, this.idControl.value).subscribe(
        data => {
          this.router.navigateByUrl(`/pages/operations/customers`);

          // this.status='danger'
          // this.title='Successfully Saved .. ';
          // this.content='Please check !!'
          // this.showToast(this.status, this.title, this.content);
          // this.data.changeMessage(this.datasaved)
          this.loading = false;

        },
        error => {
          console.log(error)
          this.loading = false;
          // this.spinnerService.hide();
          try {
            this.status='danger'
            this.title= 'Please check !!';
            this.content=error.error.message
            this.showToast(this.status, this.title, this.content);
          } catch (err) {
            this.status='danger'
            this.title='Please check !!';
            this.content=err.message;
             this.showToast(this.status, this.title,this.content );
            // this.openModal(this.loginDataObject.error, this.loginDataObject.errorMessage);
          }
        },
      );

    } else {
      this.usersService.saveCustomer(this.customer).subscribe(
        data => {
          this.router.navigateByUrl(`/pages/operations/customers`);

          this.status = 'success'
          this.title = 'Successfully Saved .. ';
          this.content = 'Please check !!'
          this.showToast(this.status, this.title, this.content);
          this.data.changeMessage(true)
          this.loading = false;
        },
        error => {
          this.loading = false;

          console.log(error)
          // this.spinnerService.hide();
          try {
            this.status='danger'
            this.title='Please check !!';
            this.content=error.error.message
             this.showToast(this.status, this.title,this.content );
          } catch (err) {
            this.status='danger'
            this.title='Please check !!';
            this.content=err.message;
             this.showToast(this.status, this.title,this.content );
            // this.openModal(this.loginDataObject.error, this.loginDataObject.errorMessage);
          }
        },
      );

    }


  }

}

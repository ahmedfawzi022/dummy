import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../../services/users/users.service';
import { environment } from '../../../../environments/environment';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Vendor } from '../../../models/users/vendor'
import { Router, ActivatedRoute } from '@angular/router';
import { DataService } from '../../../services/data/data.service';
import { BaseComponent } from '../../general/base/base.component';
import {
  NbComponentStatus,
  NbGlobalLogicalPosition,
  NbGlobalPhysicalPosition,
  NbGlobalPosition,
  NbToastrService,
  NbToastrConfig,
} from '@nebular/theme';
@Component({
  selector: 'ngx-add-banner',
  templateUrl: './add-banner.component.html',
  styleUrls: ['./add-banner.component.scss']
})
export class AddBannerComponent extends BaseComponent implements OnInit {
  //Define Form Group with its controls
  addForm: FormGroup;
  idControl: FormControl;
  nameControl: FormControl;
  descriptionControl: FormControl;
  bannerControl: FormControl;
  bannerUrlControl:FormControl;
  public imageBannerSrc: string = '';

  loading = false;

  constructor(public usersService: UsersService, public route: ActivatedRoute, public data: DataService, public router: Router, public toastrService: NbToastrService) {
    super(toastrService)
  }

  ngOnInit(): void {
    // create form group and its form controls and
    this.createFormControls();
    this.createForm();
    this.route.params.subscribe(params => {
      if(params['id']){
      this.idControl.setValue(params['id'])
      this.getBannerById(this.idControl.value)
      }
    });
  }

  // create form controls that we need to validate with their needed validators
  createFormControls() {
    this.nameControl = new FormControl(null, [
      Validators.required,
    ]),
    this.descriptionControl = new FormControl(null, [
      Validators.required,
    ]),
    this.bannerControl = new FormControl(null, [
      Validators.required,
    ]),
    this.bannerUrlControl = new FormControl(null, [
      Validators.required,
    ]),
 

    this.idControl = new FormControl(null)
  }


  // add form controls to form group
  createForm() {
    this.addForm = new FormGroup({
      name: this.nameControl,
      description:this.descriptionControl,
      banner:this.bannerControl,
      bannerUrl:this.bannerUrlControl,
      id: this.idControl
    });
  }
  handleBannerInputChange(e){
    var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
    var pattern = /image-*/;
    var reader = new FileReader();
    if (!file.type.match(pattern)) {
      alert('invalid format');
      return;
    }
    reader.onload = this._handleBannerReaderLoaded.bind(this);
    reader.readAsDataURL(file);
  }
  _handleBannerReaderLoaded(e) {
    let reader = e.target;
    this.imageBannerSrc = reader.result;
    console.log(this.imageBannerSrc)
    this.bannerControl.setValue(this.imageBannerSrc)
  }

  getBannerById(id){
    this.loading=true;

    this.usersService.getBannerById(id).subscribe(
      data => {
        this.nameControl.setValue(data.data.name)
        this.descriptionControl.setValue(data.data.description)
        this.imageBannerSrc = data?.data?.banner;
        this.bannerControl.setValue(this.imageBannerSrc)
        this.bannerUrlControl.setValue(data.data.bannerUrl)
        this.loading=false;

  //      this.isActiveControl.setValue(data.data.isActive)
        // this.categories.isActive=this.isActiveControl.value

        // this.router.navigateByUrl(`/pages/categories/categorie`);


        console.log(data)
        // console.log(token)

      },
      error => {
        this.loading=false;

        // this.spinnerService.hide();
        try {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.loginerrorMessage);
        } catch (err) {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.errorMessage);
        }
      },
    );
  }

  save() {

    this.loading = true;
   

    if (this.idControl.value) {
      this.idControl.disable();
      this.usersService.saveBanner(this.addForm.value, this.idControl.value).subscribe(
        data => {
          this.router.navigateByUrl(`/pages/operations/banners`);
          this.loading = false;

        },
        error => {
          console.log(error)
          this.loading = false;
          try {
            this.status='danger'
            this.title= 'Please check !!';
            this.content=error.error.message
            this.showToast(this.status, this.title, this.content);
          } catch (err) {
            this.status='danger'
            this.title='Please check !!';
            this.content=err.message;
             this.showToast(this.status, this.title,this.content );
          }
        },
      );

    } else {
      this.idControl.disable()
      this.usersService.saveBanner(this.addForm.value).subscribe(
        data => {
          this.router.navigateByUrl(`/pages/operations/banners`);

          this.status = 'success'
          this.title = 'Successfully Saved .. ';
          this.content = 'Please check !!'
          this.showToast(this.status, this.title, this.content);
          this.data.changeMessage(true)
          this.loading = false;
        },
        error => {
          this.loading = false;

          console.log(error)
          try {
            this.status='danger'
            this.title='Please check !!';
            this.content=error.error.message
             this.showToast(this.status, this.title,this.content );
          } catch (err) {
            this.status='danger'
            this.title='Please check !!';
            this.content=err.message;
             this.showToast(this.status, this.title,this.content );
          }
        },
      );

    }


  }

}


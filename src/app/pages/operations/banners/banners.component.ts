import { Component, OnInit, TemplateRef } from '@angular/core';
import { LocalDataSource, ServerDataSource } from 'ng2-smart-table';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DataService } from '../../../services/data/data.service';
import { SmartTableData } from '../../../@core/data/smart-table';
import { DomSanitizer } from '@angular/platform-browser';
import { VendorsActionButtonComponent } from '../vendors/vendors-action-button/vendors-action-button.component';
import { environment } from '../../../../environments/environment';
import { UsersService } from '../../../services/users/users.service';
import { NbDialogService } from '@nebular/theme';

@Component({
  selector: 'ngx-banners',
  templateUrl: './banners.component.html',
  styleUrls: ['./banners.component.scss']
})
export class BannersComponent implements OnInit {
  loading = false;
  fubUrl: any;
  rowData: any;
  settings = {
    mode: 'external',
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
    },
    columns: {
      banner: {
        title: 'Banner',
        filter: false,
        type: 'html',
        sort: false,
        valuePrepareFunction: (banner) => {
          return this._domSanitizer.bypassSecurityTrustHtml(`<img class="img-box" style="bo" src="${banner}" />`);
        },
      },
      name: {
        title: 'Name',
        type: 'string',
        sort: false,
        filter:false

      },

    }
  };

  source: ServerDataSource

  constructor(private dialogService: NbDialogService, private service: SmartTableData, public usersService: UsersService, public data: DataService, private httpClient: HttpClient, private router: Router, private _domSanitizer: DomSanitizer) {
    this.fubUrl = environment.FUBS_API_URL

  }
  ngOnInit(): void {
    this.data.currentMessage.subscribe(
      data => {
        this.getservice()
      });
  }


  getservice() {
    this.loading = true;
    this.source = new ServerDataSource(this.httpClient, {
      endPoint: `${this.fubUrl}/banners`,
      pagerPageKey: 'page',
      pagerLimitKey: 'limit',
      filterFieldKey: '#field#_like',
      totalKey: 'data.count',
      dataKey: 'data.list',
    }
    );
    this.loading = false;

  }
  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      this.onDelete(event)
    } else {
      event.confirm.reject();
    }
  }

  onAdd() {
    this.router.navigateByUrl('/pages/operations/add-banner');

  }
  onEdit(event) {
    console.log(event.data._id)
    this.router.navigateByUrl(`/pages/operations/add-banner/${event.data._id}`);
  }
  dialogDelete(dialog: TemplateRef<any>, event) {
    console.log(event)
    this.rowData = event;
    this.dialogService.open(
      dialog,
      {
        context: 'this is some additional data passed to dialog',
        closeOnBackdropClick: false,
      });

  }
  onClickYes() {
    this.onDelete(this.rowData)
  }
  onDelete(event) {
    this.usersService.deleteBanner(event.data._id)

      .subscribe(
        data => {
          // this.del.nativeElement.click();
          this.getservice();
          // this.makeToast(this.configurationDataObject.feesDeleteSuccessfullMessage);

        },
        error => {
          //            this.del.nativeElement.click();
          try {
            // if (error.error.errorCode)
            // this.openModal(error.error.errorCode, error.error.errorMessage);
            // this.openModal(this.configurationDataObject.error, this.configurationDataObject.errorMessage);
          } catch (err) {
            // this.openModal(this.configurationDataObject.error, this.configurationDataObject.errorMessage);
          }
        },
      );

  }
}





import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../../services/users/users.service';
import { environment } from '../../../../environments/environment';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Vendor } from '../../../models/users/vendor'
import { Router, ActivatedRoute } from '@angular/router';
import { DataService } from '../../../services/data/data.service';
import { BaseComponent } from '../../general/base/base.component';
import {
  NbComponentStatus,
  NbGlobalLogicalPosition,
  NbGlobalPhysicalPosition,
  NbGlobalPosition,
  NbToastrService,
  NbToastrConfig,
} from '@nebular/theme';
@Component({
  selector: 'ngx-add-vendor',
  templateUrl: './add-vendor.component.html',
  styleUrls: ['./add-vendor.component.scss']
})
export class AddVendorComponent extends BaseComponent implements OnInit {
  vendor: Vendor
  images = [];
  countries=[]

  //Define Form Group with its controls
  addForm: FormGroup;
  idControl: FormControl;
  userNameControl: FormControl;
  firstNameControl: FormControl;
  brandNameControl:FormControl;
  brandLogoControl:FormControl;
  lastNameControl:FormControl;
  phoneNumberControl: FormControl;
  countryIdControl:FormControl
  galleryControl:FormControl;


  emailControl: FormControl;
  passwordControl: FormControl;
  legalFormControl: FormControl;
  public imageLegalFormSrc: string = '';
  public imageLogoSrc: string = '';

  loading = false;

  constructor(public usersService: UsersService, public route: ActivatedRoute, public data: DataService, public router: Router, public toastrService: NbToastrService) {
    super(toastrService)
    this.vendor = new Vendor()
  }

  ngOnInit(): void {
    // create form group and its form controls and
    this.createFormControls();
    this.createForm();
    this.getCountry();
    this.route.params.subscribe(params => {
      if(params['id']){
      this.idControl.setValue(params['id'])
      this.getUserById(this.idControl.value)
      }
    });
  }

  // create form controls that we need to validate with their needed validators
  createFormControls() {
    this.firstNameControl = new FormControl(null, [
      Validators.required,
    ]),
    this.lastNameControl = new FormControl(null, [
      Validators.required,
    ]),
      this.userNameControl = new FormControl(null, [
        Validators.required,
      ]),
      this.countryIdControl = new FormControl(null, [
        Validators.required,
      ]),
      this.galleryControl = new FormControl(null, [
        //Validators.required,
      ]),
      this.passwordControl = new FormControl(null, [
        Validators.required,
        Validators.pattern("[a-zA-Z0-9]{6,255}$")
      ]);
      this.brandNameControl= new FormControl(null, [
        Validators.required,
      ]),
      this.brandLogoControl= new FormControl(null, [
        Validators.required,
      ]),
    this.emailControl = new FormControl(null, [
      Validators.required,
      Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")
    ]),
      this.phoneNumberControl = new FormControl(null, [
        Validators.required,
        Validators.pattern("(01)[0-9]{9}")
      ]);
    this.legalFormControl = new FormControl('', [
     Validators.required,
    ]);
    this.idControl = new FormControl(null)
  }


  // add form controls to form group
  createForm() {
    this.addForm = new FormGroup({
      firstName: this.firstNameControl,
      lastName:this.lastNameControl,
      userName: this.userNameControl,
      brandName:this.brandNameControl,
      brandLogo:this.brandLogoControl,
      gallery:this.galleryControl,
      email: this.emailControl,
      countryId:this.countryIdControl,
      phoneNumber: this.phoneNumberControl,
      profilePicture: this.legalFormControl,
      password: this.passwordControl,
      id: this.idControl
    });
  }
  getCountry(){
    this.loading=true;

    this.usersService.getCountry().subscribe(
      data => {
        console.log(data.data)
        this.countries=data.data.list


  //      this.isActiveControl.setValue(data.data.isActive)
        // this.categories.isActive=this.isActiveControl.value

        // this.router.navigateByUrl(`/pages/categories/categorie`);


        console.log(data)
        // console.log(token)

      },
      error => {
        this.loading=false;

        // this.spinnerService.hide();
        try {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.loginerrorMessage);
        } catch (err) {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.errorMessage);
        }
      },
    );
  }
  getUserById(id){
    this.loading=true;

    this.usersService.getUserById(id).subscribe(
      data => {
        console.log(data.data)
        this.passwordControl.setValidators([])
        this.passwordControl.updateValueAndValidity();
        this.firstNameControl.setValue(data.data.firstName)
        this.vendor.firstName = this.firstNameControl.value
        this.lastNameControl.setValue(data.data.lastName)
        this.vendor.lastName = this.lastNameControl.value;
        this.countryIdControl.setValue(data.data.countryId._id)
        this.vendor.countryId = this.countryIdControl.value;
        this.imageLegalFormSrc = data?.data?.brandId.legalForms[0];
        this.legalFormControl.setValue(this.imageLegalFormSrc)
        this.vendor.password = this.passwordControl.value
        this.userNameControl.setValue(data.data.userName)
        this.vendor.userName = this.userNameControl.value
        this.emailControl.setValue(data.data.email)
        this.vendor.email = this.emailControl.value
        this.phoneNumberControl.setValue(data.data.phoneNumber)
        this.vendor.phoneNumber = this.phoneNumberControl.value
        this.brandNameControl.setValue(data.data.brandId.name)
        this.vendor.brand.name=data.data.brandId.name
        this.imageLogoSrc = data?.data?.brandId.logo;
        this.brandLogoControl.setValue(data?.data?.brandId.logo)
        this.loading=false;

  //      this.isActiveControl.setValue(data.data.isActive)
        // this.categories.isActive=this.isActiveControl.value

        // this.router.navigateByUrl(`/pages/categories/categorie`);


        console.log(data)
        // console.log(token)

      },
      error => {
        this.loading=false;

        // this.spinnerService.hide();
        try {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.loginerrorMessage);
        } catch (err) {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.errorMessage);
        }
      },
    );
  }
  handleInputLegalFormChange(e) {
    var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
    var pattern = /image-*/;
    var reader = new FileReader();
    if (!file.type.match(pattern)) {
      alert('invalid format');
      return;
    }
    reader.onload = this._handleLegalFormReaderLoaded.bind(this);
    reader.readAsDataURL(file);
  }
  _handleLegalFormReaderLoaded(e) {
    let reader = e.target;
    this.imageLegalFormSrc = reader.result;
    console.log(this.imageLegalFormSrc)
    this.legalFormControl.setValue(this.imageLegalFormSrc)
    this.vendor.brand.legalForms.push(this.imageLegalFormSrc);
  }
  handleLogoInputChange(e){
    var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
    var pattern = /image-*/;
    var reader = new FileReader();
    if (!file.type.match(pattern)) {
      alert('invalid format');
      return;
    }
    reader.onload = this._handleLogoReaderLoaded.bind(this);
    reader.readAsDataURL(file);
  }
  _handleLogoReaderLoaded(e) {
    let reader = e.target;
    this.imageLogoSrc = reader.result;
    console.log(this.imageLogoSrc)
    this.brandLogoControl.setValue(this.imageLogoSrc)
  }
  onFileChange(event){
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
              var reader = new FileReader();
 
              reader.onload = (event:any) => {
                 this.images.push(event.target.result); 
                 this.vendor.brand.gallery=this.images;
                 this.galleryControl.setValue(this.vendor.brand.gallery)

                //  this.myForm.patchValue({
                //     fileSource: this.images
                //  });
              }

              reader.readAsDataURL(event.target.files[i]);
      }
  }
  }

  clear(){
    this.images=[];
    this.vendor.brand.gallery=this.images;
    this.galleryControl.setValue([])


  }
  save() {

    this.loading = true;
    this.vendor.firstName = this.firstNameControl.value
    this.vendor.lastName = this.lastNameControl.value
    this.vendor.userName = this.userNameControl.value;
    this.vendor.email = this.emailControl.value
    this.vendor.countryId=this.countryIdControl.value;
    this.vendor.password = this.passwordControl.value
    this.vendor.phoneNumber = this.phoneNumberControl.value
    // this.vendor.legalForm = this.legalFormControl.value
    this.vendor.brand.name=this.brandNameControl.value;
    this.vendor.brand.logo=this.brandLogoControl.value;
    console.log(this.vendor);

    if (this.idControl.value) {
      this.idControl.disable();
      this.passwordControl.disable();
      
      this.usersService.saveVendor(this.addForm.value, this.idControl.value).subscribe(
        data => {
          this.status = 'success'
          this.title = 'Successfully Saved .. ';
          this.content = 'Please check !!'
          this.showToast(this.status, this.title, this.content);
          this.data.changeMessage(true)
          this.loading = false;
          this.router.navigateByUrl(`/pages/operations/vendors`);


        },
        error => {
          console.log(error)
          this.loading = false;
          try {
            this.status='danger'
            this.title= 'Please check !!';
            this.content=error.error.message
            this.showToast(this.status, this.title, this.content);
          } catch (err) {
            this.status='danger'
            this.title='Please check !!';
            this.content=err.message;
             this.showToast(this.status, this.title,this.content );
          }
        },
      );

    } else {
      this.usersService.saveVendor(this.vendor).subscribe(
        data => {
          this.router.navigateByUrl(`/pages/operations/vendors`);

          this.status = 'success'
          this.title = 'Successfully Saved .. ';
          this.content = 'Please check !!'
          this.showToast(this.status, this.title, this.content);
          this.data.changeMessage(true)
          this.loading = false;
        },
        error => {
          this.loading = false;

          console.log(error)
          try {
            this.status='danger'
            this.title='Please check !!';
            this.content=error.error.message
             this.showToast(this.status, this.title,this.content );
          } catch (err) {
            this.status='danger'
            this.title='Please check !!';
            this.content=err.message;
             this.showToast(this.status, this.title,this.content );
          }
        },
      );

    }


  }

}


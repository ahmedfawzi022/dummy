import { Component, OnInit ,Input} from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { NbSortDirection, NbSortRequest, NbTreeGridDataSource, NbTreeGridDataSourceBuilder } from '@nebular/theme';
import { DefaultEditor, ViewCell } from 'ng2-smart-table';

@Component({
  selector: 'ngx-vendors-model-details',
  templateUrl: './vendors-model-details.component.html',
  styleUrls: ['./vendors-model-details.component.scss']
})
export class VendorsModelDetailsComponent extends DefaultEditor implements OnInit, ViewCell {

  @Input() value: string | number;
  @Input() rowData: any;
  celldata: any;

  constructor(protected ref: NbDialogRef<VendorsModelDetailsComponent>) { 
    super();

    }
    @Input() title: string;

  ngOnInit(): void {
    console.log(this.celldata)

  }
  showPdf(base64Data) {
    let MIMEType;
    var fileName;
    var signatures = {
      JVBERi0: "application/pdf",
      R0lGODdh: "image/gif",
      R0lGODlh: "image/jpg",
      iVBORw0KGgo: "image/png"
    };
    for (var s in signatures) {
      if (base64Data.indexOf(s) !== -1) {
        MIMEType = signatures[s]
      }
    }
    const linkSource = base64Data;
    const downloadLink = document.createElement("a");
    if(MIMEType == 'application/pdf')
     fileName = "legalform.pdf";
     else if(MIMEType == 'image/png')
     fileName = "legalform.png";
     else(MIMEType == 'image/jpg')
     fileName = "legalform.jpg";
  
    downloadLink.href = linkSource;
    downloadLink.download = fileName;
    downloadLink.click();
  }
  dismiss() {
    this.ref.close();
  }

}





import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorsModelDetailsComponent } from './vendors-model-details.component';

describe('VendorsModelDetailsComponent', () => {
  let component: VendorsModelDetailsComponent;
  let fixture: ComponentFixture<VendorsModelDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendorsModelDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VendorsModelDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

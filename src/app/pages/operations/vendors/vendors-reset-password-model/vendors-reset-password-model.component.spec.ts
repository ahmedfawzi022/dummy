import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorsResetPasswordModelComponent } from './vendors-reset-password-model.component';

describe('VendorsResetPasswordModelComponent', () => {
  let component: VendorsResetPasswordModelComponent;
  let fixture: ComponentFixture<VendorsResetPasswordModelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendorsResetPasswordModelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VendorsResetPasswordModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit ,Input} from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { UsersService } from '../../../../services/users/users.service';
import { DataService } from '../../../../services/data/data.service';
import { FormControl, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'ngx-vendors-reset-password-model',
  templateUrl: './vendors-reset-password-model.component.html',
  styleUrls: ['./vendors-reset-password-model.component.scss']
})
export class VendorsResetPasswordModelComponent implements OnInit {
  sendForm: FormGroup;
  valueControl: FormControl;
  constructor(protected ref: NbDialogRef<VendorsResetPasswordModelComponent>,public data:DataService,public usersService:UsersService) { }

  @Input() title: string;
  celldata:any;

  ngOnInit(): void {
    console.log(this.celldata)
    this.createFormControls()
    this.createForm();
  }
  createFormControls() {
    this.valueControl = new FormControl(null, [
      Validators.required,
    ]);

  }

  createForm() {
    this.sendForm = new FormGroup({
      password: this.valueControl,
    });
  }
  cancel() {
    this.ref.close();
  }
  resetPassword(id){
    // this.loading=true;

    this.usersService.resetPassword(this.sendForm.value,id).subscribe(
      data => {
        // this.loading=false;
        this.data.changeMessage(true);

        // this.isActiveControl.setValue(data.data.isActive)
        // this.categories.isActive=this.isActiveControl.value
       // this.router.navigateByUrl(`/pages/categories/categorie`);


        console.log("kkkk")
        // console.log(token)

      },
      error => {
        // this.loading=false;

        // this.spinnerService.hide();
        try {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.loginerrorMessage);
        } catch (err) {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.errorMessage);
        }
      },
    );
  }
  submit(name) {
    this.resetPassword(this.celldata._id)

    this.ref.close(name);
  }
}
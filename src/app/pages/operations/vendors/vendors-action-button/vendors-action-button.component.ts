import { Component, OnInit,Input } from '@angular/core';
import { VendorsModelDetailsComponent } from '../vendors-model-details/vendors-model-details.component';
import { NbDialogService } from '@nebular/theme';
import { DefaultEditor, ViewCell } from 'ng2-smart-table';
import { UsersService } from '../../../../services/users/users.service';
import { Router } from '@angular/router';
import { DataService } from '../../../../services/data/data.service';
import { VendorsResetPasswordModelComponent } from '../vendors-reset-password-model/vendors-reset-password-model.component';
import { VendorModelHistoryComponent } from '../vendor-model-history/vendor-model-history.component';
import {
  NbComponentStatus,
  NbGlobalLogicalPosition,
  NbGlobalPhysicalPosition,
  NbGlobalPosition,
  NbToastrConfig,
  NbToastrService
} from '@nebular/theme';
import {Location} from '@angular/common'; 

@Component({
  selector: 'ngx-vendors-action-button',
  templateUrl: './vendors-action-button.component.html',
  styleUrls: ['./vendors-action-button.component.scss']
})
export class VendorsActionButtonComponent extends DefaultEditor implements OnInit, ViewCell {
  constructor(private location: Location ,public toastrService: NbToastrService,private dialogService: NbDialogService,public  data:DataService,public usersService:UsersService, private router: Router) { super()}
  @Input() value: string | number;
  @Input() rowData: any;
  loading:any;
  destroyByClick = true;
  duration = 2000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;
  status: NbComponentStatus = 'primary';
  title = 'HI there!';
  content = `I'm cool toaster!`;
  ngOnInit(): void {
    console.log(this.rowData)
    console.log(this.value)
  }
  open() {
    this.dialogService.open(VendorsModelDetailsComponent, {
      context: {
        title: 'This is a title passed to the dialog component',
        celldata:this.rowData
      },
    });
  }

  viewProducts(){
    this.router.navigateByUrl(`/pages/operations/products/${this.rowData._id}`);

  }

  active(id){
    this.loading=true;

    this.usersService.activeUser(id).subscribe(
      data => {
        this.loading=false;
        this.data.changeMessage(true);

        this.status = 'success'
        this.title = 'Successfully Saved .. ';
        this.content = 'Please check !!'
        this.showToast(this.status, this.title, this.content);
        this.loading=false;
        // console.log(token)

      },
      error => {
        this.loading = false;
        try {
          this.status = 'danger'
          this.title = 'Please check !!';
          this.content = error.error.message
          this.showToast(this.status, this.title, this.content);
        } catch (err) {
          this.status = 'danger'
          this.title = 'Please check !!';
          this.content = err.message;
          this.showToast(this.status, this.title, this.content);
        }
      },
    );
  }

  resetPassword() {
    this.loading=true;
    localStorage.getItem('token')

    this.usersService.resetAdminPassword(this.rowData._id).subscribe(
      data => {
        this.data.changeMessage(true);
          // this.del.nativeElement.click();
          this.status = 'success'
          this.title = 'Successfully Saved .. ';
          this.content = 'Please check !!'
          this.showToast(this.status, this.title, this.content);
          this.loading=false;
      },
      error => {
        this.loading = false;
        try {
          this.status = 'danger'
          this.title = 'Please check !!';
          this.content = error.error.message
          this.showToast(this.status, this.title, this.content);
        } catch (err) {
          this.status = 'danger'
          this.title = 'Please check !!';
          this.content = err.message;
          this.showToast(this.status, this.title, this.content);
        }
      },
    );

    // this.dialogService.open(CustomersResetPasswordModelComponent, {
    //   context: {
    //     title: 'This is a title passed to the dialog component',
    //     celldata:this.rowData

    //   },
    // });
  }

  deactive(id){
    this.loading=true;

    this.usersService.deactiveUser(id).subscribe(
      data => {
        this.loading=false;
        this.data.changeMessage(true);

        this.status = 'success'
        this.title = 'Successfully Saved .. ';
        this.content = 'Please check !!'
        this.showToast(this.status, this.title, this.content);
        this.loading=false;


        console.log("kkkk")
        // console.log(token)

      },
      error => {
        this.loading = false;
        try {
          this.status = 'danger'
          this.title = 'Please check !!';
          this.content = error.error.message
          this.showToast(this.status, this.title, this.content);
        } catch (err) {
          this.status = 'danger'
          this.title = 'Please check !!';
          this.content = err.message;
          this.showToast(this.status, this.title, this.content);
        }
      },
    );
  }

  viewHistory(){
    this.dialogService.open(VendorModelHistoryComponent, {
      context: {
        title: 'This is a title passed to the dialog component',
        celldata:this.rowData

      },
    });
  }
  loginAsVendor(id){
    this.usersService.getTokenById(id).subscribe(
      data => {
        this.loading=false;

        this.status = 'success'
        this.title = 'Successfully Saved .. ';
        this.content = 'Please check !!'
        this.showToast(this.status, this.title, this.content);
        this.loading=false;

       localStorage.setItem('tempCurrentUser', localStorage.getItem('currentUser'));
       localStorage.setItem('tempToken', localStorage.getItem('token'));
       localStorage.setItem('currentUser', JSON.stringify(data.data));
       localStorage.setItem('token', data.data.token);
       console.log(data.data.token)
       this.location.replaceState("/pages/dashboard");

    //    this.router.navigateByUrl('/pages', { skipLocationChange: true }).then(() => {
    //     this.router.navigate(['/pages/dashboard']);
       
    // }); 
    window.location.reload();

//     setTimeout(()=>{    //<<<---    using ()=> syntax

//  }, 500);
    // console.log(token)

      },
      error => {
        this.loading = false;
        try {
          this.status = 'danger'
          this.title = 'Please check !!';
          this.content = error.error.message
          this.showToast(this.status, this.title, this.content);
        } catch (err) {
          this.status = 'danger'
          this.title = 'Please check !!';
          this.content = err.message;
          this.showToast(this.status, this.title, this.content);
        }
      },
    );
  }

  public showToast(type: NbComponentStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title ? `. ${title}` : '';

    this.toastrService.show(body, title, config);
  }

  
}

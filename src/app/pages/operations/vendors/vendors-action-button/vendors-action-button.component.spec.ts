import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorsActionButtonComponent } from './vendors-action-button.component';

describe('VendorsActionButtonComponent', () => {
  let component: VendorsActionButtonComponent;
  let fixture: ComponentFixture<VendorsActionButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendorsActionButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VendorsActionButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

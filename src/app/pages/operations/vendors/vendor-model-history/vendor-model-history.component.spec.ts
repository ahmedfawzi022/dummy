import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorModelHistoryComponent } from './vendor-model-history.component';

describe('VendorModelHistoryComponent', () => {
  let component: VendorModelHistoryComponent;
  let fixture: ComponentFixture<VendorModelHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendorModelHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VendorModelHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

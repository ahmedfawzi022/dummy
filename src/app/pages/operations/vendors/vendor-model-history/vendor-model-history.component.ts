import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'ngx-vendor-model-history',
  templateUrl: './vendor-model-history.component.html',
  styleUrls: ['./vendor-model-history.component.scss']
})
export class VendorModelHistoryComponent implements OnInit {
  @Input() title
  @Input()celldata;
  constructor() { }

  ngOnInit(): void {
  }

}

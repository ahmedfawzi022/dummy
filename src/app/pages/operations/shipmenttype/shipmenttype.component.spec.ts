import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShipmenttypeComponent } from './shipmenttype.component';

describe('ShipmenttypeComponent', () => {
  let component: ShipmenttypeComponent;
  let fixture: ComponentFixture<ShipmenttypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShipmenttypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShipmenttypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

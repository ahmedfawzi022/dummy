import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddShipmenttypeComponent } from './add-shipmenttype.component';

describe('AddShipmenttypeComponent', () => {
  let component: AddShipmenttypeComponent;
  let fixture: ComponentFixture<AddShipmenttypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddShipmenttypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddShipmenttypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

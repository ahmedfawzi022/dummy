import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../../services/users/users.service';
import { OperationsService } from '../../../services/operations/operations.service';

import { environment } from '../../../../environments/environment';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Product } from '../../../models/users/product'
import { ProductVendor } from '../../../models/operations/productvendor'
import { Router, ActivatedRoute } from '@angular/router';
import { DataService } from '../../../services/data/data.service';
import { CategoriesService } from '../../../services/categories/categories.service';
import { BaseComponent } from '../../general/base/base.component';
import {
  NbComponentStatus,
  NbGlobalLogicalPosition,
  NbGlobalPhysicalPosition,
  NbGlobalPosition,
  NbToastrService,
  NbToastrConfig,
} from '@nebular/theme';

@Component({
  selector: 'ngx-add-product',
  templateUrl:'./add-product.component.html',
  styleUrls: ['./add-product.component.scss']
})
export class AddProductComponent extends BaseComponent implements OnInit {
  product: Product
  productVendor:ProductVendor
  images = [];
  countries=[]
  catgories=[];
  subCatgories=[]
  vendorId:any;
  //Define Form Group with its controls
  addForm: FormGroup;
  idControl: FormControl;
  descriptionControl: FormControl;
  nameControl: FormControl;
  arNameControl:FormControl;
  priceControl:FormControl;
  isTopSellingControl:FormControl;
  brandLogoControl:FormControl;
  categoryIdControl:FormControl;
  subCategoryIdControl: FormControl;
  stockControl:FormControl
  discountControl: FormControl;
  isDiscountActiveControl:FormControl;
  galleryControl:FormControl;
  imageControl: FormControl;
  public imageSrc: string = '';
  currentUser:any;
  loading = false;

  constructor(public operationsService: OperationsService,public categoriesService:CategoriesService, public route: ActivatedRoute, public data: DataService, public router: Router, public toastrService: NbToastrService) {
    super(toastrService)
    this.product = new Product()
    this.productVendor = new ProductVendor()
  }

  ngOnInit(): void {
    // create form group and its form controls and
    this.createFormControls();
    this.createForm();
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));

    this.route.params.subscribe(params => {
      if(params['vendorId']){
      this.vendorId =params['vendorId']
      // this.getUserById(this.idControl.value)
      }
    });
    this.route.params.subscribe(params => {
      this.idControl.setValue(params['id'])
       this.getProductById(this.idControl.value)
      
    });

    this.getCategories();

  }

  // create form controls that we need to validate with their needed validators
  createFormControls() {
    this.nameControl = new FormControl(null, [
      Validators.required,
    ]),
    this.arNameControl = new FormControl(null, [
      Validators.required,
    ]),
    this.categoryIdControl = new FormControl(null, [
      Validators.required,
    ]),
      this.descriptionControl = new FormControl(null, [
      //  Validators.required,
      ]),
      this.stockControl = new FormControl(null, [
        Validators.required,
      ]),
this.isTopSellingControl = new FormControl(null, [
  Validators.required,
]),
      this.priceControl= new FormControl(null, [
        Validators.required,
      ]),

      this.galleryControl= new FormControl(null, [
        // Validators.required,
      ]),

    this.discountControl = new FormControl(null, [
    ]),
      this.subCategoryIdControl = new FormControl(null, [
        Validators.required,
      ]);
    this.imageControl = new FormControl('', [
     Validators.required,
    ]);
    this.isDiscountActiveControl= new FormControl(false, [
     // Validators.required,
     ]);
    this.idControl = new FormControl(null)
  }


  // add form controls to form group
  createForm() {
    this.addForm = new FormGroup({
      name: this.nameControl,
      arName:this.arNameControl,
      categoryId:this.categoryIdControl,
      description: this.descriptionControl,
      price:this.priceControl,
      discount: this.discountControl,
      isDiscountActive:this.isDiscountActiveControl,
      stock:this.stockControl,
      subCategoryId: this.subCategoryIdControl,
      gallery:this.galleryControl,
      image: this.imageControl,
      isTopSelling:this.isTopSellingControl,
      id: this.idControl
    });
  }
  getCategories(){
    this.loading=true;

    this.categoriesService.getCategoriesTable().subscribe(
      data => {
        console.log(data.data)
        this.catgories=data.data.list


  //      this.isActiveControl.setValue(data.data.isActive)
        // this.categories.isActive=this.isActiveControl.value

        // this.router.navigateByUrl(`/pages/categories/categorie`);


        console.log(data)
        // console.log(token)

      },
      error => {
        this.loading=false;

        // this.spinnerService.hide();
        try {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.loginerrorMessage);
        } catch (err) {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.errorMessage);
        }
      },
    );
  }
  onChangeCategory(event){
    console.log(event)
    this.getSubCategories(event)
  }
  getSubCategories(id){
    this.loading=true;

    this.categoriesService.getSubCategoryById(id).subscribe(
      data => {
        console.log(data.data)
        this.subCatgories=data.data.list


  //      this.isActiveControl.setValue(data.data.isActive)
        // this.categories.isActive=this.isActiveControl.value

        // this.router.navigateByUrl(`/pages/categories/categorie`);


        console.log(data)
        // console.log(token)

      },
      error => {
        this.loading=false;

        // this.spinnerService.hide();
        try {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.loginerrorMessage);
        } catch (err) {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.errorMessage);
        }
      },
    );
  }
  getProductById(id){
    this.loading=true;
    if(this.currentUser.roles[0]=="Vendor"){
      this.operationsService.getProductByIdForVendor(id).subscribe(
        data => {
          console.log(data.data)
          this.nameControl.setValue(data.data.name)
          this.arNameControl.setValue(data.data.arName)
          this.product.name = this.nameControl.value
          this.stockControl.setValue(data.data.stock)
          this.product.stock = this.stockControl.value
          this.categoryIdControl.setValue(data.data.categoryId._id)
          this.product.categoryId = this.categoryIdControl.value;
          this.getSubCategories(this.categoryIdControl.value)
          this.imageSrc = data?.data?.image;
          this.imageControl.setValue(data?.data?.image)
          this.descriptionControl.setValue(data.data.description)
          this.product.description = this.descriptionControl.value
          this.discountControl.setValue(data.data.discount)
          this.product.discount = this.discountControl.value
          this.subCategoryIdControl.setValue(data.data.subCategoryId._id)
          this.product.subCategoryId = this.subCategoryIdControl.value
          this.priceControl.setValue(data.data.price)
          this.isDiscountActiveControl.setValue(data.data.isDiscountActive)
          this.product.isDiscountActive = this.isDiscountActiveControl.value
          this.isTopSellingControl.setValue(data.data.isTopSelling)
          this.product.isTopSelling = this.isTopSellingControl.value
          this.galleryControl.setValue(data.data.gallery)
          this.product.gallery = this.galleryControl.value
          this.images=this.product.gallery
          this.loading=false;
        },
        error => {
          this.loading=false;
          try {
          } catch (err) {
          }
        },
      );
     
    }
    else{
      this.operationsService.getProductById(id).subscribe(
        data => {
          console.log(data.data)
          this.nameControl.setValue(data.data.name)
          this.arNameControl.setValue(data.data.arName)
          this.product.name = this.nameControl.value
          this.product.arName = this.arNameControl.value
          this.stockControl.setValue(data.data.stock)
          this.product.stock = this.stockControl.value
          this.categoryIdControl.setValue(data.data.categoryId._id)
          this.product.categoryId = this.categoryIdControl.value;
          this.getSubCategories(this.categoryIdControl.value)
          this.imageSrc = data?.data?.image;
          this.imageControl.setValue(data?.data?.image)
          this.descriptionControl.setValue(data.data.description)
          this.product.description = this.descriptionControl.value
          this.discountControl.setValue(data.data.discount)
          this.product.discount = this.discountControl.value
          this.subCategoryIdControl.setValue(data.data.subCategoryId._id)
          this.product.subCategoryId = this.subCategoryIdControl.value
          this.priceControl.setValue(data.data.price)
          this.isDiscountActiveControl.setValue(data.data.isDiscountActive)
          this.product.isDiscountActive = this.isDiscountActiveControl.value
          this.isTopSellingControl.setValue(data.data.isTopSelling)
          this.product.isTopSelling = this.isTopSellingControl.value
          this.galleryControl.setValue(data.data.gallery)
          this.product.gallery = this.galleryControl.value
          this.images=this.product.gallery
          this.loading=false;
        },
        error => {
          this.loading=false;
          try {
          } catch (err) {
          }
        },
      );
    }
   
  }
  handleInputProfileChange(e) {
    var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
    var pattern = /image-*/;
    var reader = new FileReader();
    if (!file.type.match(pattern)) {
      alert('invalid format');
      return;
    }
    reader.onload = this._handleProfileReaderLoaded.bind(this);
    reader.readAsDataURL(file);
  }
  _handleProfileReaderLoaded(e) {
    let reader = e.target;
    this.imageSrc = reader.result;
    console.log(this.imageSrc)
    this.imageControl.setValue(this.imageSrc)
  }

 
  onFileChange(event){
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
              var reader = new FileReader();
 
              reader.onload = (event:any) => {
                 this.images.push(event.target.result); 
                 this.product.gallery=this.images;
                 this.productVendor.gallery=this.images;

                 this.galleryControl.setValue(this.product.gallery)

                //  this.myForm.patchValue({
                //     fileSource: this.images
                //  });
              }

              reader.readAsDataURL(event.target.files[i]);
      }
  }
  }

  clear(){
    this.images=[];
    this.product.gallery=this.images;
this.galleryControl.setValue([])

  }
  save() {

    this.loading = true;
    this.product.name = this.nameControl.value
    this.product.arName = this.arNameControl.value
    this.product.categoryId = this.categoryIdControl.value
    this.product.description = this.descriptionControl.value;
    this.product.discount = this.discountControl.value
    this.product.stock=this.stockControl.value;
    this.product.subCategoryId = this.subCategoryIdControl.value
    this.product.image = this.imageControl.value
    this.product.price=this.priceControl.value;
    this.product.vendorId=this.vendorId;
    this.product.isDiscountActive=this.isDiscountActiveControl.value;
    this.product.isTopSelling = this.isTopSellingControl.value

    console.log(this.product);
    if(this.currentUser.roles[0]=="Vendor"){
      this.productVendor.name = this.nameControl.value
      this.productVendor.arName = this.arNameControl.value
      this.productVendor.categoryId = this.categoryIdControl.value
      this.productVendor.description = this.descriptionControl.value;
      this.productVendor.discount = this.discountControl.value
      this.productVendor.stock=this.stockControl.value;
      this.productVendor.subCategoryId = this.subCategoryIdControl.value
      this.productVendor.image = this.imageControl.value
      this.productVendor.price=this.priceControl.value;
      this.productVendor.isDiscountActive=this.isDiscountActiveControl.value;
      this.productVendor.isTopSelling = this.isTopSellingControl.value  
      if (this.idControl.value) {
        this.idControl.disable();
        this.operationsService.saveProductForVendor(this.productVendor, this.idControl.value).subscribe(
          data => {
            this.router.navigateByUrl(`/pages/operations/products`);
            this.loading = false;
  
          },
          error => {
            console.log(error)
            this.loading = false;
            try {
              this.status='danger'
              this.title= 'Please check !!';
              this.content=error.error.message
              this.showToast(this.status, this.title, this.content);
            } catch (err) {
              this.status='danger'
              this.title='Please check !!';
              this.content=err.message;
               this.showToast(this.status, this.title,this.content );
            }
          },
        );
  
      } else {
        this.operationsService.saveProductForVendor(this.productVendor).subscribe(
          data => {
            this.router.navigateByUrl(`/pages/operations/products`);
  
            this.status = 'success'
            this.title = 'Successfully Saved .. ';
            this.content = 'Please check !!'
            this.showToast(this.status, this.title, this.content);
            this.data.changeMessage(true)
            this.loading = false;
          },
          error => {
            this.loading = false;
  
            console.log(error)
            try {
              this.status='danger'
              this.title='Please check !!';
              this.content=error.error.message
               this.showToast(this.status, this.title,this.content );
            } catch (err) {
              this.status='danger'
              this.title='Please check !!';
              this.content=err.message;
               this.showToast(this.status, this.title,this.content );
            }
          },
        );
  
      }
  

  }else{

    if (this.idControl.value) {
      this.idControl.disable();
      this.operationsService.saveProduct(this.product, this.idControl.value).subscribe(
        data => {
          this.router.navigateByUrl(`/pages/operations/products`);
          this.loading = false;

        },
        error => {
          console.log(error)
          this.loading = false;
          try {
            this.status='danger'
            this.title= 'Please check !!';
            this.content=error.error.message
            this.showToast(this.status, this.title, this.content);
          } catch (err) {
            this.status='danger'
            this.title='Please check !!';
            this.content=err.message;
             this.showToast(this.status, this.title,this.content );
          }
        },
      );

    } else {
      this.operationsService.saveProduct(this.product).subscribe(
        data => {
          this.router.navigateByUrl(`/pages/operations/products`);

          this.status = 'success'
          this.title = 'Successfully Saved .. ';
          this.content = 'Please check !!'
          this.showToast(this.status, this.title, this.content);
          this.data.changeMessage(true)
          this.loading = false;
        },
        error => {
          this.loading = false;

          console.log(error)
          try {
            this.status='danger'
            this.title='Please check !!';
            this.content=error.error.message
             this.showToast(this.status, this.title,this.content );
          } catch (err) {
            this.status='danger'
            this.title='Please check !!';
            this.content=err.message;
             this.showToast(this.status, this.title,this.content );
          }
        },
      );

    }
 

  }

  }

}


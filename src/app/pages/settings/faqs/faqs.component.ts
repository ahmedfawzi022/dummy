import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../../services/users/users.service';
import { LocalDataSource, ServerDataSource } from 'ng2-smart-table';
import { environment } from '../../../../environments/environment';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Operators } from '../../../models/users/operators'
import { Router, ActivatedRoute } from '@angular/router';
import { SettingsService } from '../../../services/settings/settings.service';

import { DataService } from '../../../services/data/data.service';
import { BaseComponent } from '../../general/base/base.component';
// import { ToastrService } from 'ngx-toastr';
import {
  NbComponentStatus,
  NbGlobalLogicalPosition,
  NbGlobalPhysicalPosition,
  NbGlobalPosition,
  NbToastrService,
  NbToastrConfig,
} from '@nebular/theme';
import { HttpClient } from '@angular/common/http';
import { SmartTableData } from '../../../@core/data/smart-table';
import { DomSanitizer } from '@angular/platform-browser';
@Component({
  selector: 'ngx-faqs',
  templateUrl: './faqs.component.html',
  styleUrls: ['./faqs.component.scss']
})
export class FaqsComponent implements OnInit {
  loading = false;
  fubUrl: any;
  countries=[]
  source: ServerDataSource

  settings = {
    mode: 'external',

    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
  },
    columns: {
      question: {
        title: 'Question',
        type: 'string',
        filter:false

      },
      answer: {
        title: 'Answer',
        type: 'string',
        filter:false
      }


    },
  };


  constructor(private service: SmartTableData,public data:DataService,public settingsService:SettingsService, private httpClient: HttpClient,public usersService:UsersService, private router: Router, private _domSanitizer: DomSanitizer) {
    this.fubUrl = environment.FUBS_API_URL

  }
  ngOnInit(): void {
    this.data.currentMessage.subscribe(
      data => {
        this.getservice()
      });
    this.getservice();
  }

  getservice() {
    this.loading = true;
    this.source = new ServerDataSource(this.httpClient, {
      endPoint: `${this.fubUrl}/faq`,
      pagerPageKey: 'page',
      pagerLimitKey: 'limit',
      filterFieldKey: '#field#_like',
      totalKey: 'data.count',
      dataKey: 'data.list',
    }
    );
    this.loading = false;

  }


  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      this.onDelete(event)
    } else {
      event.confirm.reject();
    }
  }

  onAdd(){
    this.router.navigateByUrl('/pages/settings/add-faq');

  }
  onEdit(event){
    console.log(event.data._id)
    this.router.navigateByUrl(`/pages/settings/add-faq/${event.data._id}`);
  }

  onDelete(event) {
    this.settingsService.deleteFaq(event.data._id)

        .subscribe(
          data => {
            // this.del.nativeElement.click();
            this.getservice();
            // this.makeToast(this.configurationDataObject.feesDeleteSuccessfullMessage);

          },
          error => {
//            this.del.nativeElement.click();
            try {
              // if (error.error.errorCode)
              // this.openModal(error.error.errorCode, error.error.errorMessage);
              // this.openModal(this.configurationDataObject.error, this.configurationDataObject.errorMessage);
            } catch (err) {
              // this.openModal(this.configurationDataObject.error, this.configurationDataObject.errorMessage);
            }
          },
      );

  }
}





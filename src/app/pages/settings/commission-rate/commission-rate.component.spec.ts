import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommissionRateComponent } from './commission-rate.component';

describe('CommissionRateComponent', () => {
  let component: CommissionRateComponent;
  let fixture: ComponentFixture<CommissionRateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommissionRateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommissionRateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../../../services/settings/settings.service';
import { environment } from '../../../../environments/environment';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Operators } from '../../../models/users/operators'
import { Router, ActivatedRoute } from '@angular/router';
import { DataService } from '../../../services/data/data.service';
import { BaseComponent } from '../../general/base/base.component';
import { LocationsService } from '../../../services/locations/locations.service';

// import { ToastrService } from 'ngx-toastr';
import {
  NbComponentStatus,
  NbGlobalLogicalPosition,
  NbGlobalPhysicalPosition,
  NbGlobalPosition,
  NbToastrService,
  NbToastrConfig,
} from '@nebular/theme';
@Component({
  selector: 'ngx-commission-rate',
  templateUrl: './commission-rate.component.html',
  styleUrls: ['./commission-rate.component.scss']
})
export class CommissionRateComponent extends BaseComponent implements OnInit {
  operators: Operators
  countries=[]

  //Define Form Group with its controls
  addForm: FormGroup;
  idControl: FormControl;
  valueControl: FormControl;
  descriptionControl: FormControl;
  loading = false;

  constructor(public settingsService: SettingsService,public locationsService:LocationsService, public route: ActivatedRoute, public data: DataService, public router: Router, public toastrService: NbToastrService) {
    super(toastrService)
    this.operators = new Operators()
  }

  ngOnInit(): void {
    // create form group and its form controls and
    this.createFormControls();
    this.createForm();

    // this.route.params.subscribe(params => {
    //   if (params['id']) {
    //     this.idControl.setValue(params['id'])
    //     this.getAboutUsById(this.idControl.value)
    //   }
    // });getComissionRateById
    this.getComissionRateById()

  }

  getCountry(){
    this.loading=true;

    this.locationsService.getCountry().subscribe(
      data => {
        console.log(data.data)
        this.countries=data.data.list


  //      this.isActiveControl.setValue(data.data.isActive)
        // this.categories.isActive=this.isActiveControl.value

        // this.router.navigateByUrl(`/pages/categories/categorie`);


        console.log(data)
        // console.log(token)

      },
      error => {
        this.loading=false;

        // this.spinnerService.hide();
        try {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.loginerrorMessage);
        } catch (err) {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.errorMessage);
        }
      },
    );
  }

  // create form controls that we need to validate with their needed validators
  createFormControls() {
    this.valueControl = new FormControl(null, [
      Validators.required,
    ]),


    this.idControl = new FormControl(null)
  }


  // add form controls to form group
  createForm() {
    this.addForm = new FormGroup({
      value: this.valueControl,
      id: this.idControl
    });
  }

  getComissionRateById() {
    this.loading = true;

    this.settingsService.getComissionRateById().subscribe(
      data => {
        console.log(data.data)
        this.valueControl.setValue(data.data.value)
        this.loading = false;

        //      this.isActiveControl.setValue(data.data.isActive)
        // this.categories.isActive=this.isActiveControl.value

        // this.router.navigateByUrl(`/pages/categories/categorie`);


        console.log(data)
        // console.log(token)

      },
      error => {
        this.loading = false;

        // this.spinnerService.hide();
        try {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.loginerrorMessage);
        } catch (err) {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.errorMessage);
        }
      },
    );
  }


  save() {

    this.loading = true;

 
      this.idControl.disable();
      this.settingsService.saveComissionRate(this.addForm.value).subscribe(
        data => {
          this.router.navigateByUrl(`/pages/settings/app-settings`);

          this.status = 'success'
          this.title = 'Successfully Saved .. ';
          this.content = 'Please check !!'
          this.showToast(this.status, this.title, this.content);
          this.data.changeMessage(true)
          this.loading = false;
        },
        error => {
          this.loading = false;

          console.log(error)
          // this.spinnerService.hide();
          try {
            this.status = 'danger'
            this.title = 'Please check !!';
            this.content = error.error.message
            this.showToast(this.status, this.title, this.content);
          } catch (err) {
            this.status = 'danger'
            this.title = 'Please check !!';
            this.content = err.message;
            this.showToast(this.status, this.title, this.content);
            // this.openModal(this.loginDataObject.error, this.loginDataObject.errorMessage);
          }
        },
      );

 

  }

}


import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbMenuModule } from '@nebular/theme';
import { ThemeModule } from '../../@theme/theme.module';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {
  NbAccordionModule,
  NbButtonModule,
  NbSpinnerModule,
  NbListModule,
  NbRouteTabsetModule,
  NbStepperModule,
  NbTabsetModule, NbUserModule,
  NbCardModule, NbIconModule, NbInputModule, NbTreeGridModule,
  NbActionsModule,
  NbCheckboxModule,
  NbDatepickerModule,
  NbTooltipModule,
  NbRadioModule,
  NbSelectModule,
} from '@nebular/theme';

import { SettingsRoutingModule } from './settings-routing.module';
import { SettingComponent } from './setting/setting.component';
import { SettingsComponent } from './settings.component';
import { AddFaqComponent } from './add-faq/add-faq.component';
import { AllSettingsComponent } from './all-settings/all-settings.component';
import { AddSettingComponent } from './add-setting/add-setting.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { SettingsActionButtonComponent } from './all-settings/settings-action-button/settings-action-button.component';
import { FaqsComponent } from './faqs/faqs.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { CommissionRateComponent } from './commission-rate/commission-rate.component';
import { CountrySettingComponent } from './all-settings/country-setting/country-setting.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { AddPrivacyComponent } from './add-privacy/add-privacy.component';


@NgModule({
  declarations: [SettingComponent,SettingsComponent, AddFaqComponent, AllSettingsComponent, AddSettingComponent, SettingsActionButtonComponent, FaqsComponent, AboutUsComponent, CommissionRateComponent, CountrySettingComponent, PrivacyComponent, AddPrivacyComponent],
  imports: [
    CommonModule,
    SettingsRoutingModule,
    NbMenuModule,
    NbCardModule,
    NbTreeGridModule,
    NbIconModule,
    NbInputModule,
    NgSelectModule,
    NbSpinnerModule,
    ThemeModule,
    Ng2SmartTableModule,
    FormsModule,
    ReactiveFormsModule,
    NbTabsetModule,
    NbRouteTabsetModule,
    NbStepperModule,
    NbCardModule,
    NbButtonModule,
    NbListModule,
    NbAccordionModule,
    NbTooltipModule,
    NbUserModule,
    NbActionsModule,
    NbCheckboxModule,
    NbDatepickerModule,
    NbRadioModule,
    NbSelectModule
  ]
})
export class SettingsModule { }

import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../../services/users/users.service';
import { LocalDataSource, ServerDataSource } from 'ng2-smart-table';
import { environment } from '../../../../environments/environment';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Operators } from '../../../models/users/operators'
import { Router, ActivatedRoute } from '@angular/router';
import { SettingsService } from '../../../services/settings/settings.service';

import { DataService } from '../../../services/data/data.service';
import { BaseComponent } from '../../general/base/base.component';
// import { ToastrService } from 'ngx-toastr';
import {
  NbComponentStatus,
  NbGlobalLogicalPosition,
  NbGlobalPhysicalPosition,
  NbGlobalPosition,
  NbToastrService,
  NbToastrConfig,
} from '@nebular/theme';
import { HttpClient } from '@angular/common/http';
import { SmartTableData } from '../../../@core/data/smart-table';
import { DomSanitizer } from '@angular/platform-browser';
import { SettingsActionButtonComponent } from './settings-action-button/settings-action-button.component';
import { CountrySettingComponent } from './country-setting/country-setting.component';

@Component({
  selector: 'ngx-all-settings',
  templateUrl: './all-settings.component.html',
  styleUrls: ['./all-settings.component.scss']
})
export class AllSettingsComponent implements OnInit {
  loading = false;
  fubUrl: any;
  countries = []
  source: ServerDataSource

  settings = {
    mode: 'external',

    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
    },
    columns: {
      country: {
        title: 'Country',
        type: 'custom',
        renderComponent: CountrySettingComponent,
        editor: {
          type: 'custom',
          component: CountrySettingComponent
        },
        sort: false,
        filter: false,
        width: '100px'
      },
      taxRate: {
        title: 'Tax Rate',
        type: 'string',
        filter: false,

      },
      pointingEquation: {
        title: 'Pointing Equation',
        type: 'string',
        filter: false,

      },
      redeemEquation: {
        title: 'Redeem Equation',
        type: 'string',
        filter: false,

      },
      isActive: {
        title: 'is Active',
        type: 'string',
        filter: false,

      },
      isDefault: {
        title: 'is Default',
        type: 'string',
        filter: false,

      },

      view: {
        title: 'Call Action',
        type: 'custom',
        renderComponent: SettingsActionButtonComponent,
        editor: {
          type: 'custom',
          component: SettingsActionButtonComponent
        },
        sort: false,
        filter: false,
        width: '220px'
      },

    },
  };


  constructor(private service: SmartTableData, public data: DataService, public settingsService: SettingsService, private httpClient: HttpClient, public usersService: UsersService, private router: Router, private _domSanitizer: DomSanitizer) {
    this.fubUrl = environment.FUBS_API_URL

  }
  ngOnInit(): void {
    this.data.currentMessage.subscribe(
      data => {
        this.getservice()
      });
    this.getservice();
    this.getCountry()
  }

  getservice() {
    this.loading = true;
    this.source = new ServerDataSource(this.httpClient, {
      endPoint: `${this.fubUrl}/settings`,
      pagerPageKey: 'page',
      pagerLimitKey: 'limit',
      filterFieldKey: '#field#_like',
      totalKey: 'data.count',
      dataKey: 'data.list',
    }
    );
    this.loading = false;

  }
  getCountry() {
    this.loading = true;

    this.settingsService.getCountry().subscribe(
      data => {
        console.log(data.data)
        this.countries = data.data.list


        //      this.isActiveControl.setValue(data.data.isActive)
        // this.categories.isActive=this.isActiveControl.value

        // this.router.navigateByUrl(`/pages/categories/categorie`);


        console.log(data)
        // console.log(token)

      },
      error => {
        this.loading = false;

        // this.spinnerService.hide();
        try {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.loginerrorMessage);
        } catch (err) {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.errorMessage);
        }
      },
    );
  }
  // getOperators() {
  //   this.loading = true;
  //   this.usersService.getOperators()
  //     .subscribe(
  //       data => {
  //         console.log(data)
  //         this.loading = false;

  //        // this.source = new LocalDataSource(data.data.list)
  //       }, error => {
  //         console.log(error)
  //         this.loading = false;


  //         // this.spinnerService.hide();
  //         try {
  //           // this.openModal(this.loginDataObject.error, this.loginDataObject.loginerrorMessage);
  //         } catch (err) {
  //           // this.openModal(this.loginDataObject.error, this.loginDataObject.errorMessage);
  //         }
  //       },

  //     )

  //   // this.resultData = true;

  // }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      this.onDelete(event)
    } else {
      event.confirm.reject();
    }
  }

  onAdd() {
    this.router.navigateByUrl('/pages/settings/add-setting');

  }
  onEdit(event) {
    console.log(event.data._id)
    this.router.navigateByUrl(`/pages/settings/add-setting/${event.data._id}`);
  }

  onDelete(event) {
    this.settingsService.deleteSetting(event.data._id)

      .subscribe(
        data => {
          // this.del.nativeElement.click();
          this.getservice();
          // this.makeToast(this.configurationDataObject.feesDeleteSuccessfullMessage);

        },
        error => {
          //            this.del.nativeElement.click();
          try {
            // if (error.error.errorCode)
            // this.openModal(error.error.errorCode, error.error.errorMessage);
            // this.openModal(this.configurationDataObject.error, this.configurationDataObject.errorMessage);
          } catch (err) {
            // this.openModal(this.configurationDataObject.error, this.configurationDataObject.errorMessage);
          }
        },
      );

  }
}





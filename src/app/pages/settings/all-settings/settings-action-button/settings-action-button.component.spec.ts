import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingsActionButtonComponent } from './settings-action-button.component';

describe('SettingsActionButtonComponent', () => {
  let component: SettingsActionButtonComponent;
  let fixture: ComponentFixture<SettingsActionButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SettingsActionButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsActionButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

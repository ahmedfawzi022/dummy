import { Component, OnInit,Input } from '@angular/core';
import { NbDialogService } from '@nebular/theme';
import { DefaultEditor, ViewCell } from 'ng2-smart-table';
import {  SettingsService} from '../../../../services/settings/settings.service';
import { Router } from '@angular/router';
import { DataService } from '../../../../services/data/data.service';
import {
  NbComponentStatus,
  NbGlobalLogicalPosition,
  NbGlobalPhysicalPosition,
  NbGlobalPosition,
  NbToastrConfig,
  NbToastrService
} from '@nebular/theme';
@Component({
  selector: 'ngx-settings-action-button',
  templateUrl: './settings-action-button.component.html',
  styleUrls: ['./settings-action-button.component.scss']
})
export class SettingsActionButtonComponent extends DefaultEditor implements OnInit, ViewCell {
  constructor(private dialogService: NbDialogService,public toastrService: NbToastrService,public  data:DataService,public settingsService:SettingsService, private router: Router) { super()}
  @Input() value: string | number;
  @Input() rowData: any;
  loading:any;
  destroyByClick = true;
  duration = 2000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;
  status: NbComponentStatus = 'primary';
  title = 'HI there!';
  content = `I'm cool toaster!`;
  ngOnInit(): void {
    console.log(this.rowData)
    console.log(this.value)
  }

  active(id){
    this.loading=true;

    this.settingsService.activeSetting(id).subscribe(
      data => {
        this.data.changeMessage(true);
          // this.del.nativeElement.click();
          this.status = 'success'
          this.title = 'Successfully Saved .. ';
          this.content = 'Please check !!'
          this.showToast(this.status, this.title, this.content);
          this.loading=false;

      },
      error => {
        this.loading = false;
        try {
          this.status = 'danger'
          this.title = 'Please check !!';
          this.content = error.error.message
          this.showToast(this.status, this.title, this.content);
        } catch (err) {
          this.status = 'danger'
          this.title = 'Please check !!';
          this.content = err.message;
          this.showToast(this.status, this.title, this.content);
        }
      },
    );
  }



  deactive(id){
    this.loading=true;

    this.settingsService.deactiveSetting(id).subscribe(
      data => {
        this.data.changeMessage(true);
        this.status = 'success'
        this.title = 'Successfully Saved .. ';
        this.content = 'Please check !!'
        this.showToast(this.status, this.title, this.content);
        this.loading=false;

      },
      error => {
        this.loading = false;
        try {
          this.status = 'danger'
          this.title = 'Please check !!';
          this.content = error.error.message
          this.showToast(this.status, this.title, this.content);
        } catch (err) {
          this.status = 'danger'
          this.title = 'Please check !!';
          this.content = err.message;
          this.showToast(this.status, this.title, this.content);
        }
      },
    );
  }

  setDefault(id){
    this.loading=true;

    this.settingsService.setDefault(id).subscribe(
      data => {
        this.data.changeMessage(true);
        this.status = 'success'
        this.title = 'Successfully Saved .. ';
        this.content = 'Please check !!'
        this.showToast(this.status, this.title, this.content);
        this.loading=false;

      },
      error => {
        this.loading = false;
        try {
          this.status = 'danger'
          this.title = 'Please check !!';
          this.content = error.error.message
          this.showToast(this.status, this.title, this.content);
        } catch (err) {
          this.status = 'danger'
          this.title = 'Please check !!';
          this.content = err.message;
          this.showToast(this.status, this.title, this.content);
        }
      },
    );
  }

  public showToast(type: NbComponentStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title ? `. ${title}` : '';

    this.toastrService.show(body, title, config);
  }
  
}

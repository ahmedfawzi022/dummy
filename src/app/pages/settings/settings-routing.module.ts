import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SettingComponent } from './setting/setting.component';
import { SettingsComponent } from './settings.component';
import { AddFaqComponent } from './add-faq/add-faq.component';
import { AllSettingsComponent } from './all-settings/all-settings.component';
import { AddSettingComponent } from './add-setting/add-setting.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { AddPrivacyComponent } from './add-privacy/add-privacy.component';


const routes: Routes = [{
  path: '',
  component: SettingsComponent,
  children: [

    {
      path: 'app-settings',
      component: SettingComponent,
      // canActivate: [RoleGuard],
      // data: {
      //   expectedRole: 'Read Configuration Template'
      // },
    },
    {
      path: 'add-faq',
      component: AddFaqComponent,
      // canActivate: [RoleGuard],
      // data: {
      //   expectedRole: 'Read Configuration Template'
      // },
    },
    {
      path: 'add-faq/:id',
      component: AddFaqComponent,
      // canActivate: [RoleGuard],
      // data: {
      //   expectedRole: 'Read Configuration Template'
      // },
    },
    {
      path: 'add-privacy',
      component: AddPrivacyComponent,
      // canActivate: [RoleGuard],
      // data: {
      //   expectedRole: 'Read Configuration Template'
      // },
    },
    {
      path: 'add-privacy/:id',
      component: AddPrivacyComponent,
      // canActivate: [RoleGuard],
      // data: {
      //   expectedRole: 'Read Configuration Template'
      // },
    },
    {
      path: 'add-setting',
      component: AddSettingComponent,
      // canActivate: [RoleGuard],
      // data: {
      //   expectedRole: 'Read Configuration Template'
      // },
    },
    {
      path: 'add-setting/:id',
      component: AddSettingComponent,
      // canActivate: [RoleGuard],
      // data: {
      //   expectedRole: 'Read Configuration Template'
      // },
    },
    {
      path: 'settings',
      component: AllSettingsComponent,
      // canActivate: [RoleGuard],
      // data: {
      //   expectedRole: 'Read Configuration Template'
      // },
    }
 
  ],
}];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsRoutingModule { }

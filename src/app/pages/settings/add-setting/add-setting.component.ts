import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../../../services/settings/settings.service';
import { environment } from '../../../../environments/environment';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Operators } from '../../../models/users/operators'
import { Router, ActivatedRoute } from '@angular/router';
import { DataService } from '../../../services/data/data.service';
import { BaseComponent } from '../../general/base/base.component';
import { LocationsService } from '../../../services/locations/locations.service';

// import { ToastrService } from 'ngx-toastr';
import {
  NbComponentStatus,
  NbGlobalLogicalPosition,
  NbGlobalPhysicalPosition,
  NbGlobalPosition,
  NbToastrService,
  NbToastrConfig,
} from '@nebular/theme';

@Component({
  selector: 'ngx-add-setting',
  templateUrl:'./add-setting.component.html',
  styleUrls: ['./add-setting.component.scss']
})
export class AddSettingComponent extends BaseComponent implements OnInit {
  operators: Operators
  countries=[]

  //Define Form Group with its controls
  addForm: FormGroup;
  idControl: FormControl;
  redeemEquationControl: FormControl;
  countryIdControl: FormControl;
  taxRateControl: FormControl;
  pointingEquationControl: FormControl;
  loading = false;

  constructor(public settingsService: SettingsService,public locationsService:LocationsService, public route: ActivatedRoute, public data: DataService, public router: Router, public toastrService: NbToastrService) {
    super(toastrService)
    this.operators = new Operators()
  }

  ngOnInit(): void {
    // create form group and its form controls and
    this.createFormControls();
    this.createForm();
    this.getCountry()

    this.route.params.subscribe(params => {
      if (params['id']) {
        this.idControl.setValue(params['id'])
        this.getSettingById(this.idControl.value)
      }
    });
  }

  getCountry(){
    this.loading=true;

    this.locationsService.getCountry().subscribe(
      data => {
        console.log(data.data)
        this.countries=data.data.list


  //      this.isActiveControl.setValue(data.data.isActive)
        // this.categories.isActive=this.isActiveControl.value

        // this.router.navigateByUrl(`/pages/categories/categorie`);


        console.log(data)
        // console.log(token)

      },
      error => {
        this.loading=false;

        // this.spinnerService.hide();
        try {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.loginerrorMessage);
        } catch (err) {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.errorMessage);
        }
      },
    );
  }

  // create form controls that we need to validate with their needed validators
  createFormControls() {
    this.countryIdControl = new FormControl(null, [
      Validators.required,
    ]),
      this.taxRateControl = new FormControl(null, [
        Validators.required,
      ]),
      this.redeemEquationControl = new FormControl(null, [
        Validators.required,
      ]),

    this.pointingEquationControl = new FormControl(null, [
      Validators.required,
    ]),

    this.idControl = new FormControl(null)
  }


  // add form controls to form group
  createForm() {
    this.addForm = new FormGroup({
      countryId: this.countryIdControl,
      taxRate: this.taxRateControl,
      redeemEquation: this.redeemEquationControl,
      pointingEquation: this.pointingEquationControl,
      id: this.idControl
    });
  }

  getSettingById(id) {
    this.loading = true;

    this.settingsService.getSettingById(id).subscribe(
      data => {
        console.log(data.data)
        this.countryIdControl.setValue(data.data.countryId._id)
        this.taxRateControl.setValue(data.data.taxRate)
        this.pointingEquationControl.setValue(data.data.pointingEquation)
        this.redeemEquationControl.setValue(data.data.redeemEquation)
        this.loading = false;

        //      this.isActiveControl.setValue(data.data.isActive)
        // this.categories.isActive=this.isActiveControl.value

        // this.router.navigateByUrl(`/pages/categories/categorie`);


        console.log(data)
        // console.log(token)

      },
      error => {
        this.loading = false;

        // this.spinnerService.hide();
        try {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.loginerrorMessage);
        } catch (err) {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.errorMessage);
        }
      },
    );
  }


  save() {

    this.loading = true;

    if (this.idControl.value) {
      this.idControl.disable();
      // this.categories.isActive=this.isActiveControl.value
      this.settingsService.saveSetting(this.addForm.value, this.idControl.value).subscribe(
        data => {
          this.router.navigateByUrl(`/pages/settings/app-settings`);

          // this.status='danger'
          // this.title='Successfully Saved .. ';
          // this.content='Please check !!'
          // this.showToast(this.status, this.title, this.content);
          // this.data.changeMessage(this.datasaved)
          this.loading = false;

        },
        error => {
          console.log(error)
          this.loading = false;
          // this.spinnerService.hide();
          try {
            this.status = 'danger'
            this.title = 'Please check !!';
            this.content = error.error.message
            this.showToast(this.status, this.title, this.content);
          } catch (err) {
            this.status = 'danger'
            this.title = 'Please check !!';
            this.content = err.message;
            this.showToast(this.status, this.title, this.content);
            // this.openModal(this.loginDataObject.error, this.loginDataObject.errorMessage);
          }
        },
      );

    } else {
      this.idControl.disable();
      this.settingsService.saveSetting(this.addForm.value).subscribe(
        data => {
          this.router.navigateByUrl(`/pages/settings/app-settings`);

          this.status = 'success'
          this.title = 'Successfully Saved .. ';
          this.content = 'Please check !!'
          this.showToast(this.status, this.title, this.content);
          this.data.changeMessage(true)
          this.loading = false;
        },
        error => {
          this.loading = false;

          console.log(error)
          // this.spinnerService.hide();
          try {
            this.status = 'danger'
            this.title = 'Please check !!';
            this.content = error.error.message
            this.showToast(this.status, this.title, this.content);
          } catch (err) {
            this.status = 'danger'
            this.title = 'Please check !!';
            this.content = err.message;
            this.showToast(this.status, this.title, this.content);
            // this.openModal(this.loginDataObject.error, this.loginDataObject.errorMessage);
          }
        },
      );

    }


  }

}

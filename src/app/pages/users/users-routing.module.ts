import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersComponent } from './users.component';
import { AdministratorComponent } from './administrator/administrator.component';
import { OperatorComponent } from './operator/operator.component';
import { AddAdministratorComponent } from './add-administrator/add-administrator.component';
import { AddOperatorComponent } from './add-operator/add-operator.component';
import { UserProfileComponent } from './user-profile/user-profile.component';


const routes: Routes = [{
  path: '',
  component: UsersComponent,
  children: [

    {
      path: 'administrator',
      component: AdministratorComponent,
      // canActivate: [RoleGuard],
      // data: {
      //   expectedRole: 'Read Configuration Template'
      // },
    },
    {
      path: 'user-profile',
      component: UserProfileComponent,
      // canActivate: [RoleGuard],
      // data: {
      //   expectedRole: 'Read Configuration Template'
      // },
    },
    {
      path: 'add-administrator',
      component: AddAdministratorComponent,
      // canActivate: [RoleGuard],
      // data: {
      //   expectedRole: 'Read Configuration Template'
      // },
    },
    {
      path: 'add-administrator/:id',
      component: AddAdministratorComponent,
      // canActivate: [RoleGuard],
      // data: {
      //   expectedRole: 'Read Configuration Template'
      // },
    },
    {
      path: 'operator',
      component: OperatorComponent,
      // canActivate: [RoleGuard],
      // data: {
      //   expectedRole: 'Read Configuration Template'
      // },
    },
    {
      path: 'add-operator',
      component: AddOperatorComponent,
      // canActivate: [RoleGuard],
      // data: {
      //   expectedRole: 'Read Configuration Template'
      // },
    },
    {
      path: 'add-operator/:id',
      component: AddOperatorComponent,
      // canActivate: [RoleGuard],
      // data: {
      //   expectedRole: 'Read Configuration Template'
      // },
    },
  ],
}];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UsersRoutingModule { }

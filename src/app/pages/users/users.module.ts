import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersComponent } from './users.component';
import { NbMenuModule } from '@nebular/theme';
import { ThemeModule } from '../../@theme/theme.module';
import { UsersRoutingModule } from './users-routing.module';
import { AdministratorComponent } from './administrator/administrator.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { OperatorComponent } from './operator/operator.component';
import { AddAdministratorComponent } from './add-administrator/add-administrator.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';

import {
  NbAccordionModule,
  NbButtonModule,
  NbListModule,
  NbRouteTabsetModule,
  NbStepperModule,
  NbTabsetModule, NbUserModule,
  NbCardModule, NbIconModule, NbInputModule, NbTreeGridModule,
  NbActionsModule,
  NbCheckboxModule,
  NbDatepickerModule,
  NbRadioModule,
  NbSelectModule,
} from '@nebular/theme';
import { AddOperatorComponent } from './add-operator/add-operator.component';
import { UserProfileComponent } from './user-profile/user-profile.component';

@NgModule({
  declarations: [AdministratorComponent, UsersComponent, OperatorComponent, AddAdministratorComponent, AddOperatorComponent, UserProfileComponent],
  imports: [
    CommonModule,
    NbMenuModule,
    NbCardModule,
    NbTreeGridModule,
    NbIconModule,
    NbInputModule,
    ThemeModule,
    Ng2SmartTableModule,
    UsersRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule,
    NbTabsetModule,
    NbRouteTabsetModule,
    NbStepperModule,
    NbCardModule,
    NbButtonModule,
    NbListModule,
    NbAccordionModule,
    NbUserModule,
    NbActionsModule,
    NbCheckboxModule,
    NbDatepickerModule,
    NbRadioModule,
    NbSelectModule,
  ],
})
export class UsersModule { }

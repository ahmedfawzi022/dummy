import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../../services/users/users.service';
import { environment } from '../../../../environments/environment';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Operators } from '../../../models/users/operators'
import { Router, ActivatedRoute } from '@angular/router';
import { DataService } from '../../../services/data/data.service';
import { BaseComponent } from '../../general/base/base.component';
import {
  NbComponentStatus,
  NbGlobalLogicalPosition,
  NbGlobalPhysicalPosition,
  NbGlobalPosition,
  NbToastrService,
  NbToastrConfig,
} from '@nebular/theme';
@Component({
  selector: 'ngx-add-administrator',
  templateUrl: './add-administrator.component.html',
  styleUrls: ['./add-administrator.component.scss'],
})
export class AddAdministratorComponent extends BaseComponent implements OnInit {
  operators: Operators
  countries=[];
  //Define Form Group with its controls
  addForm: FormGroup;
  idControl: FormControl;
  userNameControl: FormControl;
  firstNameControl: FormControl;
  lastNameControl:FormControl;
  phoneNumberControl: FormControl;
  emailControl: FormControl;
  passwordControl: FormControl;
  profilePictureControl: FormControl;
  countryIdControl:FormControl;
  public imageProfileSrc: string = '';
  loading = false;

  constructor(public usersService: UsersService, public route: ActivatedRoute, public data: DataService, public router: Router, public toastrService: NbToastrService) {
    super(toastrService)
    this.operators = new Operators()
  }

  ngOnInit(): void {

    // create form group and its form controls and
    this.createFormControls();
    this.createForm();
    this.route.params.subscribe(params => {
      if(params['id']){
      this.idControl.setValue(params['id'])
      this.getUserById(this.idControl.value)
      }
    });
    this.getCountry();
  }

  // create form controls that we need to validate with their needed validators
  createFormControls() {
    this.firstNameControl = new FormControl(null, [
      Validators.required,
    ]),
    this.lastNameControl= new FormControl(null, [
      Validators.required,
    ]),
      this.userNameControl = new FormControl(null, [
        Validators.required,
      ]),
      this.passwordControl = new FormControl(null, [
        Validators.required,
        Validators.pattern("[a-zA-Z0-9]{6,255}$")
      ]);
    this.emailControl = new FormControl(null, [
      Validators.required,
      Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")
    ]),
      this.phoneNumberControl = new FormControl(null, [
        Validators.required,
        Validators.pattern("(01)[0-9]{9}")
      ]);
    this.profilePictureControl = new FormControl(null, [
      Validators.required,
    ]);
    this.countryIdControl= new FormControl(null, [
      Validators.required,
    ]);
    this.idControl = new FormControl(null)
  }


  // add form controls to form group
  createForm() {
    this.addForm = new FormGroup({
      firstName: this.firstNameControl,
      lastName:this.lastNameControl,
      userName: this.userNameControl,
      email: this.emailControl,
      phoneNumber: this.phoneNumberControl,
      profilePicture: this.profilePictureControl,
      password: this.passwordControl,
      countryId:this.countryIdControl,
      id: this.idControl
    });
  }
  getCountry(){
    this.loading=true;
    

    this.usersService.getCountry().subscribe(
      data => {
        console.log(data.data)
        this.countries=data.data.list


  //      this.isActiveControl.setValue(data.data.isActive)
        // this.categories.isActive=this.isActiveControl.value

        // this.router.navigateByUrl(`/pages/categories/categorie`);


        console.log(data)
        // console.log(token)

      },
      error => {
        this.loading=false;

        // this.spinnerService.hide();
        try {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.loginerrorMessage);
        } catch (err) {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.errorMessage);
        }
      },
    );
  }
  getUserById(id){
    this.loading=true;

    this.usersService.getUserById(id).subscribe(
      data => {
        console.log(data.data)
        this.passwordControl.setValidators([])
        this.passwordControl.updateValueAndValidity();
        this.firstNameControl.setValue(data.data.firstName)
        this.operators.firstName = this.firstNameControl.value
        this.lastNameControl.setValue(data.data.lastName)
        this.operators.lastName = this.lastNameControl.value;
        this.imageProfileSrc = data?.data?.profilePicture;
        this.profilePictureControl.setValue(data?.data?.profilePicture)
        this.operators.password = this.passwordControl.value
        this.userNameControl.setValue(data.data.userName)
        this.operators.userName = this.userNameControl.value
        this.emailControl.setValue(data.data.email)
        this.operators.email = this.emailControl.value
        this.phoneNumberControl.setValue(data.data.phoneNumber)
        this.operators.phoneNumber = this.phoneNumberControl.value
        this.countryIdControl.setValue(data.data.countryId._id)
        this.operators.countryId=this.countryIdControl.value;

        this.loading=false;

  //      this.isActiveControl.setValue(data.data.isActive)
        // this.categories.isActive=this.isActiveControl.value

        // this.router.navigateByUrl(`/pages/categories/categorie`);


        console.log(data)
        // console.log(token)

      },
      error => {
        this.loading=false;

        // this.spinnerService.hide();
        try {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.loginerrorMessage);
        } catch (err) {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.errorMessage);
        }
      },
    );
  }
  handleInputProfileChange(e) {
    var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
    var pattern = /image-*/;
    var reader = new FileReader();
    if (!file.type.match(pattern)) {
      alert('invalid format');
      return;
    }
    reader.onload = this._handleProfileReaderLoaded.bind(this);
    reader.readAsDataURL(file);
  }
  _handleProfileReaderLoaded(e) {
    let reader = e.target;
    this.imageProfileSrc = reader.result;
    console.log(this.imageProfileSrc)
    this.profilePictureControl.setValue(this.imageProfileSrc)
  }

  save() {
    this.loading = true;
    this.operators.firstName = this.firstNameControl.value
    this.operators.lastName = this.lastNameControl.value
    this.operators.userName = this.userNameControl.value;
    this.operators.email = this.emailControl.value
    this.operators.password = this.passwordControl.value
    this.operators.phoneNumber = this.phoneNumberControl.value
    this.operators.profilePicture = this.profilePictureControl.value
    this.operators.countryId=this.countryIdControl.value;
    if (this.idControl.value) {
      this.idControl.disable();
      this.passwordControl.disable();
      // this.categories.isActive=this.isActiveControl.value
      this.usersService.saveAdmin(this.addForm.value, this.idControl.value).subscribe(
        data => {
          this.router.navigateByUrl(`/pages/users/administrator`);

          // this.status='success'
          // this.title='Successfully Saved .. ';
          // this.content='Please check !!'
          // this.showToast(this.status, this.title, this.content);
          // this.data.changeMessage(this.datasaved)
          this.loading = false;

        },
        error => {
          console.log(error)
          this.loading = false;
          // this.spinnerService.hide();
          try {
            this.status='danger'
            this.title='Please check !!';
            this.content=error.error.message
             this.showToast(this.status, this.title,this.content );
          } catch (err) {
            this.status='danger'
            this.title='Eror !!';
            this.content='Please check !!'
            this.showToast(this.status, this.title, this.content);
            // this.openModal(this.loginDataObject.error, this.loginDataObject.errorMessage);
          }
        },
      );

    } else {
      this.usersService.saveAdmin(this.operators).subscribe(
        data => {
          this.router.navigateByUrl(`/pages/users/administrator`);

          this.status = 'success'
          this.title = 'Successfully Saved .. ';
          this.content = 'Please check !!'
          this.showToast(this.status, this.title, this.content);
          this.data.changeMessage(true)
          this.loading = false;
        },
        error => {
          this.loading = false;


          // this.spinnerService.hide();
          try {
            this.status='danger'
            this.title='Please check !!';
            this.content=error.error.message
             this.showToast(this.status, this.title,this.content );
            // this.openModal(this.loginDataObject.error, this.loginDataObject.loginerrorMessage);
          } catch (err) {
            this.status='danger'
            this.title='Please check !!';
            this.content=err.message;
             this.showToast(this.status, this.title,this.content );
            // this.openModal(this.loginDataObject.error, this.loginDataObject.errorMessage);
          }
        },
      );

    }


  }

}

import { Component, OnInit,TemplateRef } from '@angular/core';
import { LocalDataSource, ServerDataSource } from 'ng2-smart-table';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { SmartTableData } from '../../../@core/data/smart-table';
import { Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import {  UsersService} from '../../../services/users/users.service';
import { environment } from '../../../../environments/environment';
import { NbDialogService } from '@nebular/theme';

import {
  NbComponentStatus,
  NbGlobalLogicalPosition,
  NbGlobalPhysicalPosition,
  NbGlobalPosition,
  NbToastrConfig,
  NbToastrService
} from '@nebular/theme';
@Component({
  selector: 'ngx-administrator',
  templateUrl: './administrator.component.html',
  styleUrls: ['./administrator.component.scss'],
})
export class AdministratorComponent implements OnInit {
  loading = false;
  fubUrl: any;
  rowData:any;

  settings = {
    mode: 'external',

    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      // confirmDelete: true,
    },
    columns: {
      profilePicture: {
        title: 'Profile Picture',
        filter: false,
        type: 'html',
        valuePrepareFunction: (profilePicture) => {
            return this._domSanitizer.bypassSecurityTrustHtml(`<img class="img-box" style="bo" src="${profilePicture}" />`);
        },
    },
      firstName: {
        title: 'Full Name',
        type: 'string',
      },
      lastName: {
        title: 'Last Name',
        type: 'string',
      },
      userName: {
        title: 'User Name',
        type: 'string',
      },
      phoneNumber: {
        title: 'Phone Number',
        type: 'string',
      },
      email: {
        title: 'E-mail',
        type: 'string',
      },

    },
  };

  source: LocalDataSource = new LocalDataSource();

  constructor(private dialogService: NbDialogService,private service: SmartTableData, private httpClient: HttpClient,public usersService:UsersService, private router: Router, private _domSanitizer: DomSanitizer) {
    const data = this.service.getData();
    this.source.load(data);
    this.fubUrl = environment.FUBS_API_URL

  }
  ngOnInit(): void {
    this.getservice();
  }

  getservice() {
    this.loading = true;
    this.source = new ServerDataSource(this.httpClient, {
      endPoint: `${this.fubUrl}/users/admin`,
      pagerPageKey: 'page',
      pagerLimitKey: 'limit',
      filterFieldKey: '#field#',
      totalKey: 'data.count',
      dataKey: 'data.list',
    }
    );
    this.loading = false;

  }
  // getOperators() {
  //   this.loading = true;
  //   this.usersService.getOperators()
  //     .subscribe(
  //       data => {
  //         console.log(data)
  //         this.loading = false;

  //        // this.source = new LocalDataSource(data.data.list)
  //       }, error => {
  //         console.log(error)
  //         this.loading = false;


  //         // this.spinnerService.hide();
  //         try {
  //           // this.openModal(this.loginDataObject.error, this.loginDataObject.loginerrorMessage);
  //         } catch (err) {
  //           // this.openModal(this.loginDataObject.error, this.loginDataObject.errorMessage);
  //         }
  //       },

  //     )

  //   // this.resultData = true;

  // }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      this.onDelete(event)
    } else {
      event.confirm.reject();
    }
  }
  onDelete(event) {

    this.usersService.deleteUser(event.data._id)

        .subscribe(
          data => {
            // this.del.nativeElement.click();
            this.getservice();
            // this.makeToast(this.configurationDataObject.feesDeleteSuccessfullMessage);

          },
          error => {
//            this.del.nativeElement.click();
            try {
              // if (error.error.errorCode)
              // this.openModal(error.error.errorCode, error.error.errorMessage);
              // this.openModal(this.configurationDataObject.error, this.configurationDataObject.errorMessage);
            } catch (err) {
              // this.openModal(this.configurationDataObject.error, this.configurationDataObject.errorMessage);
            }
          },
      );

  }
  
  dialogDelete(dialog: TemplateRef<any>,event) {
    console.log(event)
    this.rowData=event;
    this.dialogService.open(
      dialog,
      {
        context: 'this is some additional data passed to dialog',
        closeOnBackdropClick: false,
      });

  }
  onClickYes(){
    this.onDelete(this.rowData)
  }
  onAdd(){
    this.router.navigateByUrl('/pages/users/add-administrator');

  }
  onEdit(event){
    console.log(event.data._id)
    this.router.navigateByUrl(`/pages/users/add-administrator/${event.data._id}`);
  }
}





import { Component, OnInit,TemplateRef } from '@angular/core';
import { LocalDataSource,ServerDataSource } from 'ng2-smart-table';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LocationsService } from '../../../services/locations/locations.service';
import { NbDialogService } from '@nebular/theme';

import { SmartTableData } from '../../../@core/data/smart-table';
import { DomSanitizer } from '@angular/platform-browser';
import { environment } from '../../../../environments/environment';
import { CountryCurrancyComponent } from './country-currancy/country-currancy.component';
@Component({
  selector: 'ngx-currencies',
  templateUrl:'./currencies.component.html',
  styleUrls: ['./currencies.component.scss']
})
export class CurrenciesComponent implements OnInit {
  source:ServerDataSource
  fubUrl:any;
  rowData:any;
  loading:any;
  settings = {
    mode: 'external',
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {

      name: {
        title: 'Name',
        type: 'string',
        filter:false
      },
      country: {
        title: 'Country',
        type: 'custom',
        renderComponent: CountryCurrancyComponent,
        editor: {
          type: 'custom',
          component: CountryCurrancyComponent
        },
        filter:false
      },
      symbol: {
        title: 'Symbol',
        type: 'string',
        filter:false
      },
      icon: {
        title: 'Icon',
        filter:false,
      type: 'html',
        valuePrepareFunction: (icon:string) => { return `<img class="img-box" src="${icon}" />`; },
        },
    },

      // picture: {
      //   title: 'Picture',
      //   type: 'html',
      //   valuePrepareFunction: (picture:string) => { return `<img width="50px" src="${picture}" />`; },
      //   },
    
  };


  constructor(private dialogService: NbDialogService,private service: SmartTableData, public locationsService:LocationsService,private httpClient: HttpClient, private router: Router, private _domSanitizer: DomSanitizer) {
    this.fubUrl = environment.FUBS_API_URL

  }
  ngOnInit(): void {
    this.getservice()
  }
  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
      this.onDelete(event)
    } else {
      event.confirm.reject();
    }
  }

  getservice() {
    this.loading = true;
    this.source = new ServerDataSource(this.httpClient, {
      endPoint: `${this.fubUrl}/currencies`,
      pagerPageKey: 'page',
      pagerLimitKey: 'limit',
      filterFieldKey: '#field#_like',
      totalKey: 'data.count',
      dataKey: 'data.list',
    }
    );
    this.loading = false;

  }

  dialogDelete(dialog: TemplateRef<any>,event) {
    console.log(event)
    this.rowData=event;
    this.dialogService.open(
      dialog,
      {
        context: 'this is some additional data passed to dialog',
        closeOnBackdropClick: false,
      });

  }
  onClickYes(){
    this.onDelete(this.rowData)
  }

  onAdd(){
    this.router.navigateByUrl('/pages/locations/add-currency');
  }

  onEdit(event){
    console.log(event.data._id)
    this.router.navigateByUrl(`/pages/locations/add-currency/${event.data._id}`);
  }

  onDelete(event) {
    this.locationsService.deleteCurrency(event.data._id)

        .subscribe(
          data => {
            // this.del.nativeElement.click();
            this.getservice();
            // this.makeToast(this.configurationDataObject.feesDeleteSuccessfullMessage);

          },
          error => {
//            this.del.nativeElement.click();
            try {
              // if (error.error.errorCode)
              // this.openModal(error.error.errorCode, error.error.errorMessage);
              // this.openModal(this.configurationDataObject.error, this.configurationDataObject.errorMessage);
            } catch (err) {
              // this.openModal(this.configurationDataObject.error, this.configurationDataObject.errorMessage);
            }
          },
      );

  }

}





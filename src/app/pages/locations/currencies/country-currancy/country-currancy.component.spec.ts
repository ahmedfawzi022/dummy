import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CountryCurrancyComponent } from './country-currancy.component';

describe('CountryCurrancyComponent', () => {
  let component: CountryCurrancyComponent;
  let fixture: ComponentFixture<CountryCurrancyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CountryCurrancyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CountryCurrancyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit,TemplateRef } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { Router, ActivatedRoute } from '@angular/router';
import { ServerDataSource } from 'ng2-smart-table';
import { environment } from '../../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LocationsService } from '../../../services/locations/locations.service';
import { DataService } from '../../../services/data/data.service';
import { NbDialogService } from '@nebular/theme';

import { SmartTableData } from '../../../@core/data/smart-table';
import { DomSanitizer } from '@angular/platform-browser';
import { AnyARecord } from 'dns';
import { SmartTableViewDistrictComponent } from './smart-table-view-district/smart-table-view-district.component';
@Component({
  selector: 'ngx-cities',
  templateUrl:'./cities.component.html',
  styleUrls: ['./cities.component.scss']
})
export class CitiesComponent implements OnInit {
  loading:any;
  rowData:any;
  countryId:any;
  country:any;
  source: ServerDataSource;
  countries=[];
  fubUrl:any;
  message:any;
  settings = {
    mode: 'external',
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
    name: {
        title: 'Name',
        type: 'string',
      },
      view: {
        title: 'View District',
        type: 'custom',
        renderComponent: SmartTableViewDistrictComponent,
        editor: {
          type: 'custom',
          component: SmartTableViewDistrictComponent
        },
        sort: false,
        filter: false,
      },

      
      // picture: {
      //   title: 'Picture',
      //   type: 'html',
      //   valuePrepareFunction: (picture:string) => { return `<img width="50px" src="${picture}" />`; },
      //   },
    },
  };

  constructor(private dialogService: NbDialogService,private service: SmartTableData, public data:DataService,public locationsService: LocationsService, private route: ActivatedRoute, private httpClient: HttpClient, private router: Router, private _domSanitizer: DomSanitizer) {
    // const data = this.service.getData();
    // this.source.load(data);
    this.fubUrl = environment.FUBS_API_URL
  }
  ngOnInit(): void {
    this.data.currentMessage.subscribe(message => {
      this.message = message
    })
    this.route.params.subscribe(params => {
      this.countryId = params['id']
      console.log(this.countryId)
    });
    this.data.currentAction.subscribe(message => {
      this.getservice(this.countryId);
    })
    this.getservice(this.countryId);
    this.getCountry()
  }
  getservice(id) {
    this.loading = true;
    this.source = new ServerDataSource(this.httpClient, {
      endPoint: `${this.fubUrl}/countries/${id}/cities`,
      pagerPageKey: 'page',
      pagerLimitKey: 'limit',
      filterFieldKey: '#field#_like',
      totalKey: 'data.count',
      dataKey: 'data.list',
    }
    );
    this.loading = false;

  }
  // getCities() {
  //   this.loading = true;
  //   this.locationsService.getCities(this.countryId)
  //     .subscribe(
  //       data => {
  //         console.log(data)
  //         this.loading = false;

  //         this.source = new LocalDataSource(data.data.list)
  //       }, error => {
  //         console.log(error)
  //         this.loading = false;


  //         // this.spinnerService.hide();
  //         try {
  //           // this.openModal(this.loginDataObject.error, this.loginDataObject.loginerrorMessage);
  //         } catch (err) {
  //           // this.openModal(this.loginDataObject.error, this.loginDataObject.errorMessage);
  //         }
  //       },

  //     )

  //   // this.resultData = true;

  // }
  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }
  onDelete(event) {
    this.locationsService.deleteCity(event.data._id)

        .subscribe(
          data => {
            // this.del.nativeElement.click();
            this.getservice(this.countryId);
            // this.makeToast(this.configurationDataObject.feesDeleteSuccessfullMessage);

          },
          error => {
//            this.del.nativeElement.click();
            try {
              // if (error.error.errorCode)
              // this.openModal(error.error.errorCode, error.error.errorMessage);
              // this.openModal(this.configurationDataObject.error, this.configurationDataObject.errorMessage);
            } catch (err) {
              // this.openModal(this.configurationDataObject.error, this.configurationDataObject.errorMessage);
            }
          },
      );

  }

  onAdd(){
    this.router.navigateByUrl(`/pages/locations/${this.countryId}/add-city`);

  }

  onEdit(event){
    console.log(event)
    this.router.navigateByUrl(`/pages/locations/${event.data.countryId._id}/add-city/${event.data._id}`);

  }
  getCountry(){
    this.loading=true;

    this.locationsService.getCountry().subscribe(
      data => {
        console.log(data.data)
        this.countries=data.data.list


  //      this.isActiveControl.setValue(data.data.isActive)
        // this.categories.isActive=this.isActiveControl.value

        // this.router.navigateByUrl(`/pages/categories/categorie`);


        console.log(data)
        // console.log(token)

      },
      error => {
        this.loading=false;

        // this.spinnerService.hide();
        try {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.loginerrorMessage);
        } catch (err) {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.errorMessage);
        }
      },
    );
  }

  onChangeCountry(event){
    this.countryId=event;
    this.getservice(event)
  }
  dialogDelete(dialog: TemplateRef<any>,event) {
    console.log(event)
    this.rowData=event;
    this.dialogService.open(
      dialog,
      {
        context: 'this is some additional data passed to dialog',
        closeOnBackdropClick: false,
      });

  }
  onClickYes(){
    this.onDelete(this.rowData)
  }
}





import { Component, OnInit,Input } from '@angular/core';
import { Router } from '@angular/router';
import { DefaultEditor, ViewCell } from 'ng2-smart-table';

@Component({
  selector: 'ngx-smart-table-view-district',
  templateUrl: './smart-table-view-district.component.html',
  styleUrls: ['./smart-table-view-district.component.scss']
})
export class SmartTableViewDistrictComponent extends DefaultEditor implements OnInit, ViewCell {

  renderValue: string;
  @Input() value: string | number;
  @Input() rowData: any;
  constructor(public router:Router) {    super();
  }

  ngOnInit(): void {
    console.log(this.rowData)

  }

  view(){
    this.router.navigateByUrl(`/pages/locations/${this.rowData._id}/districts`);

  }

}


import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmartTableViewDistrictComponent } from './smart-table-view-district.component';

describe('SmartTableViewDistrictComponent', () => {
  let component: SmartTableViewDistrictComponent;
  let fixture: ComponentFixture<SmartTableViewDistrictComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmartTableViewDistrictComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmartTableViewDistrictComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

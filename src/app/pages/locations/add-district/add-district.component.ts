import { Component, OnInit } from '@angular/core';
import { LocationsService } from '../../../services/locations/locations.service';
import { environment } from '../../../../environments/environment';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Operators } from '../../../models/users/operators'
import { Router, ActivatedRoute } from '@angular/router';
import { DataService } from '../../../services/data/data.service';
import { BaseComponent } from '../../general/base/base.component';
// import { ToastrService } from 'ngx-toastr';
import {
  NbComponentStatus,
  NbGlobalLogicalPosition,
  NbGlobalPhysicalPosition,
  NbGlobalPosition,
  NbToastrService,
  NbToastrConfig,
} from '@nebular/theme';

@Component({
  selector: 'ngx-add-district',
  templateUrl: './add-district.component.html',
  styleUrls: ['./add-district.component.scss']
})
export class AddDistrictComponent extends BaseComponent implements OnInit {
  operators: Operators
  countries=[]
  countryId:any;
  cityId:any;

  //Define Form Group with its controls
  addForm: FormGroup;
  idControl: FormControl;
  nameControl: FormControl;
  cityIdControl:FormControl

  public imageIconSrc: string = '';
  loading = false;

  constructor(public locationsService: LocationsService, public route: ActivatedRoute, public data: DataService, public router: Router, public toastrService: NbToastrService) {
    super(toastrService)
    this.operators = new Operators()
  }

  ngOnInit(): void {
    // create form group and its form controls and
    this.createFormControls();
    this.createForm();
    this.route.params.subscribe(params => {
      this.cityIdControl.setValue(params['cityId'])
    });
    this.route.params.subscribe(params => {
      this.idControl.setValue(params['id'])
    });
    if(this.idControl.value)
    this.getDistrictById(this.idControl.value)
    // this.route.params.subscribe(params => {
    //   if(params['id']){
    //   this.idControl.setValue(params['id'])
    //   this.getCountryById(this.idControl.value)
    //   }
    // });
  }

  // create form controls that we need to validate with their needed validators
  createFormControls() {
    this.nameControl = new FormControl(null, [
      Validators.required,
    ]),
    this.cityIdControl = new FormControl(null, [
      Validators.required,
    ]),

    this.idControl = new FormControl(null)
  }


  // add form controls to form group
  createForm() {
    this.addForm = new FormGroup({
      name: this.nameControl,
      cityId:this.cityIdControl,
      id: this.idControl
    });
  }

  getDistrictById(id){
    this.loading=true;

    this.locationsService.getDistrcitById(id).subscribe(
      data => {
        console.log(data.data)
      
        this.nameControl.setValue(data.data.name)
        this.loading=false;

  //      this.isActiveControl.setValue(data.data.isActive)
        // this.categories.isActive=this.isActiveControl.value

        // this.router.navigateByUrl(`/pages/categories/categorie`);


        console.log(data)
        // console.log(token)

      },
      error => {
        this.loading=false;

        // this.spinnerService.hide();
        try {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.loginerrorMessage);
        } catch (err) {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.errorMessage);
        }
      },
    );
  }

  getCountry(){
    this.loading=true;

    this.locationsService.getCountry().subscribe(
      data => {
        console.log(data.data)
        this.countries=data.data.list


  //      this.isActiveControl.setValue(data.data.isActive)
        // this.categories.isActive=this.isActiveControl.value

        // this.router.navigateByUrl(`/pages/categories/categorie`);


        console.log(data)
        // console.log(token)

      },
      error => {
        this.loading=false;

        // this.spinnerService.hide();
        try {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.loginerrorMessage);
        } catch (err) {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.errorMessage);
        }
      },
    );
  }
  back(){
    this.router.navigateByUrl(`/pages/locations/${this.cityIdControl.value}/districts`);

  }
  save() {

    this.loading = true;
   
    if (this.idControl.value) {
      this.idControl.disable();
      // this.categories.isActive=this.isActiveControl.value
      this.locationsService.updateDistrict(this.addForm.value, this.idControl.value).subscribe(
        data => {
          this.router.navigateByUrl(`/pages/locations/${this.cityIdControl.value}/districts`);

          // this.status='danger'
          // this.title='Successfully Saved .. ';
          // this.content='Please check !!'
          // this.showToast(this.status, this.title, this.content);
          // this.data.changeMessage(this.datasaved)
          this.loading = false;

        },
        error => {
          console.log(error)
          this.loading = false;
          // this.spinnerService.hide();
          try {
            this.status='danger'
            this.title= 'Please check !!';
            this.content=error.error.message
            this.showToast(this.status, this.title, this.content);
          } catch (err) {
            this.status='danger'
            this.title='Please check !!';
            this.content=err.message;
             this.showToast(this.status, this.title,this.content );
            // this.openModal(this.loginDataObject.error, this.loginDataObject.errorMessage);
          }
        },
      );

    } else {
      this.idControl.disable();
      this.cityIdControl.disable()

      this.locationsService.saveDistrict(this.addForm.value,this.cityIdControl.value).subscribe(
        data => {
          this.router.navigateByUrl(`/pages/locations/${this.cityIdControl.value}/districts`);

          this.status = 'success'
          this.title = 'Successfully Saved .. ';
          this.content = 'Please check !!'
          this.showToast(this.status, this.title, this.content);
          this.data.changeMessage(true)
          this.loading = false;
        },
        error => {
          this.loading = false;

          console.log(error)
          // this.spinnerService.hide();
          try {
            this.status='danger'
            this.title='Please check !!';
            this.content=error.error.message
             this.showToast(this.status, this.title,this.content );
          } catch (err) {
            this.status='danger'
            this.title='Please check !!';
            this.content=err.message;
             this.showToast(this.status, this.title,this.content );
            // this.openModal(this.loginDataObject.error, this.loginDataObject.errorMessage);
          }
        },
      );

    }


  }

}

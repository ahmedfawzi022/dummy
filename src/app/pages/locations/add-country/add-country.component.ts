import { Component, OnInit } from '@angular/core';
import { LocationsService } from '../../../services/locations/locations.service';
import { environment } from '../../../../environments/environment';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Operators } from '../../../models/users/operators'
import { Router, ActivatedRoute } from '@angular/router';
import { DataService } from '../../../services/data/data.service';
import { BaseComponent } from '../../general/base/base.component';
// import { ToastrService } from 'ngx-toastr';
import {
  NbComponentStatus,
  NbGlobalLogicalPosition,
  NbGlobalPhysicalPosition,
  NbGlobalPosition,
  NbToastrService,
  NbToastrConfig,
} from '@nebular/theme';

@Component({
  selector: 'ngx-add-country',
  templateUrl: './add-country.component.html',
  styleUrls: ['./add-country.component.scss']
})
export class AddCountryComponent extends BaseComponent implements OnInit {
  operators: Operators

  //Define Form Group with its controls
  addForm: FormGroup;
  idControl: FormControl;
  nameControl: FormControl;
  codeControl: FormControl;
  iconControl: FormControl;
  public imageIconSrc: string = '';
  loading = false;

  constructor(public locationsService: LocationsService, public route: ActivatedRoute, public data: DataService, public router: Router, public toastrService: NbToastrService) {
    super(toastrService)
    this.operators = new Operators()
  }

  ngOnInit(): void {
    // create form group and its form controls and
    this.createFormControls();
    this.createForm();
    this.route.params.subscribe(params => {
      if(params['id']){
      this.idControl.setValue(params['id'])
      this.getCountryById(this.idControl.value)
      }
    });
  }

  // create form controls that we need to validate with their needed validators
  createFormControls() {
    this.nameControl = new FormControl(null, [
      Validators.required,
    ]),
    this.codeControl = new FormControl(null, [
      Validators.required,
    ]),

    this.iconControl = new FormControl('', [
     Validators.required,
    ]);
    this.idControl = new FormControl(null)
  }


  // add form controls to form group
  createForm() {
    this.addForm = new FormGroup({
      name: this.nameControl,
      code:this.codeControl,
      icon: this.iconControl,
      id: this.idControl
    });
  }

  getCountryById(id){
    this.loading=true;

    this.locationsService.getCountryById(id).subscribe(
      data => {
        console.log(data.data)
      
        this.nameControl.setValue(data.data.name)
        this.codeControl.setValue(data.data.code)
        this.imageIconSrc = data?.data?.icon
        this.iconControl.setValue(data?.data?.icon)
        this.loading=false;

  //      this.isActiveControl.setValue(data.data.isActive)
        // this.categories.isActive=this.isActiveControl.value

        // this.router.navigateByUrl(`/pages/categories/categorie`);


        console.log(data)
        // console.log(token)

      },
      error => {
        this.loading=false;

        // this.spinnerService.hide();
        try {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.loginerrorMessage);
        } catch (err) {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.errorMessage);
        }
      },
    );
  }
  handleInputIconChange(e) {
    var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
    var pattern = /image-*/;
    var reader = new FileReader();
    if (!file.type.match(pattern)) {
      alert('invalid format');
      return;
    }
    reader.onload = this._handleIconReaderLoaded.bind(this);
    reader.readAsDataURL(file);
  }
  _handleIconReaderLoaded(e) {
    let reader = e.target;
    this.imageIconSrc = reader.result;
    console.log(this.imageIconSrc)
    this.iconControl.setValue(this.imageIconSrc)
  }

  save() {

    this.loading = true;
   
    if (this.idControl.value) {
      this.idControl.disable();
      // this.categories.isActive=this.isActiveControl.value
      this.locationsService.saveCountry(this.addForm.value, this.idControl.value).subscribe(
        data => {
          this.router.navigateByUrl(`/pages/locations/countries`);

          // this.status='danger'
          // this.title='Successfully Saved .. ';
          // this.content='Please check !!'
          // this.showToast(this.status, this.title, this.content);
          // this.data.changeMessage(this.datasaved)
          this.loading = false;

        },
        error => {
          console.log(error)
          this.loading = false;
          // this.spinnerService.hide();
          try {
            this.status='danger'
            this.title= 'Please check !!';
            this.content=error.error.message
            this.showToast(this.status, this.title, this.content);
          } catch (err) {
            this.status='danger'
            this.title='Please check !!';
            this.content=err.message;
             this.showToast(this.status, this.title,this.content );
            // this.openModal(this.loginDataObject.error, this.loginDataObject.errorMessage);
          }
        },
      );

    } else {
      this.idControl.disable();

      this.locationsService.saveCountry(this.addForm.value).subscribe(
        data => {
          this.router.navigateByUrl(`/pages/locations/countries`);

          this.status = 'success'
          this.title = 'Successfully Saved .. ';
          this.content = 'Please check !!'
          this.showToast(this.status, this.title, this.content);
          this.data.changeMessage(true)
          this.loading = false;
        },
        error => {
          this.loading = false;

          console.log(error)
          // this.spinnerService.hide();
          try {
            this.status='danger'
            this.title='Please check !!';
            this.content=error.error.message
             this.showToast(this.status, this.title,this.content );
          } catch (err) {
            this.status='danger'
            this.title='Please check !!';
            this.content=err.message;
             this.showToast(this.status, this.title,this.content );
            // this.openModal(this.loginDataObject.error, this.loginDataObject.errorMessage);
          }
        },
      );

    }


  }

}

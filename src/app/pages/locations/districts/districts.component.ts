import { Component, OnInit,TemplateRef } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { Router, ActivatedRoute } from '@angular/router';
import { ServerDataSource } from 'ng2-smart-table';
import { environment } from '../../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LocationsService } from '../../../services/locations/locations.service';
import { DataService } from '../../../services/data/data.service';
import { NbDialogService } from '@nebular/theme';

import { SmartTableData } from '../../../@core/data/smart-table';
import { DomSanitizer } from '@angular/platform-browser';
import { AnyARecord } from 'dns';
@Component({
  selector: 'ngx-districts',
  templateUrl: './districts.component.html',
  styleUrls: ['./districts.component.scss']
})
export class DistrictsComponent implements OnInit {
  loading:any;
  cityId:any;
  source: ServerDataSource;
  country:any;
  city:any;
  fubUrl:any;
  countries=[];
  cities=[];
  message:any;
  rowData:any;
  settings = {
    mode: 'external',
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {

      name: {
        title: 'Name',
        type: 'string',
      }
     
      // picture: {
      //   title: 'Picture',
      //   type: 'html',
      //   valuePrepareFunction: (picture:string) => { return `<img width="50px" src="${picture}" />`; },
      //   },
    },
  };


  constructor(private dialogService: NbDialogService,private service: SmartTableData, public data:DataService,public locationsService: LocationsService, private route: ActivatedRoute, private httpClient: HttpClient, private router: Router, private _domSanitizer: DomSanitizer) {
    // const data = this.service.getData();
    // this.source.load(data);
    this.fubUrl = environment.FUBS_API_URL
  }
  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.cityId = params['id']
    });
    this.data.currentMessage.subscribe(message => {
      this.message = message
      this.getservice( this.cityId)

    })
    this.getCountry()
  }
  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }

  onAdd(){
    this.router.navigateByUrl(`/pages/locations/${this.cityId}/add-district`);

  }

  onEdit(event){
    console.log(event)
    this.router.navigateByUrl(`/pages/locations/${this.cityId}/add-district/${event.data._id}`);

  }

  getservice(id) {
    this.loading = true;
    this.source = new ServerDataSource(this.httpClient, {
      endPoint: `${this.fubUrl}/countries/${id}/districts`,
      pagerPageKey: 'page',
      pagerLimitKey: 'limit',
      filterFieldKey: '#field#_like',
      totalKey: 'data.count',
      dataKey: 'data.list',
    }
    );
    this.loading = false;

  }

  getCountry(){
    this.loading=true;

    this.locationsService.getCountry().subscribe(
      data => {
        console.log(data.data)
        this.countries=data.data.list


  //      this.isActiveControl.setValue(data.data.isActive)
        // this.categories.isActive=this.isActiveControl.value

        // this.router.navigateByUrl(`/pages/categories/categorie`);


        console.log(data)
        // console.log(token)

      },
      error => {
        this.loading=false;

        // this.spinnerService.hide();
        try {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.loginerrorMessage);
        } catch (err) {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.errorMessage);
        }
      },
    );
  }

  onChangeCountry(event){
    this.getCities(event)
  }
  getCities(id){
    this.loading=true;

    this.locationsService.getCities(id).subscribe(
      data => {
        console.log(data.data)
        this.cities=data.data.list


  //      this.isActiveControl.setValue(data.data.isActive)
        // this.categories.isActive=this.isActiveControl.value

        // this.router.navigateByUrl(`/pages/categories/categorie`);


        console.log(data)
        // console.log(token)

      },
      error => {
        this.loading=false;

        // this.spinnerService.hide();
        try {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.loginerrorMessage);
        } catch (err) {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.errorMessage);
        }
      },
    );
  }

  onChangeCity(id){
    this.cityId=id
    this. getservice(id)
  }
  onDelete(event) {
    this.locationsService.deleteDistrict(event.data._id)

        .subscribe(
          data => {
            // this.del.nativeElement.click();
            this.getservice( this.cityId)
            // this.makeToast(this.configurationDataObject.feesDeleteSuccessfullMessage);

          },
          error => {
//            this.del.nativeElement.click();
            try {
              // if (error.error.errorCode)
              // this.openModal(error.error.errorCode, error.error.errorMessage);
              // this.openModal(this.configurationDataObject.error, this.configurationDataObject.errorMessage);
            } catch (err) {
              // this.openModal(this.configurationDataObject.error, this.configurationDataObject.errorMessage);
            }
          },
      );

  }
  dialogDelete(dialog: TemplateRef<any>,event) {
    console.log(event)
    this.rowData=event;
    this.dialogService.open(
      dialog,
      {
        context: 'this is some additional data passed to dialog',
        closeOnBackdropClick: false,
      });

  }
  onClickYes(){
    this.onDelete(this.rowData)
  }
}





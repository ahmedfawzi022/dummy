import { Component, OnInit,TemplateRef } from '@angular/core';
import { LocalDataSource, ServerDataSource } from 'ng2-smart-table';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NbDialogService } from '@nebular/theme';

import { SmartTableData } from '../../../@core/data/smart-table';
import { Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { LocationsService } from '../../../services/locations/locations.service';
import { environment } from '../../../../environments/environment';
import { SmartTableViewCityComponent } from './smart-table-view-city/smart-table-view-city.component';
@Component({
  selector: 'ngx-countries',
  templateUrl: './countries.component.html',
  styleUrls: ['./countries.component.scss']
})
export class CountriesComponent implements OnInit {
  fubUrl:any;
  loading:any;
  rowData:any;
  settings = {
     mode: 'external',
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
    },
    columns: {
      icon: {
        title: 'Icon',
        filter:false,
      type: 'html',
        valuePrepareFunction: (icon:string) => { return `<img class="img-box" src="${icon}" />`; },
        },
      name: {
        title: 'Name',
        type: 'string',
      },
      code: {
        title: 'Code',
        type: 'string',
      },
      view: {
        title: 'View Cities',
        type: 'custom',
        renderComponent: SmartTableViewCityComponent,
        editor: {
          type: 'custom',
          component: SmartTableViewCityComponent
        },
        sort: false,
        filter: false,
      },

    },
  };

  source: LocalDataSource = new LocalDataSource();

  constructor(private dialogService: NbDialogService,private service: SmartTableData, private httpClient: HttpClient,public locationsService:LocationsService, private router: Router, private _domSanitizer: DomSanitizer) {
    const data = this.service.getData();
    this.source.load(data);
    this.fubUrl = environment.FUBS_API_URL

  }
  ngOnInit(): void {
    this.getservice()
  }

  getservice() {
    this.loading = true;
    this.source = new ServerDataSource(this.httpClient, {
      endPoint: `${this.fubUrl}/countries`,
      pagerPageKey: 'page',
      pagerLimitKey: 'limit',
      filterFieldKey: '#field#_like',
      totalKey: 'data.count',
      dataKey: 'data.list',
    }
    );
    this.loading = false;

  }
  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
      this.onDelete(event)
    } else {
      event.confirm.reject();
    }
  }
  onAdd(){
    this.router.navigateByUrl('/pages/locations/add-country');

  }
  onEdit(event){
    console.log(event.data._id)
    this.router.navigateByUrl(`/pages/locations/add-country/${event.data._id}`);
  }

  onDelete(event) {
    this.locationsService.deleteCountry(event.data._id)

        .subscribe(
          data => {
            // this.del.nativeElement.click();
            this.getservice();
            // this.makeToast(this.configurationDataObject.feesDeleteSuccessfullMessage);

          },
          error => {
//            this.del.nativeElement.click();
            try {
              // if (error.error.errorCode)
              // this.openModal(error.error.errorCode, error.error.errorMessage);
              // this.openModal(this.configurationDataObject.error, this.configurationDataObject.errorMessage);
            } catch (err) {
              // this.openModal(this.configurationDataObject.error, this.configurationDataObject.errorMessage);
            }
          },
      );

  }
  dialogDelete(dialog: TemplateRef<any>,event) {
    console.log(event)
    this.rowData=event;
    this.dialogService.open(
      dialog,
      {
        context: 'this is some additional data passed to dialog',
        closeOnBackdropClick: false,
      });

  }
  onClickYes(){
    this.onDelete(this.rowData)
  }
}





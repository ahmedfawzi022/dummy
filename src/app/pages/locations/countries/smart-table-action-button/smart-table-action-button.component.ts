import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-smart-table-action-button',
  templateUrl: './smart-table-action-button.component.html',
  styleUrls: ['./smart-table-action-button.component.scss']
})
export class SmartTableActionButtonComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

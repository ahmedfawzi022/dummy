import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmartTableActionButtonComponent } from './smart-table-action-button.component';

describe('SmartTableActionButtonComponent', () => {
  let component: SmartTableActionButtonComponent;
  let fixture: ComponentFixture<SmartTableActionButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmartTableActionButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmartTableActionButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

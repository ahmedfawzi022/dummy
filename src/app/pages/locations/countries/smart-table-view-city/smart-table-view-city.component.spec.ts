import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmartTableViewCityComponent } from './smart-table-view-city.component';

describe('SmartTableViewCityComponent', () => {
  let component: SmartTableViewCityComponent;
  let fixture: ComponentFixture<SmartTableViewCityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmartTableViewCityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmartTableViewCityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

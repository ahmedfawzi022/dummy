import { Component, OnInit,Input } from '@angular/core';
import { Router } from '@angular/router';
import { DefaultEditor, ViewCell } from 'ng2-smart-table';

@Component({
  selector: 'ngx-smart-table-view-city',
  templateUrl:'./smart-table-view-city.component.html',
  styleUrls: ['./smart-table-view-city.component.scss']
})
export class SmartTableViewCityComponent extends DefaultEditor implements OnInit, ViewCell {

  renderValue: string;
  @Input() value: string | number;
  @Input() rowData: any;
  constructor(public router:Router) {    super();
  }

  ngOnInit(): void {
    console.log(this.rowData)

  }

  viewCities(){
    this.router.navigateByUrl(`/pages/locations/${this.rowData._id}/cities`);

  }

}

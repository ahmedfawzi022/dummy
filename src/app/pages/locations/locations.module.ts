import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbMenuModule } from '@nebular/theme';
import { ThemeModule } from '../../@theme/theme.module';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LocationsRoutingModule } from './locations-routing.module';
import { CurrenciesComponent } from './currencies/currencies.component';
import { AddCurrencyComponent } from './add-currency/add-currency.component';
import { CountriesComponent } from './countries/countries.component';
import { AddCountryComponent } from './add-country/add-country.component';
import { CitiesComponent } from './cities/cities.component';
import { AddCityComponent } from './add-city/add-city.component';
import { DistrictsComponent } from './districts/districts.component';
import { AddDistrictComponent } from './add-district/add-district.component';
import { LocationsComponent } from './locations.component';
import { NgSelectModule } from '@ng-select/ng-select';


import {
  NbAccordionModule,
  NbButtonModule,
  NbSpinnerModule,
  NbListModule,
  NbRouteTabsetModule,
  NbStepperModule,
  NbTabsetModule, NbUserModule,
  NbCardModule, NbIconModule, NbInputModule, NbTreeGridModule,
  NbActionsModule,
  NbCheckboxModule,
  NbDatepickerModule,
  NbRadioModule,
  NbSelectModule,
} from '@nebular/theme';
import { SmartTableActionButtonComponent } from './countries/smart-table-action-button/smart-table-action-button.component';
import { SmartTableViewCityComponent } from './countries/smart-table-view-city/smart-table-view-city.component';
import { SmartTableViewDistrictComponent } from './cities/smart-table-view-district/smart-table-view-district.component';
import { CountryCurrancyComponent } from './currencies/country-currancy/country-currancy.component';

@NgModule({
  declarations: [LocationsComponent,CurrenciesComponent, AddCurrencyComponent, CountriesComponent, AddCountryComponent, CitiesComponent, AddCityComponent, DistrictsComponent, AddDistrictComponent, SmartTableActionButtonComponent, SmartTableViewCityComponent, SmartTableViewDistrictComponent, CountryCurrancyComponent],
  imports: [
    CommonModule,
    LocationsRoutingModule,
    NbMenuModule,
    NbCardModule,
    NbTreeGridModule,
    NbIconModule,
    NbInputModule,
    NbSpinnerModule,
    ThemeModule,
    Ng2SmartTableModule,
    FormsModule,
    ReactiveFormsModule,
    NbTabsetModule,
    NbRouteTabsetModule,
    NbStepperModule,
    NbCardModule,
    NbButtonModule,
    NbListModule,
    NbAccordionModule,
    NbUserModule,
    NbActionsModule,
    NbCheckboxModule,
    NbDatepickerModule,
    NbRadioModule,
    NbSelectModule,
    NgSelectModule
  ]
})
export class LocationsModule { }

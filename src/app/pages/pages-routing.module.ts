import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ECommerceComponent } from './e-commerce/e-commerce.component';
import { CategoriesComponent } from './categories/categories.component';
import { FubsDashboardComponent } from './fubs-dashboard/fubs-dashboard.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [

        {
      path: 'dashboard',
      component: ECommerceComponent,
    },
    {
      path: 'users',
      loadChildren: () => import('./users/users.module')
        .then(m => m.UsersModule),
    },
    {
      path: 'categories',
      loadChildren: () => import('./categories/categories.module')
        .then(m => m.CategoriesModule),
    },
    {
      path: 'notifications',
      loadChildren: () => import('./notifications/notifications.module')
        .then(m => m.NotificationsModule),
    },
    {
      path: 'operations',
      loadChildren: () => import('./operations/operations.module')
        .then(m => m.OperationsModule),
    },
    {
      path: 'settings',
      loadChildren: () => import('./settings/settings.module')
        .then(m => m.SettingsModule),
    },
    {
      path: 'locations',
      loadChildren: () => import('./locations/locations.module')
        .then(m => m.LocationsModule),
    },
    {
      path: 'orders',
      loadChildren: () => import('./orders/orders.module')
        .then(m => m.OrdersModule),
    },
    {
      path: 'fubsdashboard',
      loadChildren: () => import('./fubs-dashboard/fubs-dashboard.module')
        .then(m => m.FubsDashboardModule),
    },
    {
      path: '',
      redirectTo: 'dashboard',
      pathMatch: 'full',
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}

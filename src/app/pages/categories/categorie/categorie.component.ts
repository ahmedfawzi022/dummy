import { Component, OnInit, SimpleChanges,TemplateRef } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { Router } from '@angular/router';
import { ServerDataSource } from 'ng2-smart-table';
import { environment } from '../../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CategoriesService } from '../../../services/categories/categories.service';
import { NbDialogService } from '@nebular/theme';

import { SmartTableData } from '../../../@core/data/smart-table';
import { DomSanitizer } from '@angular/platform-browser';
import { SmartTableViewCategoryComponent } from './../categorie/smart-table-view-category/smart-table-view-category.component';
import { DataService } from '../../../services/data/data.service';
import { SmartTableActiveButtomComponent } from './smart-table-active-buttom/smart-table-active-buttom.component';

@Component({
  selector: 'ngx-categorie',
  templateUrl: './categorie.component.html',
  styleUrls: ['./categorie.component.scss'],
})
export class CategorieComponent implements OnInit {
  source: ServerDataSource;
  message:any;
  loading = false;
  fubUrl: any;
  rowData:any;
  settings = {
    mode: 'external',
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    custom: [
      {
        name: 'save',
        title: '<i class="fa fa-eye smarttable-eye"></i>'
      }
    ],
    position: 'right', // left|right

    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    pager: {
      display: true,
      perPage: 10
    },
    columns: {
      image: {
        title: 'Featured Image',
        filter: false,
        type: 'html',
        valuePrepareFunction: (picture: string) => { return `<div class="image-box"><img class="img-box"  src="${picture}" /></div>`; },

      },
      icon: {
        title: 'Icon',
        filter: false,
        type: 'html',
        valuePrepareFunction: (picture: string) => { return `<img class="img-box" src="${picture}" />`; },

      },
      name: {
        title: 'Name',
        type: 'string',
      },
      // description: {
      //   title: 'Description',
      //   type: 'string',
      //   filter: false
      // },
      view: {
        title: 'View Sub Category',
        type: 'custom',
        renderComponent: SmartTableViewCategoryComponent,
        editor: {
          type: 'custom',
          component: SmartTableViewCategoryComponent
        },
        sort: false,
        filter: false,
      },
      active: {
        title: 'Action Status',
        type: 'custom',
        renderComponent: SmartTableActiveButtomComponent,
        editor: {
          type: 'custom',
          component: SmartTableActiveButtomComponent
        },
        sort: false,
        filter: false,
      },

    },
  };

  // source: LocalDataSource = new LocalDataSource();

  constructor(private dialogService: NbDialogService,private service: SmartTableData, public data:DataService,public categoriesService: CategoriesService, private httpClient: HttpClient, private router: Router, private _domSanitizer: DomSanitizer) {
    // this.source.load(data);
    this.fubUrl = environment.FUBS_API_URL
  }
  ngOnInit(): void {
    this.data.currentMessage.subscribe(message => {
      this.message = message
    })
    this.data.currentAction.subscribe(message => {
      this.getservice();
    })
    // this.getCategory();
    this.getservice();
  }
  ngOnChanges(changes: SimpleChanges) {
    console.log(changes)
    if (!changes.isLoginBackground.firstChange) {

    }
  }
  getCategory() {
    this.loading = true;
    this.categoriesService.getCategoriesTable()
      .subscribe(
        data => {
          console.log(data)
          this.loading = false;

         // this.source = new LocalDataSource(data.data.list)
        }, error => {
          console.log(error)
          this.loading = false;


          // this.spinnerService.hide();
          try {
            // this.openModal(this.loginDataObject.error, this.loginDataObject.loginerrorMessage);
          } catch (err) {
            // this.openModal(this.loginDataObject.error, this.loginDataObject.errorMessage);
          }
        },

      )

    // this.resultData = true;

  }

  getservice() {
    this.loading = true;
    this.source = new ServerDataSource(this.httpClient, {
      endPoint: `${this.fubUrl}/categories`,
      pagerPageKey: 'page',
      pagerLimitKey: 'limit',
      filterFieldKey: '#field#_like',
      totalKey: 'data.count',
      dataKey: 'data.list',
    }
    );
    this.loading = false;

  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }
  dialogDelete(dialog: TemplateRef<any>,event) {
    console.log(event)
    this.rowData=event;
    this.dialogService.open(
      dialog,
      {
        context: 'this is some additional data passed to dialog',
        closeOnBackdropClick: false,
      });

  }
  onClickYes(){
    this.onDelete(this.rowData)
  }
  onAdd() {
    this.router.navigateByUrl('/pages/categories/add-categorie');

  }
  onDelete(event) {
    this.categoriesService.deleteCategory(event.data._id)

        .subscribe(
          data => {
            // this.del.nativeElement.click();
            this.getservice();
            // this.makeToast(this.configurationDataObject.feesDeleteSuccessfullMessage);

          },
          error => {
//            this.del.nativeElement.click();
            try {
              // if (error.error.errorCode)
              // this.openModal(error.error.errorCode, error.error.errorMessage);
              // this.openModal(this.configurationDataObject.error, this.configurationDataObject.errorMessage);
            } catch (err) {
              // this.openModal(this.configurationDataObject.error, this.configurationDataObject.errorMessage);
            }
          },
      );

  }
  onEdit(event) {
    console.log(event.data._id)
    this.router.navigateByUrl(`/pages/categories/add-categorie/${event.data._id}`);

  }
}





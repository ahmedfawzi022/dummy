import { Component, OnInit,Input } from '@angular/core';
import { Router } from '@angular/router';
import { DefaultEditor, ViewCell } from 'ng2-smart-table';
import { CategoriesService } from '../../../../services/categories/categories.service';
import { DataService } from '../../../../services/data/data.service';

@Component({
  selector: 'ngx-smart-table-active-buttom',
  templateUrl: './smart-table-active-buttom.component.html',
  styleUrls: ['./smart-table-active-buttom.component.scss']
})
export class SmartTableActiveButtomComponent extends DefaultEditor implements OnInit, ViewCell {
  loading:boolean=false;
  renderValue: string;
  active:boolean=false;
  @Input() value: string | number;
  @Input() rowData: any;
  constructor(public router:Router,public categoriesService:CategoriesService,public data:DataService) {    super();
  }

  ngOnInit(): void {
    console.log(this.rowData)
    if(this.rowData.isActive == true)
    this.active=true
    else
    this.active=false;

  }

  activeCategory(){
    this.loading = true;
    let active={
      "isActive":true
    }
    this.categoriesService.activeCategory(active,this.rowData._id)
      .subscribe(
        data => {
          console.log(data)
          this.loading = false;
          this.data.changeAction(true)

         // this.source = new LocalDataSource(data.data.list)
        }, error => {
          console.log(error)
          this.loading = false;


          // this.spinnerService.hide();
          try {
            // this.openModal(this.loginDataObject.error, this.loginDataObject.loginerrorMessage);
          } catch (err) {
            // this.openModal(this.loginDataObject.error, this.loginDataObject.errorMessage);
          }
        },

      )


  }
  deactiveCategory(){
    this.loading = true;
    let active={
      "isActive":false
    }
    this.categoriesService.activeCategory(active,this.rowData._id)
      .subscribe(
        data => {
          console.log(data)
          this.loading = false;
          this.data.changeAction(true)

         // this.source = new LocalDataSource(data.data.list)
        }, error => {
          console.log(error)
          this.loading = false;


          // this.spinnerService.hide();
          try {
            // this.openModal(this.loginDataObject.error, this.loginDataObject.loginerrorMessage);
          } catch (err) {
            // this.openModal(this.loginDataObject.error, this.loginDataObject.errorMessage);
          }
        },

      )


  }

}

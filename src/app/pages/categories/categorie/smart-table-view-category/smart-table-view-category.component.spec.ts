import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmartTableViewCategoryComponent } from './smart-table-view-category.component';

describe('SmartTableViewCategoryComponent', () => {
  let component: SmartTableViewCategoryComponent;
  let fixture: ComponentFixture<SmartTableViewCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmartTableViewCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmartTableViewCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

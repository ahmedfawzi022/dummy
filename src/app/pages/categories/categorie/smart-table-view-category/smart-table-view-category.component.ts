import { Component, OnInit,Input } from '@angular/core';
import { Router } from '@angular/router';
import { DefaultEditor, ViewCell } from 'ng2-smart-table';

@Component({
  selector: 'ngx-smart-table-view-category',
  templateUrl: './smart-table-view-category.component.html',
  styleUrls: ['./smart-table-view-category.component.scss']
})
export class SmartTableViewCategoryComponent extends DefaultEditor implements OnInit, ViewCell {

  renderValue: string;
  @Input() value: string | number;
  @Input() rowData: any;
  constructor(public router:Router) {    super();
  }

  ngOnInit(): void {
    console.log(this.rowData)

  }

  viewSubCategory(){
    this.router.navigateByUrl(`/pages/categories/${this.rowData._id}/sub-categorie`);

  }

}

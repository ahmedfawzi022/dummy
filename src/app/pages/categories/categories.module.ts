import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CategoriesComponent } from './categories.component';
import { NbMenuModule } from '@nebular/theme';
import { ThemeModule } from '../../@theme/theme.module';
import { CategoriesRoutingModule } from './categories-routing.module';
import { CategorieComponent } from './categorie/categorie.component';
import { AddCategorieComponent } from './add-categorie/add-categorie.component';
import { SubCategorieComponent } from './sub-categorie/sub-categorie.component';
import { AddSubCategorieComponent } from './add-sub-categorie/add-sub-categorie.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';

import {
  NbAccordionModule,
  NbButtonModule,
  NbSpinnerModule,
  NbListModule,
  NbRouteTabsetModule,
  NbStepperModule,
  NbTabsetModule, NbUserModule,
  NbCardModule, NbIconModule, NbInputModule, NbTreeGridModule,
  NbActionsModule,
  NbCheckboxModule,
  NbDatepickerModule,
  NbRadioModule,
  NbSelectModule,
} from '@nebular/theme';
import { SmartTableViewCategoryComponent } from './categorie/smart-table-view-category/smart-table-view-category.component';
import { SmartTableActiveSubButtomComponent } from './sub-categorie/smart-table-active-sub-buttom/smart-table-active-sub-buttom.component';
import { SmartTableActiveButtomComponent } from './categorie/smart-table-active-buttom/smart-table-active-buttom.component';

@NgModule({
  declarations: [CategoriesComponent,SmartTableActiveButtomComponent, CategorieComponent, AddCategorieComponent, SubCategorieComponent, AddSubCategorieComponent, SmartTableViewCategoryComponent, SmartTableActiveSubButtomComponent],
  imports: [
    CommonModule,
    CategoriesRoutingModule,
    NbMenuModule,
    NbCardModule,
    NbTreeGridModule,
    NbIconModule,
    NbInputModule,
    NbSpinnerModule,
    NgSelectModule,
    ThemeModule,
    Ng2SmartTableModule,
    FormsModule,
    ReactiveFormsModule,
    NbTabsetModule,
    NbRouteTabsetModule,
    NbStepperModule,
    NbCardModule,
    NbButtonModule,
    NbListModule,
    NbAccordionModule,
    NbUserModule,
    NbActionsModule,
    NbCheckboxModule,
    NbDatepickerModule,
    NbRadioModule,
    NbSelectModule,
  ],
  entryComponents: [SmartTableViewCategoryComponent,SmartTableActiveButtomComponent,SmartTableViewCategoryComponent,SmartTableActiveSubButtomComponent]

})
export class CategoriesModule { }

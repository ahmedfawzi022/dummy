import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CategoriesService } from '../../../services/categories/categories.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Subcategory } from '../../../models/categories/subCategory'
import { DataService } from '../../../services/data/data.service';
import { BaseComponent } from '../../../pages/general/base/base.component';
import {
  NbComponentStatus,
  NbGlobalLogicalPosition,
  NbGlobalPhysicalPosition,
  NbGlobalPosition,
  NbToastrService,
  NbToastrConfig,
} from '@nebular/theme';
@Component({
  selector: 'ngx-add-sub-categorie',
  templateUrl:'./add-sub-categorie.component.html',
  styleUrls: ['./add-sub-categorie.component.scss'],
})
export class AddSubCategorieComponent extends BaseComponent implements OnInit {
  subcategory:Subcategory
  //Define Form Group with its controls
  addForm: FormGroup;
  nameControl: FormControl;
  descriptionControl: FormControl;
  featuredImageControl: FormControl;
  iconControl: FormControl;
  categoryIdControl:FormControl;
  // isActiveControl:FormControl;
  imageFeaturedImageSrc:any;
  catgories=[];
  imageIconSrc:any;
  categoryId:any;
  subCategoryId:any
  private sub: any;
  loading=false;
  datasaved = false;


  constructor(public categoriesService:CategoriesService , public data:DataService,public toastrService: NbToastrService,public router:Router,public route:ActivatedRoute) { 
  super(toastrService);
    this.subcategory=new Subcategory
  }

  ngOnInit(): void {
    
    this.route.params.subscribe(params => {
      this.categoryId = params['id']
    });
    this.route.params.subscribe(params => {
      this.subCategoryId = params['subcategoryid']
    });

    if(this.subCategoryId)
    this.getSubCategoryById()
    // create form group and its form controls and
    this.createFormControls();
    this.createForm();
    this.getCategories();
  }

  // create form controls that we need to validate with their needed validators
  createFormControls() {

    this.nameControl = new FormControl(null, [
      Validators.required,
    ]),
      this.descriptionControl = new FormControl(null, [
        Validators.required,
      ]);
      this.categoryIdControl = new FormControl(null, [
        Validators.required,
      ]);
    this.featuredImageControl = new FormControl('icon.jpg', [
      Validators.required,
    ]),
      this.iconControl = new FormControl('icon.jpg', [
        Validators.required,
      ]);
      // this.isActiveControl = new FormControl(false)
  }


  // add form controls to form group
  createForm() {
    this.addForm = new FormGroup({
      name: this.nameControl,
      description: this.descriptionControl,
      image: this.featuredImageControl,
      categoryId:this.categoryIdControl,
      icon: this.iconControl,
      // isActive:this.isActiveControl
    });
  }

  handleInputChangeFeaturedImage(e) {
    var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
    var pattern = /image-*/;
    var reader = new FileReader();
    if (!file.type.match(pattern)) {
      alert('invalid format');
      return;
    }
    reader.onload = this._handleReaderLoadedFeaturedImage.bind(this);
    reader.readAsDataURL(file);
  }

  _handleReaderLoadedFeaturedImage(e) {
    let reader = e.target;
    this.imageFeaturedImageSrc = reader.result;
    this.featuredImageControl.setValue(this.imageFeaturedImageSrc)
  }
  handleInputChangeIcon(e) {
    var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
    var pattern = /image-*/;
    var reader = new FileReader();
    if (!file.type.match(pattern)) {
      alert('invalid format');
      return;
    }
    reader.onload = this._handleReaderLoadedIcon.bind(this);
    reader.readAsDataURL(file);
  }

  _handleReaderLoadedIcon(e) {
    let reader = e.target;
    this.imageIconSrc = reader.result;
    this.iconControl.setValue(this.imageIconSrc)
  }
  getSubCategoryById() {
    this.loading=true;

    this.categoriesService.getSubCategoryById(this.subCategoryId).subscribe(
      data => {
        console.log(data.data)
        this.nameControl.setValue(data.data.name)
         this.subcategory.name = this.nameControl.value
        this.descriptionControl.setValue(data.data.description)
         this.subcategory.description = this.descriptionControl.value;
        this.imageFeaturedImageSrc = data?.data?.image;
        this.featuredImageControl.setValue(data?.data?.image)
         this.subcategory.image = this.featuredImageControl.value
        this.imageIconSrc = data?.data?.icon
        this.iconControl.setValue(data?.data?.icon)
         this.subcategory.icon = this.iconControl.value
         this.categoryIdControl.setValue(data?.data?.categoryId)
         this.subcategory.categoryId = this.categoryIdControl.value
        //  this.isActiveControl.setValue(data.data.isActive)
        //  this.subcategory.isActive=this.isActiveControl.value
         this.subcategory.categoryId=this.categoryId
         this.loading=false;

        // this.router.navigateByUrl(`/pages/categories/categorie`);


        console.log(data)
        // console.log(token)

      },
      error => {
        this.loading=false;
        // this.spinnerService.hide();
        try {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.loginerrorMessage);
        } catch (err) {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.errorMessage);
        }
      },
    );

  }
  getCategories() {
    this.loading=true;

    this.categoriesService.getCategoriesTable().subscribe(
      data => {
        console.log(data.data)
        this.catgories = data.data.list
         this.loading=false;

        // this.router.navigateByUrl(`/pages/categories/categorie`);


        console.log(data)
        // console.log(token)

      },
      error => {
        this.loading=false;
        // this.spinnerService.hide();
        try {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.loginerrorMessage);
        } catch (err) {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.errorMessage);
        }
      },
    );

  }
  addSubCategory() {
    this.loading=true;
    // this.subcategory.isActive=this.isActiveControl.value

    if(this.subCategoryId){
      this.subcategory.name = this.nameControl.value
      this.subcategory.description = this.descriptionControl.value;
      this.subcategory.image = this.featuredImageControl.value
      this.subcategory.icon = this.iconControl.value
  
  this.categoriesService.updateSubCategory(this.subcategory,this.subcategory).subscribe(
    data => {
      this.loading=false;

      this.router.navigateByUrl(`/pages/categories/${this.categoryId}/sub-categorie`);

      this.status='danger'
      this.title='Successfully Saved .. ';
      this.content='Please check !!'
      this.showToast(this.status, this.title, this.content);
      this.data.changeMessage(this.datasaved)
      this.loading=false;

      console.log(data)
      // console.log(token)

    },
    error => {
      console.log(error)
      this.loading=false;
      // this.spinnerService.hide();
      try {
        this.status='danger'
        this.title=error.error.message;
        this.content='Please check !!'
        this.showToast(this.status, this.title, this.content);
      } catch (err) {
        this.status='danger'
        this.title='Eror !!';
        this.content='Please check !!'
        this.showToast(this.status, this.title, this.content);
        // this.openModal(this.loginDataObject.error, this.loginDataObject.errorMessage);
      }
    }

  );

}else{
  this.categoryIdControl.disable()
  this.categoriesService.saveSubCategory(this.addForm.value,this.categoryIdControl.value).subscribe(
    data => {
      this.loading=false;

      this.router.navigateByUrl(`/pages/categories/categorie`);


      console.log(data)
      // console.log(token)

    },
    error => {
      console.log(error)
      this.loading=false;
      // this.spinnerService.hide();
      try {
        this.status='danger'
        this.title=error.error.message;
        this.content='Please check !!'
        this.showToast(this.status, this.title, this.content);
      } catch (err) {
        this.status='danger'
        this.title='Eror !!';
        this.content='Please check !!'
        this.showToast(this.status, this.title, this.content);
        // this.openModal(this.loginDataObject.error, this.loginDataObject.errorMessage);
      }

    },
  );

}
  

  }

}

import { Component, OnInit,TemplateRef } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { Router, ActivatedRoute } from '@angular/router';
import { ServerDataSource } from 'ng2-smart-table';
import { environment } from '../../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CategoriesService } from '../../../services/categories/categories.service';
import { DataService } from '../../../services/data/data.service';
import { NbDialogService } from '@nebular/theme';

import { SmartTableData } from '../../../@core/data/smart-table';
import { DomSanitizer } from '@angular/platform-browser';
import { SmartTableActiveSubButtomComponent } from './smart-table-active-sub-buttom/smart-table-active-sub-buttom.component';
@Component({
  selector: 'ngx-sub-categorie',
  templateUrl: './sub-categorie.component.html',
  styleUrls: ['./sub-categorie.component.scss'],
})
export class SubCategorieComponent implements OnInit {
  source: LocalDataSource;
  loading = false;
  rowData:any;
  categoryId: any;
  message:any;
  fubUrl: any;
  settings = {
    mode: 'external',
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      image: {
        title: 'Featured Image',
        filter: false,
        type: 'html',
        valuePrepareFunction: (picture: string) => { return `<img class="img-box" src="${picture}" />`; },

      },
      icon: {
        title: 'Icon',
        filter: false,
        type: 'html',
        valuePrepareFunction: (picture: string) => { return `<img class="img-box" src="${picture}" />`; },

      },
      name: {
        title: 'Name',
        type: 'string',
      },
      // description: {
      //   title: 'Description',
      //   type: 'string',
      //   filter: false
      // },

      active: {
        title: 'Action Status',
        type: 'custom',
        renderComponent: SmartTableActiveSubButtomComponent,
        editor: {
          type: 'custom',
          component: SmartTableActiveSubButtomComponent
        },
        sort: false,
        filter: false,
      },
      // picture: {
      //   title: 'Picture',
      //   type: 'html',
      //   valuePrepareFunction: (picture:string) => { return `<img width="50px" src="${picture}" />`; },
      //   },
    },
  };

  // source: LocalDataSource = new LocalDataSource();

  constructor(private dialogService: NbDialogService,private service: SmartTableData, public data:DataService,public categoriesService: CategoriesService, private route: ActivatedRoute, private httpClient: HttpClient, private router: Router, private _domSanitizer: DomSanitizer) {
    // const data = this.service.getData();
    // this.source.load(data);
    this.fubUrl = environment.FUBS_API_URL
  }
  ngOnInit(): void {
    this.data.currentMessage.subscribe(message => {
      this.message = message
    })
    this.route.params.subscribe(params => {
      this.categoryId = params['id']
      console.log(this.categoryId)
    });
    this.data.currentAction.subscribe(message => {
      this.getCategory();
    })
    this.getCategory();
  }
  getCategory() {
    this.loading = true;
    this.categoriesService.getSubCategoriesTable(this.categoryId)
      .subscribe(
        data => {
          console.log(data)
          this.loading = false;

          this.source = new LocalDataSource(data.data.list)
        }, error => {
          console.log(error)
          this.loading = false;


          // this.spinnerService.hide();
          try {
            // this.openModal(this.loginDataObject.error, this.loginDataObject.loginerrorMessage);
          } catch (err) {
            // this.openModal(this.loginDataObject.error, this.loginDataObject.errorMessage);
          }
        },

      )

    // this.resultData = true;

  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }

  onAdd() {
    this.router.navigateByUrl(`/pages/categories/${this.categoryId}/add-sub-categorie`);

  }
  dialogDelete(dialog: TemplateRef<any>,event) {
    console.log(event)
    this.rowData=event;
    this.dialogService.open(
      dialog,
      {
        context: 'this is some additional data passed to dialog',
        closeOnBackdropClick: false,
      });

  }
  onClickYes(){
    this.onDelete(this.rowData)
  }
  onDelete(event) {
    this.categoriesService.deleteSubCategory(event.data._id)

        .subscribe(
          data => {
            // this.del.nativeElement.click();
            this.getCategory();
            // this.makeToast(this.configurationDataObject.feesDeleteSuccessfullMessage);

          },
          error => {
//            this.del.nativeElement.click();
            try {
              // if (error.error.errorCode)
              // this.openModal(error.error.errorCode, error.error.errorMessage);
              // this.openModal(this.configurationDataObject.error, this.configurationDataObject.errorMessage);
            } catch (err) {
              // this.openModal(this.configurationDataObject.error, this.configurationDataObject.errorMessage);
            }
          },
      );

  }
  onEdit(event) {
    console.log(event)
    this.router.navigateByUrl(`/pages/categories/${this.categoryId}/add-sub-categorie/${event.data._id}`);

  }
}





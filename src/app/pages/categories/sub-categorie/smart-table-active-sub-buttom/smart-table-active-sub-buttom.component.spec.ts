import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmartTableActiveButtomComponent } from './smart-table-active-buttom.component';

describe('SmartTableActiveButtomComponent', () => {
  let component: SmartTableActiveButtomComponent;
  let fixture: ComponentFixture<SmartTableActiveButtomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmartTableActiveButtomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmartTableActiveButtomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

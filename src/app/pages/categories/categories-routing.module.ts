import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CategorieComponent } from './categorie/categorie.component';
import { CategoriesComponent } from './categories.component';

import { AddCategorieComponent } from './add-categorie/add-categorie.component';
import { SubCategorieComponent } from './sub-categorie/sub-categorie.component';
import { AddSubCategorieComponent } from './add-sub-categorie/add-sub-categorie.component';
const routes: Routes = [{
  path: '',
  component: CategoriesComponent,
  children: [

    {
      path: 'categorie',
      component: CategorieComponent,
      // canActivate: [RoleGuard],
      // data: {
      //   expectedRole: 'Read Configuration Template'
      // },
    },
    {
      path: 'add-categorie',
      component: AddCategorieComponent,
      // canActivate: [RoleGuard],
      // data: {
      //   expectedRole: 'Read Configuration Template'
      // },
    },
    {
      path: 'add-categorie/:id',
      component: AddCategorieComponent,
      // canActivate: [RoleGuard],
      // data: {
      //   expectedRole: 'Read Configuration Template'
      // },
    },
    {
      path: ':id/sub-categorie',
      component: SubCategorieComponent,
      // canActivate: [RoleGuard],
      // data: {
      //   expectedRole: 'Read Configuration Template'
      // },
    },
    {
      path: ':id/add-sub-categorie/:subcategoryid',
      component: AddSubCategorieComponent,
      // canActivate: [RoleGuard],
      // data: {
      //   expectedRole: 'Read Configuration Template'
      // },
    },
    {
      path: ':id/add-sub-categorie',
      component: AddSubCategorieComponent,
      // canActivate: [RoleGuard],
      // data: {
      //   expectedRole: 'Read Configuration Template'
      // },
    },
    {
      path: 'add-sub-categorie',
      component: AddSubCategorieComponent,
      // canActivate: [RoleGuard],
      // data: {
      //   expectedRole: 'Read Configuration Template'
      // },
    }
  ],
}];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CategoriesRoutingModule { }

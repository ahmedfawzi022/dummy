import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CategoriesService } from '../../../services/categories/categories.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Categories } from '../../../models/categories/categories'
import { DataService } from '../../../services/data/data.service';
import { BaseComponent } from '../../../pages/general/base/base.component';
import {
  NbComponentStatus,
  NbGlobalLogicalPosition,
  NbGlobalPhysicalPosition,
  NbGlobalPosition,
  NbToastrService,
  NbToastrConfig,
} from '@nebular/theme';
@Component({
  selector: 'ngx-add-categorie',
  templateUrl: './add-categorie.component.html',
  styleUrls: ['./add-categorie.component.scss'],
})
export class AddCategorieComponent extends BaseComponent implements OnInit {
  categories: Categories
  datasaved = false;
  //Define Form Group with its controls
  addForm: FormGroup;
  idControl: FormControl;
  nameControl: FormControl;
  descriptionControl: FormControl;
  featuredImageControl: FormControl;
  iconControl: FormControl;
  isActiveControl:FormControl;
  imageFeaturedImageSrc: any;
  imageIconSrc: any;
  categoryId: any;
  constructor(public categoriesService: CategoriesService,public toastrService: NbToastrService, public data:DataService,public router: Router, public route: ActivatedRoute) {
    super(toastrService);
    this.categories = new Categories;

  }
  loading=false;

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.categoryId = params['id']
    });
    if (this.categoryId)
      this.getCategoryById()
    // create form group and its form controls and
    this.createFormControls();
    this.createForm();
  }

  // create form controls that we need to validate with their needed validators
  createFormControls() {
    this.nameControl = new FormControl(null, [
      Validators.required,
    ]),
      this.descriptionControl = new FormControl(null, [
        Validators.required,
      ]);
    this.featuredImageControl = new FormControl('icon.jpg', [
      Validators.required,
    ]),
      this.iconControl = new FormControl('icon.jpg', [
        Validators.required,
      ]);
      // this.isActiveControl = new FormControl(false)
  }


  // add form controls to form group
  createForm() {
    this.addForm = new FormGroup({
      name: this.nameControl,
      description: this.descriptionControl,
      image: this.featuredImageControl,
      icon: this.iconControl,
//      isActive:this.isActiveControl
    });
  }

  getCategoryById() {
    this.loading=true;

    this.categoriesService.getCategoryById(this.categoryId).subscribe(
      data => {
        console.log(data.data)
        this.nameControl.setValue(data.data.name)
        this.categories.name = this.nameControl.value
        this.descriptionControl.setValue(data.data.description)
        this.categories.description = this.descriptionControl.value;
        this.imageFeaturedImageSrc = data?.data?.image;
        this.featuredImageControl.setValue(data?.data?.image)
        this.categories.image = this.featuredImageControl.value
        this.imageIconSrc = data?.data?.icon
        this.iconControl.setValue(data?.data?.icon)
        this.categories.icon = this.iconControl.value
        this.loading=false;

  //      this.isActiveControl.setValue(data.data.isActive)
        // this.categories.isActive=this.isActiveControl.value

        // this.router.navigateByUrl(`/pages/categories/categorie`);


        console.log(data)
        // console.log(token)

      },
      error => {
        this.loading=false;

        // this.spinnerService.hide();
        try {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.loginerrorMessage);
        } catch (err) {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.errorMessage);
        }
      },
    );

  }
  handleInputChangeFeaturedImage(e) {
    var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
    var pattern = /image-*/;
    var reader = new FileReader();
    if (!file.type.match(pattern)) {
      alert('invalid format');
      return;
    }
    reader.onload = this._handleReaderLoadedFeaturedImage.bind(this);
    reader.readAsDataURL(file);
  }

  _handleReaderLoadedFeaturedImage(e) {
    let reader = e.target;
    this.imageFeaturedImageSrc = reader.result;
    this.featuredImageControl.setValue(this.imageFeaturedImageSrc)
  }
  handleInputChangeIcon(e) {
    var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
    var pattern = /image-*/;
    var reader = new FileReader();
    if (!file.type.match(pattern)) {
      alert('invalid format');
      return;
    }
    reader.onload = this._handleReaderLoadedIcon.bind(this);
    reader.readAsDataURL(file);
  }

  _handleReaderLoadedIcon(e) {
    let reader = e.target;
    this.imageIconSrc = reader.result;
    this.iconControl.setValue(this.imageIconSrc)
  }

  addCategory() {
    this.loading=true;


    if (this.categoryId) {
      this.categories.name = this.nameControl.value
      this.categories.description = this.descriptionControl.value;
      this.categories.image = this.featuredImageControl.value
      this.categories.icon = this.iconControl.value
      // this.categories.isActive=this.isActiveControl.value
      this.categoriesService.saveCategory(this.categories, this.categoryId).subscribe(
        data => {
          this.router.navigateByUrl(`/pages/categories/categorie`);

          this.status='danger'
          this.title='Successfully Saved .. ';
          this.content='Please check !!'
          this.showToast(this.status, this.title, this.content);
          this.data.changeMessage(this.datasaved)
          this.loading=false;

        },
        error => {
          console.log(error)
          this.loading=false;
          // this.spinnerService.hide();
          try {
            this.status='danger'
            this.title=error.error.message;
            this.content='Please check !!'
            this.showToast(this.status, this.title, this.content);
          } catch (err) {
            this.status='danger'
            this.title='Eror !!';
            this.content='Please check !!'
            this.showToast(this.status, this.title, this.content);
            // this.openModal(this.loginDataObject.error, this.loginDataObject.errorMessage);
          }
        },
      );

    } else {
      this.categoriesService.saveCategory(this.addForm.value).subscribe(
        data => {
          this.router.navigateByUrl(`/pages/categories/categorie`);

          this.status='danger'
          this.title='Successfully Saved .. ';
          this.content='Please check !!'
          this.showToast(this.status, this.title, this.content);
          this.data.changeMessage(this.datasaved)
          this.loading=false;
        },
        error => {
          this.loading=false;

          console.log(error)
          // this.spinnerService.hide();
          try {
            // this.openModal(this.loginDataObject.error, this.loginDataObject.loginerrorMessage);
          } catch (err) {
            // this.openModal(this.loginDataObject.error, this.loginDataObject.errorMessage);
          }
        },
      );

    }


  }

}

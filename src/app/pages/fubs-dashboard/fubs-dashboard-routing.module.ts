import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FubsDashboardComponent } from './fubs-dashboard.component';

const routes: Routes = [{
  path: '',
  component: FubsDashboardComponent}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FubsDashboardRoutingModule { }

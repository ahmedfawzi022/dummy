import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbMenuModule } from '@nebular/theme';
import { ThemeModule } from '../../@theme/theme.module';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FubsDashboardRoutingModule } from './fubs-dashboard-routing.module';
import { FubsDashboardComponent } from './fubs-dashboard.component';
import { FubsCountrsComponent } from './fubs-countrs/fubs-countrs.component';


import {
  NbAccordionModule,
  NbButtonModule,
  NbSpinnerModule,
  NbListModule,
  NbRouteTabsetModule,
  NbStepperModule,
  NbTabsetModule, NbUserModule,
  NbCardModule, NbIconModule, NbInputModule, NbTreeGridModule,
  NbActionsModule,
  NbCheckboxModule,
  NbDatepickerModule,
  NbRadioModule,
  NbSelectModule,
} from '@nebular/theme';

@NgModule({
  declarations: [FubsDashboardComponent, FubsCountrsComponent],
  imports: [
    CommonModule,
    FubsDashboardRoutingModule,
    NbMenuModule,
    NbCardModule,
    NbTreeGridModule,
    NbIconModule,
    NbInputModule,
    NbSpinnerModule,
    ThemeModule,
    Ng2SmartTableModule,
    FormsModule,
    ReactiveFormsModule,
    NbTabsetModule,
    NbRouteTabsetModule,
    NbStepperModule,
    NbCardModule,
    NbButtonModule,
    NbListModule,
    NbAccordionModule,
    NbUserModule,
    NbActionsModule,
    NbCheckboxModule,
    NbDatepickerModule,
    NbRadioModule,
    NbSelectModule
  ]
})
export class FubsDashboardModule { }

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FubsCountrsComponent } from './fubs-countrs.component';

describe('FubsCountrsComponent', () => {
  let component: FubsCountrsComponent;
  let fixture: ComponentFixture<FubsCountrsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FubsCountrsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FubsCountrsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

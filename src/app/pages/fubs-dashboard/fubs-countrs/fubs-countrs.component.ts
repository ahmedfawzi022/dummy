import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { Router } from '@angular/router';

import { SmartTableData } from '../../../@core/data/smart-table';
import { DomSanitizer } from '@angular/platform-browser';


@Component({
  selector: 'ngx-fubs-countrs',
  templateUrl:'./fubs-countrs.component.html',
  styleUrls: ['./fubs-countrs.component.scss']
})
export class FubsCountrsComponent implements OnInit {

  settings = {
    mode: 'external',
    hideSubHeader:false,
    hideHeader:false,
    pager :{
      display:false,
      perPage:10
    },
    actions: {
      // columnTitle: this.basicDefinitionDataObject.actions,
      add: false,
      delete: false,
      edit:false,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {

      id: {
        title: 'Order Id',
        type: 'string',
      },
      lastName: {
        title: 'Customer Name',
        type: 'string',
      },
     
      // picture: {
      //   title: 'Picture',
      //   type: 'html',
      //   valuePrepareFunction: (picture:string) => { return `<img width="50px" src="${picture}" />`; },
      //   },
    },
  };

  source: LocalDataSource = new LocalDataSource();

  constructor(private service: SmartTableData, private router: Router, private _domSanitizer: DomSanitizer) {
    const data = this.service.getData();
    this.source.load(data);
  }
  ngOnInit(): void {
  }
  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }

  onAdd(){
    this.router.navigateByUrl('/pages/users/add-administrator');

  }
}





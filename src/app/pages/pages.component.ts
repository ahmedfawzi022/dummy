import { Component, OnInit } from '@angular/core';

import { MENU_ITEMS, MENU_ITEMS_Vendor, MENU_ITEMS_Operator, MENU_ITEMS_Admin } from './pages-menu';

@Component({
  selector: 'ngx-pages',
  styleUrls: ['pages.component.scss'],
  template: `
    <ngx-one-column-layout>
      <nb-menu [items]="menu"></nb-menu>
      <router-outlet></router-outlet>
    </ngx-one-column-layout>
  `,
})
export class PagesComponent implements OnInit {
  menu: any;
  ngOnInit(): void {
    var currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (currentUser.roles[0] == "Super_Admin") {
      this.menu = MENU_ITEMS;
    }
    if (currentUser.roles[0] == "Vendor") {
      this.menu = MENU_ITEMS_Vendor;
    }
    if (currentUser.roles[0] == "Admin") {
      this.menu = MENU_ITEMS_Admin;
    }
    if (currentUser.roles[0] == "Operator") {
      this.menu = MENU_ITEMS_Operator;
    }


  }
}

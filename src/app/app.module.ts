/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule,CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { HttpClientModule,HTTP_INTERCEPTORS } from '@angular/common/http';
import { CoreModule } from './@core/core.module';
import { ThemeModule } from './@theme/theme.module';
import { AppComponent } from './app.component';
import { AuthService } from './services/auth/auth.service';
import { CategoriesService } from './services/categories/categories.service';
import {DataService  } from './services/data/data.service';
import { FUBSServiceBase } from './services/base/FUBSServiceBase';
import { AuthGuard } from './_guards/guards';

import { ToastrModule } from 'ngx-toastr';

import { HttpConfigInterceptor} from './_guards/httpconfig.interceptor';

import { AppRoutingModule } from './app-routing.module';
import {
  NbChatModule,
  NbDatepickerModule,
  NbDialogModule,
  NbMenuModule,
  NbSpinnerModule,
  NbSidebarModule,
  NbToastrModule,
  NbWindowModule,
} from '@nebular/theme';
import { NgxSpinnerModule } from "ngx-spinner";

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    NgxSpinnerModule,
    NbSidebarModule.forRoot(),
    NbMenuModule.forRoot(),
    NbDatepickerModule.forRoot(),
    NbDialogModule.forRoot(),
    NbWindowModule.forRoot(),
    NbToastrModule.forRoot(),
    ToastrModule.forRoot(), // ToastrModule added
    NbChatModule.forRoot({
      messageGoogleMapKey: 'AIzaSyA_wNuCzia92MAmdLRzmqitRGvCF7wCZPY',
    }),
    CoreModule.forRoot(),
    ThemeModule.forRoot(),
  ],

  bootstrap: [AppComponent],
  providers: [
    AuthService,
    CategoriesService,
    DataService,
    AuthGuard,
    FUBSServiceBase,
    { provide:HTTP_INTERCEPTORS, useClass: HttpConfigInterceptor, multi: true }

  ],
})
export class AppModule {
}

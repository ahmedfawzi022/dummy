import { Injectable } from "@angular/core";
import { Router, CanActivate, ActivatedRouteSnapshot } from "@angular/router";
import { AuthService } from "./../services/auth/auth.service";

@Injectable()
export class RoleGuardService implements CanActivate {

  constructor(public auth: AuthService, public router: Router) { }

  canActivate(route: ActivatedRouteSnapshot): boolean {
    // this will be passed from the route config
    // on the data property
    let roles = localStorage.getItem('Roles');
    let expectedRole = route.data.expectedRole;

    if (roles && expectedRole) {
      let result = roles.indexOf(expectedRole);

      if (!this.auth.isAuthenticated() || result === -1) {
        this.router.navigate(['pages']).then(() => {
          // this.openMyMessageBox('Unauthorized', "The Current user doesn't have the right permission to access this page");
        });

        return false;
      }

      return true;
    }

    return false;
  }

  // openMyMessageBox(title: string, message: string) {
  //   const activeModal = this.modalService.open(MessageBoxModel, {
  //     size: 'sm',
  //     container: 'nb-layout',
  //   });

  //   activeModal.componentInstance.title = title;
  //   activeModal.componentInstance.message = message;
  // }
}

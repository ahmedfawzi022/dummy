import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FUBSServiceBase } from '../base/FUBSServiceBase';
import { Observable } from 'rxjs/Observable';
// import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable()
export class AuthService extends FUBSServiceBase {
  loginURL = 'http://localhost:3000/api/login';

  constructor(private httpClient: HttpClient) {
    super();

  }

  userlogin(data): Observable<any> {
    let options = {
      headers: new HttpHeaders().set('Content-Type', 'application/json')
    };
    return this.httpClient.post(`${this.fubsUrl}/auth/login`, data, options)
  }

  public isAuthenticated(): boolean {
    const currentUser = localStorage.getItem('currentUser');
    const token = currentUser['.expires'];
    // Check whether the token is expired and return
    // true or false
    if (currentUser) {
      return true;
    } else {
      return false;
    }
  }

  // getRoles(vendingStationId?: number): Observable<any>  {
  //   return this.httpClient.get(`${this.mdmUrl}/identity/roles?vendingStationId=${vendingStationId}`);
  // }

  logout(vendingStationId?: number): Observable<any> {

    return this.httpClient.post(`${this.fubsUrl}/Account/Logout?vendingStationId=${vendingStationId}`, '');

  }

  registerVendor(data,id?:any): Observable<any> {
    let options = {
      headers: new HttpHeaders().set('Content-Type', 'application/json')
    };

    return this.httpClient.post(`${this.fubsUrl}/auth/vendor/signup`, data,options)
    // return this.httpClient.post(`${this.fubUrl}/categories`, data)

  }


}



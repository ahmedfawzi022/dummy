import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { DatePipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import {FUBSServiceBase  } from '../base/FUBSServiceBase';

@Injectable()
export class DataService extends FUBSServiceBase{

  private messageSource = new BehaviorSubject('default message');
  currentMessage = this.messageSource.asObservable();

  private actionSource = new BehaviorSubject('default message');
  currentAction = this.actionSource.asObservable();

  private loginSource = new BehaviorSubject('');
  currentLogin = this.messageSource.asObservable();


  constructor(private datepipe:DatePipe,private http: HttpClient) {
    super();
  }

  changeMessage(message: any) {
    this.messageSource.next(message)
  }

  changeLogin(login: any) {
    this.loginSource.next(login)
  }

  changeAction(message: any) {
    this.actionSource.next(message)
  }

  
}

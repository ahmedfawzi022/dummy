import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { IfStmt } from '@angular/compiler';
@Injectable({
  providedIn: 'root',
})
export class UsersService {
  fubUrl: string;

  constructor(private httpClient: HttpClient) {
    this.fubUrl = environment.FUBS_API_URL;

  }

  save(data,id?:any): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    if(id){

    return this.httpClient.put(`${this.fubUrl}/users/${id}`, data,{ headers })
    }
    else
    return this.httpClient.post(`${this.fubUrl}/users/operator`, data,{ headers })
    // return this.httpClient.post(`${this.fubUrl}/categories`, data)

  }

  saveCustomer(data,id?:any): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    if(id){

    return this.httpClient.put(`${this.fubUrl}/users/${id}`, data,{ headers })
    }
    else
    return this.httpClient.post(`${this.fubUrl}/users/customer`, data,{ headers })
    // return this.httpClient.post(`${this.fubUrl}/categories`, data)

  }

  saveAdmin(data,id?:any): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    if(id){

    return this.httpClient.put(`${this.fubUrl}/users/${id}`, data,{ headers })
    }
    else
    return this.httpClient.post(`${this.fubUrl}/users/admin`, data,{ headers })
    // return this.httpClient.post(`${this.fubUrl}/categories`, data)

  }

  saveVendor(data,id?:any): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
  
    if(id){

      return this.httpClient.put(`${this.fubUrl}/users/${id}`, data,{ headers })
      }
      else
    return this.httpClient.post(`${this.fubUrl}/users/vendor`, data,{ headers })
    // return this.httpClient.post(`${this.fubUrl}/categories`, data)

  }

  saveShipmenttype(data,id?:any): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
  
    if(id){

      return this.httpClient.put(`${this.fubUrl}/shipment-types/${id}`, data,{ headers })
      }
      else
    return this.httpClient.post(`${this.fubUrl}/shipment-types`, data,{ headers })
    // return this.httpClient.post(`${this.fubUrl}/categories`, data)

  }
  saveBanner(data,id?:any): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
  
    if(id){

      return this.httpClient.put(`${this.fubUrl}/banners/${id}`, data,{ headers })
      }
      else
    return this.httpClient.post(`${this.fubUrl}/banners`, data,{ headers })
    // return this.httpClient.post(`${this.fubUrl}/categories`, data)

  }

  saveProduct(data,id?:any): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
  
    if(id){

      return this.httpClient.put(`${this.fubUrl}/products/admin/${id}`, data,{ headers })
      }
      else
    return this.httpClient.post(`${this.fubUrl}/products/admin`, data,{ headers })
    // return this.httpClient.post(`${this.fubUrl}/categories`, data)

  }

  getUserById(id): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    return this.httpClient.get(`${this.fubUrl}/users/${id}`,{ headers });
  }

  getShipmntById(id): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    return this.httpClient.get(`${this.fubUrl}/shipment-types/${id}`,{ headers });
  }
  getBannerById(id): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    return this.httpClient.get(`${this.fubUrl}/banners/${id}`,{ headers });
  }

  deleteUser(id): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    return this.httpClient.delete(`${this.fubUrl}/users/${id}`,{ headers });
  }

  deleteShipmentType(id): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    return this.httpClient.delete(`${this.fubUrl}/shipment-types/${id}`,{ headers });
  }
  deleteBanner(id): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    return this.httpClient.delete(`${this.fubUrl}/banners/${id}`,{ headers });
  }

  activeUser(id): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    let data ={
      "isActive":"true"
    }
    return this.httpClient.put(`${this.fubUrl}/users/${id}/activate`,data,{ headers });
  }

  resetAdminPassword(id): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });

    return this.httpClient.put(`${this.fubUrl}/users/${id}/reset-password`,'',{ headers });
  }
  getCountry(): Observable<any> {
    // let headers = new HttpHeaders({
    //   'Authorization': localStorage.getItem("token")
    // });
    return this.httpClient.get(`${this.fubUrl}/countries`);
  }

  getCategories(): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    return this.httpClient.get(`${this.fubUrl}/categories`,{ headers });
  }
  getSubCategories(id): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    return this.httpClient.get(`${this.fubUrl}/categories/${id}/sub-category`,{ headers });
  }

  getVendors(): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    return this.httpClient.get(`${this.fubUrl}/users/vendor`,{ headers });
  }
  getCustomers(): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    return this.httpClient.get(`${this.fubUrl}/users/customer`,{ headers });
  }
  getTokenById(id): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    let data={
      "id":id
    }
    return this.httpClient.post(`${this.fubUrl}/users/admin/as-vendor`,data,{ headers });
  }
  deactiveUser(id): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    let data ={
      "isActive":"false"
    }
    return this.httpClient.put(`${this.fubUrl}/users/${id}/activate`,data,{ headers });
  }

  resetPassword(data,id): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
 
    return this.httpClient.put(`${this.fubUrl}/users/${id}/reset-password`,data,{ headers });
  }

  getTotalCustomers(from,to): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    return this.httpClient.get(`${this.fubUrl}/users/admin/total-customers/dashboard?from=${from}&to=${to}`,{ headers });
  }

  getTotalOrders(from,to): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    return this.httpClient.get(`${this.fubUrl}/orders/admin/total-orders-count/dashboard?from=${from}&to=${to}`,{ headers });
  }
  getTotalOrdersVendor(from,to): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    return this.httpClient.get(`${this.fubUrl}/shipments/vendor/total-shipments-count/dashboard?from=${from}&to=${to}`,{ headers });
  }


}

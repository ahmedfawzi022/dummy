import { environment } from '../../../environments/environment';

export class FUBSServiceBase {
    mockBaseUrl: string;
    productionUrl: string;
    fubsUrl: string;

    constructor() {
       this.fubsUrl = environment.FUBS_API_URL;
        this.mockBaseUrl = '';
   }
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { IfStmt } from '@angular/compiler';
@Injectable({
  providedIn: 'root'
})
export class LocationsService {
  fubUrl: string;

  constructor(private httpClient: HttpClient) {
    this.fubUrl = environment.FUBS_API_URL;

  }

  saveCountry(data,id?:any): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    if(id){

    return this.httpClient.put(`${this.fubUrl}/countries/${id}`, data,{ headers })
    }
    else
    return this.httpClient.post(`${this.fubUrl}/countries`, data,{ headers })
    // return this.httpClient.post(`${this.fubUrl}/categories`, data)

  }

  saveCurrency(data,id?:any): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    if(id){

    return this.httpClient.put(`${this.fubUrl}/currencies/${id}`, data,{ headers })
    }
    else
    return this.httpClient.post(`${this.fubUrl}/currencies`, data,{ headers })
    // return this.httpClient.post(`${this.fubUrl}/categories`, data)

  }

  saveCity(data,id:any): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });

    return this.httpClient.post(`${this.fubUrl}/countries/${id}/cities`, data,{ headers })
    // return this.httpClient.post(`${this.fubUrl}/categories`, data)

  }

  updateCity(data,id:any): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });

    return this.httpClient.put(`${this.fubUrl}/countries//cities/${id}`, data,{ headers })
    // return this.httpClient.post(`${this.fubUrl}/categories`, data)

  }

  saveDistrict(data,id:any): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });

    return this.httpClient.post(`${this.fubUrl}/countries/${id}/districts`, data,{ headers })
    // return this.httpClient.post(`${this.fubUrl}/categories`, data)

  }


  updateDistrict(data,id:any): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });

    return this.httpClient.put(`${this.fubUrl}/countries//districts/${id}`, data,{ headers })
    // return this.httpClient.post(`${this.fubUrl}/categories`, data)

  }


  getCountryById(id): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    return this.httpClient.get(`${this.fubUrl}/countries/${id}`,{ headers });
  }

  getCityById(id): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    return this.httpClient.get(`${this.fubUrl}/countries/cities/${id}`,{ headers });
  }

  getDistrcitById(id): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    return this.httpClient.get(`${this.fubUrl}/countries/districts/${id}`,{ headers });
  }

  getcurrencyById(id): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    return this.httpClient.get(`${this.fubUrl}/currencies/${id}`,{ headers });
  }

  getCountry(): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    return this.httpClient.get(`${this.fubUrl}/countries`,{ headers });
  }

  getCities(countryId): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    return this.httpClient.get(`${this.fubUrl}/countries/${countryId}/cities`,{ headers });
  }

  deleteCountry(id): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    return this.httpClient.delete(`${this.fubUrl}/countries/${id}`,{ headers });
  }
  deleteCity(id): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    return this.httpClient.delete(`${this.fubUrl}/countries/cities/${id}`,{ headers });
  }
  deleteDistrict(id): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    return this.httpClient.delete(`${this.fubUrl}/countries/districts/${id}`,{ headers });
  }
  deleteCurrency(id): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    return this.httpClient.delete(`${this.fubUrl}/currencies/${id}`,{ headers });
  }


  


}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class OrdersService {
  fubUrl: string;

  constructor(private httpClient: HttpClient) {
    this.fubUrl = environment.FUBS_API_URL;

  }

  getTotalRevenue(): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    return this.httpClient.get(`${this.fubUrl}/orders/admin/total-revenue/dashboard`,{ headers });
  }
  getTotalCollected(): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    return this.httpClient.get(`${this.fubUrl}/orders/admin/total-collected/dashboard`,{ headers });
  }

  getTotalRevenueVendor(): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    return this.httpClient.get(`${this.fubUrl}/shipments/vendor/total-shipments-count/dashboard`,{ headers });
  }
  getTotalCollectedVendor(): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    return this.httpClient.get(`${this.fubUrl}/shipments/vendor/total-collected/dashboard`,{ headers });
  }

  changeStatus(value,id): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    let data={
      "shipmentStatus":value
    }
    return this.httpClient.put(`${this.fubUrl}/shipments/vendor/${id}/status`,data,{ headers });
  }

  

}

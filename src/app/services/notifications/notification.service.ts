import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { IfStmt } from '@angular/compiler';
@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  fubUrl: string;

  constructor(private httpClient: HttpClient) {
    this.fubUrl = environment.FUBS_API_URL;

  }

  save(data,id?:any): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    if(id){

    return this.httpClient.put(`${this.fubUrl}/notifications/${id}`, data,{ headers })
    }
    else
    return this.httpClient.post(`${this.fubUrl}/notifications`, data,{ headers })
    // return this.httpClient.post(`${this.fubUrl}/categories`, data)

  }



  getNotificationById(id): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    return this.httpClient.get(`${this.fubUrl}/notifications/${id}`,{ headers });
  }

  deleteNotifications(id): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    return this.httpClient.delete(`${this.fubUrl}/notifications/${id}`,{ headers });
  }

  notifyUsers(data): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    return this.httpClient.post(`${this.fubUrl}/notifications/notify`,data,{ headers });
  }

  
  getNotificationss(): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    return this.httpClient.get(`${this.fubUrl}/notifications`,{ headers });
  }
 

}

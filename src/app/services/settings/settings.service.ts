import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class SettingsService {
  fubUrl: string;

  constructor(private httpClient: HttpClient) {
    this.fubUrl = environment.FUBS_API_URL;

  }

  saveSetting(data,id?:any): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    if(id)
    return this.httpClient.put(`${this.fubUrl}/settings/${id}`, data,{ headers })
    else
    return this.httpClient.post(`${this.fubUrl}/settings`, data,{ headers })

  }
  saveFaq(data,id?:any): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    if(id)
    return this.httpClient.put(`${this.fubUrl}/faq/${id}`, data,{ headers })
    else
    return this.httpClient.post(`${this.fubUrl}/faq`, data,{ headers })

  }

  saveprivacy(data,id?:any): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    if(id)
    return this.httpClient.put(`${this.fubUrl}/privacy/${id}`, data,{ headers })
    else
    return this.httpClient.post(`${this.fubUrl}/privacy`, data,{ headers })

  }

  saveAboutUs(data,id?:any): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    if(id)
    return this.httpClient.put(`${this.fubUrl}/about-us/${id}`, data,{ headers })
    else
    return this.httpClient.post(`${this.fubUrl}/about-us`, data,{ headers })

  }

  saveComissionRate(data): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });

    return this.httpClient.post(`${this.fubUrl}/commission-rate`, data,{ headers })

  }


  getSettingById(id): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    return this.httpClient.get(`${this.fubUrl}/settings/${id}`,{ headers });
  }

  getFAQById(id): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    return this.httpClient.get(`${this.fubUrl}/faq/${id}`,{ headers });
  }
  getPrivacyById(id): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    return this.httpClient.get(`${this.fubUrl}/privacy/${id}`,{ headers });
  }
  getAboutUsById(id): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    return this.httpClient.get(`${this.fubUrl}/about-us/${id}`,{ headers });
  }

  getComissionRateById(): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    return this.httpClient.get(`${this.fubUrl}/commission-rate`,{ headers });
  }

  getCountry(): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    return this.httpClient.get(`${this.fubUrl}/countries`,{ headers });
  }

  getCities(countryId): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    return this.httpClient.get(`${this.fubUrl}/countries/${countryId}/cities`,{ headers });
  }

  deleteSetting(id): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    return this.httpClient.delete(`${this.fubUrl}/settings/${id}`,{ headers });
  }

  deleteFaq(id): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    return this.httpClient.delete(`${this.fubUrl}/faq/${id}`,{ headers });
  }
  deleteprivacy(id): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    return this.httpClient.delete(`${this.fubUrl}/privacy/${id}`,{ headers });
  }
  
  activeSetting(id): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    let data ={
      "isActive":"true"
    }
    return this.httpClient.put(`${this.fubUrl}/settings/${id}/activate`,data,{ headers });
  }
  deactiveSetting(id): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    let data ={
      "isActive":"false"
    }
    return this.httpClient.put(`${this.fubUrl}/settings/${id}/activate`,data,{ headers });
  }

  setDefault(id): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    let data ={
      "isActive":"false"
    }
    return this.httpClient.put(`${this.fubUrl}/settings/${id}/default`,data,{ headers });
  }
  


}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { IfStmt } from '@angular/compiler';
@Injectable({
  providedIn: 'root'
})
export class CategoriesService {
  fubUrl: string;

  constructor(private httpClient: HttpClient) {
    this.fubUrl = environment.FUBS_API_URL;

  }

  saveCategory(data,id?:any): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    if(id){

    return this.httpClient.put(`${this.fubUrl}/categories/${id}`, data,{ headers })
    }
    else
    return this.httpClient.post(`${this.fubUrl}/categories`, data,{ headers })
    // return this.httpClient.post(`${this.fubUrl}/categories`, data)

  }
  activeCategory(data,id): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    return this.httpClient.put(`${this.fubUrl}/categories/${id}/activate`,data,{ headers })
    // return this.httpClient.post(`${this.fubUrl}/categories`, data)

  }
  deactiveCategory(data,id): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    return this.httpClient.put(`${this.fubUrl}/categories/${id}/activate`,data,{ headers })
    // return this.httpClient.post(`${this.fubUrl}/categories`, data)

  }
  getCategoriesTable(): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    return this.httpClient.get(`${this.fubUrl}/categories`,{ headers });
  }
  getCategoryById(id): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    return this.httpClient.get(`${this.fubUrl}/categories/${id}`,{ headers });
  }

  getSubCategoryById(id): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    return this.httpClient.get(`${this.fubUrl}/categories//${id}/sub-category`,{ headers });
  }
  activeSubCategory(data,id): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    return this.httpClient.put(`${this.fubUrl}/categories/${id}/sub-category/activate`,data,{ headers })
    // return this.httpClient.post(`${this.fubUrl}/categories`, data)

  }
  deactiveSubCategory(data,id): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    return this.httpClient.put(`${this.fubUrl}/categories/${id}/sub-category/activate`,data,{ headers })
    // return this.httpClient.post(`${this.fubUrl}/categories`, data)

  }
  saveSubCategory(data,id): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });

    return this.httpClient.post(`${this.fubUrl}/categories/${id}/sub-category`, data,{ headers })
    // return this.httpClient.post(`${this.fubUrl}/categories`, data)

  }
  updateSubCategory(data,id): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    if(id)
    return this.httpClient.put(`${this.fubUrl}/categories/sub-category/${id}`, data,{ headers })
    // return this.httpClient.post(`${this.fubUrl}/categories`, data)

  }

  getSubCategoriesTable(id): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    return this.httpClient.get(`${this.fubUrl}/categories/${id}/sub-category`,{ headers });
  }
  deleteCategory(id): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    return this.httpClient.delete(`${this.fubUrl}/categories/${id}`,{ headers });
  }
  deleteSubCategory(id): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    return this.httpClient.delete(`${this.fubUrl}/categories/sub-category/${id}`,{ headers });
  }

}

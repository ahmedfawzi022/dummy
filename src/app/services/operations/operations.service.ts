import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class OperationsService {
  fubUrl: string;

  constructor(private httpClient: HttpClient) {
    this.fubUrl = environment.FUBS_API_URL;

  }

  
  activeProduct(id): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    let data ={
      "isActive":"true"
    }
    return this.httpClient.put(`${this.fubUrl}/products/admin/${id}/activate`,data,{ headers });
  }
  deactiveProduct(id): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    let data ={
      "isActive":"false"
    }
    return this.httpClient.put(`${this.fubUrl}/products/admin/${id}/activate`,data,{ headers });
  }

  setDefault(id): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    let data ={
      "isActive":"false"
    }
    return this.httpClient.put(`${this.fubUrl}/settings/${id}/default`,data,{ headers });
  }
  
  saveProduct(data,id?:any): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
  
    if(id){

      return this.httpClient.put(`${this.fubUrl}/products/admin/${id}`, data,{ headers })
      }
      else
    return this.httpClient.post(`${this.fubUrl}/products/admin`, data,{ headers })
    // return this.httpClient.post(`${this.fubUrl}/categories`, data)

  }

  saveProductForVendor(data,id?:any): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
  
    if(id){

      return this.httpClient.put(`${this.fubUrl}/products/vendor/${id}`, data,{ headers })
      }
      else
    return this.httpClient.post(`${this.fubUrl}/products/vendor`, data,{ headers })
    // return this.httpClient.post(`${this.fubUrl}/categories`, data)

  }

  getProductById(id): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    return this.httpClient.get(`${this.fubUrl}/products/admin/${id}`,{ headers });
  }
  getProductByIdForVendor(id): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    return this.httpClient.get(`${this.fubUrl}/products/vendor/${id}`,{ headers });
  }

  deleteProduct(id): Observable<any> {
    let headers = new HttpHeaders({
      'Authorization': localStorage.getItem("token")
    });
    return this.httpClient.delete(`${this.fubUrl}/products/admin/${id}`,{ headers });
  }

}

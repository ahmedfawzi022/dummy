import { Component, OnInit,ChangeDetectionStrategy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NbLayoutDirectionService } from '@nebular/theme';
import { AuthService } from '../../services/auth/auth.service';
import * as JWT from 'jwt-decode';
import { BaseComponent } from '../../../app/pages/general/base/base.component';
import { NgxSpinnerService } from "ngx-spinner";
import { DataService } from '../../services/data/data.service';

// import { ToastrService } from 'ngx-toastr';
import {
  NbComponentStatus,
  NbGlobalLogicalPosition,
  NbGlobalPhysicalPosition,
  NbGlobalPosition,
  NbToastrService,
  NbToastrConfig,
} from '@nebular/theme';

@Component({
  selector: 'ngx-login',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl:'./login.component.html',
  styleUrls: ['./login.component.scss'],
})

export class LoginComponent extends BaseComponent implements OnInit {
  loading = false;
  isLoginSucess:boolean=true;

  //Define Form Group with its controls
  signin: FormGroup;
  userNameControl: FormControl;
  passwordControl: FormControl;
  // config: NbToastrConfig;
  status: NbComponentStatus = 'success';

  title:any;
  content:any;
  // status: NbComponentStatus = 'success';

  constructor(public auth: AuthService, public data:DataService,private router: Router,public toastrService: NbToastrService,private spinner: NgxSpinnerService) {
    super(toastrService);
    const el = document.getElementById('nb-global-spinner');
    if (el) {
      el.style['display'] = 'none';
    }
  }

  loadingLargeGroup = false;
  loadingMediumGroup = false;

  toggleLoadingLargeGroupAnimation() {
    this.loadingLargeGroup = true;

    setTimeout(() => this.loadingLargeGroup = false, 3000);
  }

  ngOnInit() {
    // this.data.currentMessage.subscribe(message => {

    // })
    // create form group and its form controls and
    this.createFormControls();
    this.createForm();
    this.logout();
    const el = document.getElementById('nb-global-spinner');
    if (el) {
      el.style['display'] = 'none';
    }

  }

  // create form controls that we need to validate with their needed validators
  createFormControls() {

    this.userNameControl = new FormControl(null, [
      Validators.required,
    ]),
      this.passwordControl = new FormControl(null, [
        Validators.required,
      ]);
  }


  // add form controls to form group
  createForm() {
    this.signin = new FormGroup({
      userName: this.userNameControl,
      password: this.passwordControl,
    });
  }
  logout(){
    localStorage.removeItem('currentUser');
    localStorage.removeItem('role');
    localStorage.removeItem('token');

    // this.router.navigate(['auth/login'])
  }
  signIn() {
    this.isLoginSucess=false;

    this.spinner.show();
    //userlogin
    this.auth.userlogin(this.signin.value).subscribe(
      user => {
        // location.reload()
        this.isLoginSucess=false;

        this.status='success'
        //  let token = JWT(user.data.token)
        localStorage.setItem('signinValues', JSON.stringify(this.signin.value));
        localStorage.setItem('currentUser', JSON.stringify(user.data));
        sessionStorage.setItem('currentUser', JSON.stringify(user.data));
        localStorage.setItem('token', user.data.token);
        console.log(JSON.stringify(user.data))
        this.data.changeLogin(localStorage.getItem('signinValues'))
        // this.title='welcome '+this.userNameControl.value;
        // this.content='TO FUBS ADMIN PANEL'

        this.router.navigateByUrl(`/pages/dashboard`);

        // this.showToast(this.status, this.title, this.content);
        setTimeout(()=>{    //<<<---    using ()=> syntax
          this.spinner.hide();
          this.isLoginSucess=true;


     },1000);



        // console.log(user)
        // console.log(token)

      },
      error => {
        this.spinner.hide();
                this.status='danger'
        this.title='user name or password incorrect';
        this.content='Plese try again ...'
        this.showToast(this.status, this.title, this.content);
        this.isLoginSucess=false;

        // this.spinnerService.hide();
        try {
          this.isLoginSucess=true;

          // this.openModal(this.loginDataObject.error, this.loginDataObject.loginerrorMessage);
        } catch (err) {
          this.isLoginSucess=true;

          // this.openModal(this.loginDataObject.error, this.loginDataObject.errorMessage);
        }
      },
    );


  }



}

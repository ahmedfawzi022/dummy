import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../services/users/users.service';
import { AuthService } from '../../services/auth/auth.service';

import { environment } from '../../../environments/environment';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Vendor } from '../../models/users/vendor'
import { Router, ActivatedRoute } from '@angular/router';
import { DataService } from '../../services/data/data.service';
import { BaseComponent } from '../../pages/general/base/base.component';
import {
  NbComponentStatus,
  NbGlobalLogicalPosition,
  NbGlobalPhysicalPosition,
  NbGlobalPosition,
  NbToastrService,
  NbToastrConfig,
} from '@nebular/theme';
@Component({
  selector: 'ngx-signup',
  templateUrl:'./signup.component.html',
  styleUrls: ['./signup.component.scss'],
})
export class SignupComponent extends BaseComponent implements OnInit {
  vendor: Vendor
  images = [];
  countries=[]

  //Define Form Group with its controls
  signIn: FormGroup;
  idControl: FormControl;
  userNameControl: FormControl;
  firstNameControl: FormControl;
  brandNameControl:FormControl;
  brandLogoControl:FormControl;
  lastNameControl:FormControl;
  phoneNumberControl: FormControl;
  countryIdControl:FormControl


  emailControl: FormControl;
  passwordControl: FormControl;
  profilePictureControl: FormControl;
  public imageProfileSrc: string = '';
  public imageLogoSrc: string = '';

  loading = false;

  constructor(public usersService: UsersService,public authService:AuthService, public route: ActivatedRoute, public data: DataService, public router: Router, public toastrService: NbToastrService) {
    super(toastrService)
    this.vendor = new Vendor()
  }

  ngOnInit() {
    // create form group and its form controls and
    this.createFormControls();
    this.createForm();
    this.getCountry();

  }

  // create form controls that we need to validate with their needed validators
  createFormControls() {
    this.firstNameControl = new FormControl(null, [
      Validators.required,
    ]),
    this.lastNameControl = new FormControl(null, [
      Validators.required,
    ]),
      this.userNameControl = new FormControl(null, [
        Validators.required,
      ]),
      this.countryIdControl = new FormControl(null, [
        Validators.required,
      ]),
      this.passwordControl = new FormControl(null, [
        Validators.required,
        Validators.pattern("[a-zA-Z0-9]{6,255}$")
      ]);
      this.brandNameControl= new FormControl(null, [
        Validators.required,
      ]),
      this.brandLogoControl= new FormControl(null, [
        Validators.required,
      ]),
    this.emailControl = new FormControl(null, [
      Validators.required,
      Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")
    ]),
      this.phoneNumberControl = new FormControl(null, [
        Validators.required,
        Validators.pattern("(01)[0-9]{9}")
      ]);
    this.profilePictureControl = new FormControl('', [
     Validators.required,
    ]);
    this.idControl = new FormControl(null)
  }


  // add form controls to form group
  createForm() {
    this.signIn = new FormGroup({
      firstName: this.firstNameControl,
      lastName:this.lastNameControl,
      userName: this.userNameControl,
      brandName:this.brandNameControl,
      brandLogo:this.brandLogoControl,
      email: this.emailControl,
      countryId:this.countryIdControl,
      phoneNumber: this.phoneNumberControl,
      profilePicture: this.profilePictureControl,
      password: this.passwordControl,
      id: this.idControl
    });
  }
  getCountry(){
    this.loading=true;
  

    this.usersService.getCountry().subscribe(
      data => {
        console.log(data.data)
        this.countries=data.data.list


  //      this.isActiveControl.setValue(data.data.isActive)
        // this.categories.isActive=this.isActiveControl.value

        // this.router.navigateByUrl(`/pages/categories/categorie`);


        console.log(data)
        // console.log(token)

      },
      error => {
        this.loading=false;

        // this.spinnerService.hide();
        try {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.loginerrorMessage);
        } catch (err) {
          // this.openModal(this.loginDataObject.error, this.loginDataObject.errorMessage);
        }
      },
    );
  }
 
  handleInputProfileChange(e) {
    var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
    var pattern = /image-*/;
    var reader = new FileReader();
    if (!file.type.match(pattern)) {
      alert('invalid format');
      return;
    }
    reader.onload = this._handleProfileReaderLoaded.bind(this);
    reader.readAsDataURL(file);
  }
  _handleProfileReaderLoaded(e) {
    let reader = e.target;
    this.imageProfileSrc = reader.result;
    console.log(this.imageProfileSrc)
    this.profilePictureControl.setValue(this.imageProfileSrc)
  }
  handleLogoInputChange(e){
    var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
    var pattern = /image-*/;
    var reader = new FileReader();
    if (!file.type.match(pattern)) {
      alert('invalid format');
      return;
    }
    reader.onload = this._handleLogoReaderLoaded.bind(this);
    reader.readAsDataURL(file);
  }
  _handleLogoReaderLoaded(e) {
    let reader = e.target;
    this.imageLogoSrc = reader.result;
    console.log(this.imageLogoSrc)
    this.brandLogoControl.setValue(this.imageLogoSrc)
  }
  onFileChange(event){
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
              var reader = new FileReader();
 
              reader.onload = (event:any) => {
                 this.images.push(event.target.result); 
                 this.vendor.brand.gallery=this.images;

                //  this.myForm.patchValue({
                //     fileSource: this.images
                //  });
              }

              reader.readAsDataURL(event.target.files[i]);
      }
  }
  }

  clear(){
    this.images=[];
    this.vendor.brand.gallery=this.images;


  }
  save() {

    this.loading = true;
    this.vendor.firstName = this.firstNameControl.value
    this.vendor.lastName = this.lastNameControl.value
    this.vendor.userName = this.userNameControl.value;
    this.vendor.email = this.emailControl.value
    this.vendor.countryId=this.countryIdControl.value;
    this.vendor.password = this.passwordControl.value
    this.vendor.phoneNumber = this.phoneNumberControl.value
    this.vendor.profilePicture = this.profilePictureControl.value
    this.vendor.brand.name=this.brandNameControl.value;
    this.vendor.brand.logo=this.brandLogoControl.value;
    console.log(this.vendor);

   
      this.authService.registerVendor(this.vendor).subscribe(
        data => {

          this.status = 'success'
          this.title = 'Successfully Saved .. ';
          this.content = 'Please check !!'
          this.showToast(this.status, this.title, this.content);
          this.data.changeMessage(true)
          this.router.navigateByUrl(`/auth/login`);

          this.loading = false;
        },
        error => {
          this.loading = false;

          console.log(error)
          try {
            this.status='danger'
            this.title='Please check !!';
            this.content=error.error.message
             this.showToast(this.status, this.title,this.content );
          } catch (err) {
            this.status='danger'
            this.title='Please check !!';
            this.content=err.message;
             this.showToast(this.status, this.title,this.content );
          }
        },
      );

    


  }

}


export class Subcategory {
    name: string;
    description: string;
    image: string;
    icon: string;
    isActive:any;
    categoryId:any;
  }
  
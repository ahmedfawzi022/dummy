export class ProductVendor {
    name: string;
    arName:string;
    description:string;
    categoryId: string;
    subCategoryId: string;
    stock:string;
    price: string;
    image:any;
    discount:any;
    isDiscountActive:boolean=false;
    isTopSelling:0;
    gallery= [];
  }

  
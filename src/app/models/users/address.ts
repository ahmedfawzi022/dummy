export class Address {
    name: string;
    contactNumber: string;
    country: string;
    city: string;
    district: string;
    streetName: string;
    buildingNumber: string;
    apartmentNumber: string;

  }
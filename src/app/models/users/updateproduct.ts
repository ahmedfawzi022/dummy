export class UpdateProduct {
    name: string;
    description:string;
    categoryId: string;
    subCategoryId: string;
    stock:string;
    price: string;
    image:any;
    discount:any
    vendorId:any;
    isDiscountActive:boolean=false;
    gallery= [];
  }

  
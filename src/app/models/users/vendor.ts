export class Vendor {
    firstName: string;
    lastName:string;
    userName: string;
    email: string;
    countryId:string;
    phoneNumber: string;
    profilePicture='https://cdn.business2community.com/wp-content/uploads/2017/08/blank-profile-picture-973460_640.png';
    password:any
    brand=new Brand
  }

  export class Brand {
    name: string;
    logo:string;
    gallery= [];
    legalForms= [];

  }
  
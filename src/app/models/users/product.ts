export class Product {
    name: string;
    arName:string;
    description:string;
    categoryId: string;
    subCategoryId: string;
    stock:string;
    price: string;
    image:any;
    discount:any;
    vendorId:any;
    isDiscountActive:boolean=false;
    isTopSelling:0;
    gallery= [];
  }

  
import { Component } from '@angular/core';

@Component({
  selector: 'ngx-three-columns-layout',
  styleUrls: ['./three-columns.layout.scss'],
  template: `
  <nb-layout>
  <nb-layout-column class="row p-0 m-0">
    <div class="col-sm-6 p-0 videoBG">
    <div class="position-absolute d-flex align-items-center w-100 h-100 text-center bg-danger">
    <img class="d-block mx-auto" src="./assets/images/logo-white.png" />

        <video class="position-fixed" loop muted autoplay oncanplay="this.play()" onloadedmetadata="this.muted = true">
          <source src="./assets/images/background.mp4" type="video/mp4"> Your browser does not support HTML5 video.
        </video>
      </div>
    </div>
    <div class="col-sm-6 p-5 bg-white  align-items-center">
      <ng-content select="router-outlet"></ng-content>
    </div>
  </nb-layout-column>
</nb-layout>

  `,
})
export class ThreeColumnsLayoutComponent {}

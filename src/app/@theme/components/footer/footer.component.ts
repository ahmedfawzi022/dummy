import { Component } from '@angular/core';

@Component({
  selector: 'ngx-footer',
  styleUrls: ['./footer.component.scss'],
  template: `
    <span class="created-by">
      Created with ♥ by <b><a href="https://betahubs.com" target="_blank">BETAHUBS</a></b> 2020
    </span>
    <div class="socials">
      <a href="https://betahubs.com" target="_blank" class="ion ion-social-facebook"></a>
      <a href="https://betahubs.com" target="_blank" class="ion ion-social-twitter"></a>
      <a href="https://betahubs.com" target="_blank" class="ion ion-social-linkedin"></a>
    </div>
  `,
})
export class FooterComponent {
}
